# Matchingham Documentation Website

This website is built using [Docusaurus v3.4](https://docusaurus.io/), a modern static website generator.

## Installation

```
$ yarn
```

## Local Development

```
$ yarn start
```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

## Build

```
$ yarn build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

## Deployment

This project is deployed to [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/). The deployment is automated using Gitlab CI/CD pipeline. The pipeline is configured to deploy the website to Gitlab Pages when changes are pushed to the `main` branch.

## Matchingham Automated SDK CI/CD Pipeline Process

This project has an automation with Matchingham SDK projects. This GitLab CI/CD pipeline is configured to manage deployment, documentation updates, and notifications via Slack. It consists of three main stages: `deploy`, `document`, and `slack`. Each stage is designed to perform specific tasks crucial for deploying SDK projects. 

> This pipeline will only work if any tag is being pushed to main branch of the SDK project repository in gitlab.matchingham.gs.

**Deploy**:
Publishes packages to NPM automatically when tags are pushed.

**Document**:
Handles the update of documentation based on changes to markdown files limited only for CHANGELOG.md files.

**Slack**
Sends Slack notification specified in catalog project, 

### Creating a new SDK project
  
- Changes in `gitlab.matchingham.gs`  
To include any Matchingham SDK project in Matchingham Gitlab server onto Changelog automation, simply create `.gitlab-ci.yml` file and add the below lines.

  ```yml
  include: 
    - project: 'devops-snippets/ci-cd-snippets'
      ref: main
      file: '/sdk-automation/sdk-documentation-base-ci.yml'
  ```

- Changes in this Docusaurus documentation project in `gitlab.com`.
  Pipeline will create a new folder inside the `docs` folder. This is the name of the directory for the project. For example if the project URL is like so: `gitlab.matchingham.gs/group-name/project-1`, this folder name is equal to is project-1. for detailed documetation please follow: [CI_PROJECT_NAME](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

  Add your file into sidebar.js file with  desired hierarchy.

  ```js
  {
    type: 'category',
    label: 'Sidebar Visible Name',
    items: [
      {
        type: 'doc',
        id: 'sdk-path/README',
        label: 'Document',
      },
      {
        type: 'doc',
        id: 'sdk-path/CHANGELOG',
        label: 'Changelog',
      }
    ],
  },
  ```

  From now on pipeline will automatically update the gitlab.com project documentation based on the changes in the CHANGELOG.md file in gitlab.matchingham.gs project.

## Creating Markdown Files

to create a new markdown file, you can create a new file in the `docs` folder and add the file to the sidebar.js file. 

```js
{
  type: 'doc',
  id: 'sdk-path/NEW-MARKDOWN-FILE',
  label: 'New Markdown File',
},
```

for detail information please refer to : [docusaurus v3.4](https://docusaurus.io/docs) documentation.