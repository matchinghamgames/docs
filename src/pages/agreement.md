# LICENSE OF COPYRIGHT AGREEMENT

This is a License of Copyright Agreement (the “**Agreement**”) between you ("**Licensee**” as defined below) and Matchingham Games Limited with the company number 12452205 (“**Licensor**”). In order to use the **Product** (as defined below), you must first agree to this Agreement. BY DOWNLOADING, INSTALLING, ACCESSING OR OTHERWISE USING THE SDK, YOU ACCEPT THE TERMS OF THIS AGREEMENT. IF YOU DO NOT AGREE TO THE TERMS OF THIS AGREEMENT, DO NOT INSTALL, ACCESS OR USE THE PRODUCT. If you are agreeing to be bound by the Agreement on behalf of your employer or other entity, you represent and warrant that you have full legal authority to bind your employer or such entity to the Agreement. If you do not have the requisite authority, you may not accept the Agreement or use the Product on behalf of your employer or other entity. You agree that this Agreement is enforceable like any written negotiated agreement signed by you. If you do not agree to the terms of the Agreement, do not use the Product.

This Agreement commences on the date that Licensee starts using Product.

## Background

1. The Licensor has created the Product. The Product and any authorized copies that Licensee make are the intellectual property of, and all rights therein are owned by, the Licensor. 
2. The Licensee wishes to receive and the Licensor is willing to grant to the Licensee a licence on the terms and conditions set out in this Agreement to use the Product.

## Agreed terms

1. Interpretation

    The following definitions and rules of interpretation apply in this Agreement.

    1. **Definitions**:
       1. **Copyright :** all copyright and rights in the nature of copyright subsisting in the Product in any part of the world to which the Licensor is, or may become, entitled. 
       2. **Business Day :** a day other than a Saturday, Sunday or public holiday in England.
       3. **Effective Date :** the date of this Agreement.
       4. **Licensee :** any person or entity acquiring or using the Product under the terms of this Agreement.
       5. **Product :** Software Development Kit (SDK) that is licensed under this Agreement and that has been reproduced pursuant to this Agreement.  
       6. **Territory :** worldwide.
       7. **VAT :** value added tax or any equivalent tax chargeable in the UK or elsewhere. 

    2. A **person** includes a natural person, corporate or unincorporated body (whether having separate legal personality or not).
    3. A reference to a statute or statutory provision (i) is a reference to it as amended, extended or re-enacted from time to time, and (ii) shall include all subordinate legislation made from time to time under that statute or statutory provision.
    4. Any words following the terms **including**, **include**, **in particular**, **for example** or any similar expression shall be construed as illustrative and shall not limit the sense of the words, description, definition, phrase or term preceding those terms.

2. Grant and Limits

    1. In consideration of the sum of one pound (£1) (receipt of which the Licensor hereby acknowledges), the Licensor hereby grants to the Licensee a non-exclusive licence under the Copyright to do the following acts in the Territory for the term of this Agreement, subject to, and in accordance with, the terms of this Agreement to reproduce the Product use for the purposes of the Cooperation Agreement separately signed between the Parties.   
    2. The Licensee shall not assign, transfer, mortgage, charge, sub-license, sub-contract, delegate, declare a trust over or deal in any other manner with any or all of its rights and obligations under this Agreement without the prior written consent of the Licensor.
    3. Any modified or merged portion of the Product remains Licensor’s exclusive property and is subject to this Agreement. Except for the foregoing, Licensee are not granted any right or license to modify, improve or create derivative works of the Product, or any of the items in the Product.
    4. Except as expressly set forth in this Agreement, Licensee may not, directly or indirectly, sell, sublicense, rent, loan, share, disclose, transfer or lease the Product, or any portion of the Product, to any third party. Licensee may not, directly or indirectly, reverse engineer, decompile, disassemble or otherwise attempt to discover the source code and/or file formats of any portion of the Product. To the extent that local law grants Licensee the right to decompile software to obtain information necessary to render the software interoperable with other software, Licensee shall first request to Licensor in writing to provide Licensee with the necessary information. Licensor has the right to impose reasonable conditions on decompiling its software such as a reasonable fee for doing so. Requests for information should be directed to the Licensor at the address provided in the Product or such other address made available.
    5. In addition to all of the terms and conditions herein, you agree to comply with any rules or best practices made available in connection with the Product by Licensor from time to time. 
    6.  Notwithstanding anything to the contrary, Licensor may revoke access to the Product at any time for any reason or no reason at all (either with respect to Licensee only, or with respect to other Product licensees). 
    7.  This Agreement does not grant Licensee any rights to patents, copyrights, trade secrets, trademarks, or any other rights in respect to the items in the Product, and all rights not expressly granted are reserved by Licensor.


3. Quality Control and Marking
    1. The Licensee shall ensure that the Product is not defective in terms of workmanship, materials or otherwise.
    2. The Licensee may reproduce the Product without any substantial alteration or amendment.
    3. The Licensee shall procure that every Product sold and all descriptive literature relating to the Product be marked with a notice in the following terms:

    © Copyright SDK 2022 manufactured by Matchingham Games


4. Protection of the Copyright

    1. The Licensee shall immediately notify the Licensor in writing giving full particulars if any of the following matters come to its attention:
        1. any actual, suspected or threatened infringement of the Copyright;
        2. any claim made or threatened that the Product infringes the rights of any third party; or 
        3. any other form of attack, charge or claim to which the Copyright may be subject. 
    2. In respect of any of the matters listed in clause 4.1:
        1. the Licensor shall, at their absolute discretion, decide what action to take, if any;
        2. the Licensor shall have exclusive control over, and conduct of, all claims and proceedings;
        3. the Licensee shall not make any admissions other than to the Licensor and shall provide the Licensor with all assistance that the Licensor may reasonably require in the conduct of any claims or proceedings; and
        4. the Licensor shall be entitled to retain all sums recovered in any action for the Licensor’s own account.
    3. The provisions of sections 101 and 101A of the Copyright, Designs and Patents Act 1988 (or equivalent legislation in any jurisdiction) are expressly excluded.
    4. Nothing in this Agreement shall constitute any representation or warranty that the exercise by the Licensee of rights granted under this Agreement will not infringe the rights of any person. 


5. Moral Rights

    The Licensor, being the sole author of the Product, asserts the Licensor’s moral right under Chapter 4 of the Copyright, Designs and Patents Act 1988 to be identified as the author of the Product. 

6. Data Collection
    1. If Licensee uses the Product to run applications developed by a third party or that access data, content, or resources provided by a third party, Licensee agrees that Licensor is not responsible for those applications, data, content, or resources. Licensee understands that all data, content, or resources which Licensee may access through such third-party applications are the sole responsibility of the person from which they originated and that Licensor is not liable for any loss or damage that Licensee may experience as a result of the use or access of any of those third-party applications, data, content, or resources. Licensee agrees that, if Licensee uses the Product to develop applications for other users, Licensee will protect the privacy and legal rights of Licensee’s users, prominently display a privacy policy that describes to Licensee's users the information that is collected by Licensee and Licensee’s application and how such information is used and shared. 


7. Indemnity

    1. The Licensee shall indemnify the Licensor against all liabilities, costs, expenses, damages and losses (including any direct, indirect or consequential losses, loss of profit, loss of reputation and all interest, penalties and legal costs (calculated on a full indemnity basis) and all other reasonable professional costs and expenses) suffered or incurred by the Licensor arising out of or in connection with: 
        1. the Licensee’s exercise of its rights granted under this Agreement;
        2. the Licensee’s breach or negligent performance or non-performance of this Agreement, including any product liability claim relating to products manufactured, supplied or put into use by the Licensee;
        3. the enforcement of this Agreement; or
        4. any claim made against the Licensor by a third party for death, personal injury or damage to property arising out of or in connection with defective Products, to the extent that the defect in the Products is attributable to the acts or omissions of the Licensee, its employees or agents.
    2. If a payment due from the Licensee under this clause is subject to tax (whether by way of direct assessment or withholding at its source), the Licensor shall be entitled to receive from the Licensee such amounts as shall ensure that the net receipt, after tax, to the Licensor in respect of the payment is the same as it would have been were the payment not subject to tax.


8. Duration and Termination

    1. This Agreement is effective until terminated. 
    2. Licensor has the right to terminate this Agreement immediately, without judicial intervention, for any reason or no reason at all, including without limitation if Licensee fail to comply with any term herein or any rules or best practices made available in connection with the Product by Licensor from time to time. If Licensee wants to terminate this Agreement, Licensee may do so by ceasing the use of the Product and any relevant developer credentials. 
    3.  Upon any such termination (i) all licenses granted to Licensee with respect to the Product will immediately terminate, (ii) Licensee will immediately discontinue all distribution, support and use of the Product and (iii) Licensee must remove all full and partial copies of the items in the Product from their computers, systems and servers and discontinue use of the items in the Product
    4.  Any provision of this Agreement that expressly or by implication is intended to come into or continue in force on or after termination of this Agreement shall remain in full force and effect.


9. Disclaimer of Warranty

    1. Licensor licenses the Product to Licensee only on an “AS-IS” basis. Licensor makes no warranties or representation, express or implied, with respect to the adequacy of the Product, and any items in the Product, whether or not used by Licensee, for any particular purpose or with respect to their adequacy to produce any particular result. Licensor and its suppliers shall not be liable for loss or damage arising out of this Agreement or from use by Licensee of any devices or products containing portions of the Product. LICENSOR AND ITS SUPPLIERS DISCLAIM ALL WARRANTIES, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO IMPLIED CONDITIONS OR WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT. NO PERSON IS AUTHORIZED TO MAKE ANY OTHER WARRANTY OR REPRESENTATION CONCERNING THE PERFORMANCE OF THE PRODUCT. YOU ASSUME THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE **PRODUCT** AND ANY DESIGN OR PRODUCT IN WHICH THE **PRODUCT** MAY BE USED, INCLUDING, WITHOUT LIMITATION, ANY LICENSEE PRODUCTS.


10. Governing Law

    1.  This Agreement and any dispute or claim arising out of or in connection with it or its subject matter or formation (including non-contractual disputes or claims) shall be governed by and construed in accordance with the law of England and Wales.


11. Jurisdiction

    1.  Each party irrevocably agrees that the courts of England and Wales shall have exclusive jurisdiction to settle any dispute or claim arising out of or in connection with this Agreement or its subject matter or formation.