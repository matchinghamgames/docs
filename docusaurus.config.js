// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const {themes} = require('prism-react-renderer');
const lightTheme = themes.github;
const darkTheme = themes.dracula;
const simplePlantUML = require("@mstroppel/remark-local-plantuml");
import rehypeRaw from 'rehype-raw';
const rehypeRawOptions = {
  passThrough: [
    'mdxFlowExpression',
    'mdxJsxFlowElement',
    'mdxJsxTextElement',
    'mdxTextExpression',
    'mdxjsEsm'
  ]
};

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Matchingham Games',
  tagline: 'Matchingham SDK',
  url: 'https://matchinghamgames.gitlab.io',
  baseUrl: '/docs/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img~/favicon.ico',
  organizationName: 'matchinghamgames', // Usually your GitHub org/user name.
  projectName: 'docs', // Usually your repo name.

   plugins: [
     [
       require.resolve("@cmfcmf/docusaurus-search-local"),
       {
         indexBlog: false
       },
     ],
   ],
  
  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          routeBasePath: '/', // Serve the docs at the site's root
          sidebarPath: require.resolve('./sidebars.js'),
          remarkPlugins: [simplePlantUML],
          rehypePlugins: [[rehypeRaw, rehypeRawOptions]],
        },
        blog: false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Matchingham Games',
        logo: {
          alt: 'Matchingham Logo',
          src: 'img~/logo.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Documents',
          },
          {
            to: 'agreement',
            position: 'left',
            label: 'License of Copyright Agreement',
          },
        ],
      },
      footer: {
        style: 'dark',
        copyright: `Copyright © ${new Date().getFullYear()} Matchingham Games.`,
      },
      prism: {
        theme: lightTheme,
        darkTheme: darkTheme,
        additionalLanguages: ['csharp', 'bash', 'diff', 'json'],
      },
    }),
};

module.exports = config;
