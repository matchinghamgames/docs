# Vegas Mediation Module

## Overview
Vegas module is a proxy module for managing different mediation SDKs. Vegas also sends ad analytics (eCPM events, Impression events) via `Sherlock` module. When `Dealer` module is present, Vegas can detect if user made an in-app purchase that has `no-ads`. Most of the configuration parameters can be changed remotely when `Meteor` module is present.

## Installing
* Import package from Package Manager UI
* Import mediation service implementation what you want to use from Package Manager UI. Unlike other modules, Vegas can work with only one mediation service. But, you can swap mediation service without changing your code.
  * Make sure that the mediation service's config is correct. These are explained in [Services](#services) section.

## How to use
* Make sure to call `Vegas.Instance.Initialize()` at the appropriate place.
  * If `Meteor` and `Backpack` modules are present, `Vegas` must be initialized after these modules. 
  * After successful initialization, you can call ads however you want. Vegas supports three ad types: `Banner, Interstitial, Rewarded Video, AppOpen, Mrec`
    * For example: 
    ```csharp
    Vegas.Banner.Show();
    Vegas.Interstitial.Show();
    Vegas.RewardedVideo.Show();
    Vegas.AppOpen.Show();
	Vegas.Mrec.Show();
    ```

## Displaying Interstitials In Place of Rewarded Videos
* Enable `Fullscreen Ad Wrapper Enabled` in `Vegas Config` to use this feature.
  * By default this is set to true.
* Set `Fullscreen Ad Wrapper Interstitials Before Forcing Rewarded Video` to the number of interstitials that can be shown consecutively before forcing a rewarded video to be displayed.
  * By default this is set to 2.
* Call `VegasFullscreenAdWrapper.ShowFullscreenAd` method with proper parameters set to utilize this feature.
  * System will check if the number of interstitials shown consecutively exceeds the limit set in `Fullscreen Ad Wrapper Interstitials Before Forcing Rewarded Video` and force a rewarded video to be displayed if it does.
  * System will prefer the higher ECPM ad otherwise.

## Samples
### Using Interstitial as a Rewarded Video if Rewarded Video is not yet loaded

> [!IMPORTANT] 
> When using the below code, you will end up triggering interstitial ad events from Vegas, as you are showing an Interstitial ad. If your code
> uses global event handlers to change game state based on ad events, you should be aware of this.

```csharp
public class MyAdManager
{
    // ...
    
    public void ShowRewardedVideo()
    {
        // Check if rewarded video is loaded using Vegas SDK API
        if (Vegas.RewardedVideo.IsLoaded)
        {
            // Show a rewarded video using Vegas SDK API
            Vegas.RewardedVideo.Show();
        }
        else
        {
            // Show an interstitial using Vegas SDK API
            Vegas.Interstitial.Show();
        }
    }
    
    // ...
}
```

### 

## Configuration
Go to > `Matchingham > Mediation > Config`

| Variable                                                          | Remote Name                                   | Description                                                                                                                                                                                                                                                                                                                                        |
|-------------------------------------------------------------------|-----------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Enabled                                                           | adsEnabled                                    | Enabling/disabling the module                                                                                                                                                                                                                                                                                                                      |
| AutoInitialize                                                    | N/A                                           | If enabled, you don't need to call Initialize method manually.                                                                                                                                                                                                                                                                                     |
| No Ads Item                                                       | N/A                                           | If you are using `Dealer` and `Backpack` modules, place your `no-ads` item here.                                                                                                                                                                                                                                                                   |
| Auto Load Banner After Init                                       | autoLoadBannerAfterInit                       | When Vegas is initialized, Vegas will start loading `Banner` immediately.                                                                                                                                                                                                                                                                          |
| Auto Load Interstitial After Init                                 | autoLoadInterstitialAfterInit                 | When Vegas is initialized, Vegas will start loading `Interstitial` immediately.                                                                                                                                                                                                                                                                    |
| Auto Load Rewarded Video After Init                               | autoLoadRewardedVideoAfterInit                | When Vegas is initialized, Vegas will start loading `Rewarded Video` immediately.                                                                                                                                                                                                                                                                  |
| Auto Load App Open After Init                                     | autoLoadAppOpenAfterInit                      | When Vegas is initialized, Vegas will start loading `App Open` immediately.                                                                                                                                                                                                                                                                        |
| Auto Load Mrec After Init                                         | autoLoadMrecAfterInit                         | When Vegas is initialized, Vegas will start loading `Mrec` immediately.                                                                                                                                                                                                                                                                            |
| Auto Show Banner After Load                                       | autoShowBannerAfterLoad                       | When `Banner` is successfully loaded, Vegas will show it immediately.                                                                                                                                                                                                                                                                              |
| Auto Load Interstitial After Close                                | autoLoadInterstitialAfterClose                | When `Interstitial` is closed, Vegas will load another one immediately.                                                                                                                                                                                                                                                                            |
| Auto Load Rewarded Video After Close                              | autoLoadRewardedVideoAfterClose               | When `Rewarded Video` is closed, Vegas will load another one immediately.                                                                                                                                                                                                                                                                          |
| Auto Load App Open After Close                                    | autoLoadAppOpenAfterClose                     | When `App Open` is closed, Vegas will load another one immediately.                                                                                                                                                                                                                                                                                |
| First Interstitial Show Cap                                       | firstInterstitialShowCap                      | When players open the game for the first time, they won't be able to see `Interstitial` for this seconds.                                                                                                                                                                                                                                          |
| Interstitial Show Cap                                             | interstitialShowCap                           | When players close `Interstital`, they won't be able to see `Interstitial` for this seconds. This applies **after** the user closes `Interstital`                                                                                                                                                                                                  | 
| First App Open Show Cap                                           | firstAppOpenShowCap                           | When players open the game for the first time, they won't be able to see `App Open` for this seconds.                                                                                                                                                                                                                                              |
| App Open Show Cap                                                 | appOpenShowCap                                | When players close `App Open`, they won't be able to see `App Open` for this seconds. This applies **after** the user closes `App Open`                                                                                                                                                                                                            |
| Banner Ecpm Event Thresholds                                      | N/A                                           | For filtering banner eCPM events. <br/>**You have to fill sherlock events list for CPM events as well. Their name have to be like this: ** `ecpmGreaterThanX` (X = ecpm value) <br/>For example: ``` ecpmGreaterThan100 ecpmGreaterThan500 ecpmGreaterThan1000```                                                                                  |
| Interstitial Ecpm Event Thresholds                                | N/A                                           | For filtering interstitial eCPM events. <br/>**You have to fill sherlock events list for CPM events as well. Their name have to be like this: ** `ecpmGreaterThanX` (X = ecpm value) <br/>For example: ``` ecpmGreaterThan100 ecpmGreaterThan500 ecpmGreaterThan1000```                                                                            |
| Rewarded Video Ecpm Event Thresholds                              | N/A                                           | For filtering rewarded video eCPM events. <br/>**You have to fill sherlock events list for CPM events as well. Their name have to be like this: ** `ecpmGreaterThanX` (X = ecpm value) <br/>For example: ``` ecpmGreaterThan100 ecpmGreaterThan500 ecpmGreaterThan1000```                                                                          |
| App Open Ecpm Event Thresholds                                    | N/A                                           | For filtering app open eCPM events. <br/>**You have to fill sherlock events list for CPM events as well. Their name have to be like this: ** `ecpmGreaterThanX` (X = ecpm value) <br/>For example: ``` ecpmGreaterThan100 ecpmGreaterThan500 ecpmGreaterThan1000```                                                                                |
| Mrec Ecpm Event Thresholds                                        | N/A                                           | For filtering Mrec eCPM events. <br/>**You have to fill sherlock events list for CPM events as well. Their name have to be like this: ** `ecpmGreaterThanX` (X = ecpm value) <br/>For example: ``` ecpmGreaterThan100 ecpmGreaterThan500 ecpmGreaterThan1000```                                                                                    |
| Cumulative Ecpm Events                                            | N/A                                           | If enabled, when an ad's eCPM value is higher than more than one threshold, all of their events will be triggered.                                                                                                                                                                                                                                 |
| Max Ecpm Threshold                                                | N/A                                           | When an ad has this much ecpm, Vegas won't send any eCPM events                                                                                                                                                                                                                                                                                    |
| Send Ecpm Events To Adjust                                        | sendEcpmEventsToAdjust                        | Enabling/disabling sending eCPM events to Adjust                                                                                                                                                                                                                                                                                                   |
| Send Ad Revenue Events To Adjust                                  | sendAdRevenueEventsToAdjust                   | Enabling/disabling sending Ad Impression events to Adjust                                                                                                                                                                                                                                                                                          |
| Send Ecpm Events To Firebase                                      | sendEcpmEventsToFirebase                      | Enabling/disabling sending eCPM events to Firebase                                                                                                                                                                                                                                                                                                 |
| Send Ad Revenue Events To Firebase                                | sendAdRevenueEventsToFirebase                 | Enabling/disabling sending Ad Impression events to Firebase                                                                                                                                                                                                                                                                                        |
| Send Ecpm Events To GameAnalytics                                 | sendEcpmEventsToGameAnalytics                 | Enabling/disabling sending eCPM events to GameAnalytics                                                                                                                                                                                                                                                                                            |
| Send Ad Revenue Events To GameAnalytics                           | sendAdRevenueEventsToGameAnalytics            | Enabling/disabling sending Ad Impression events to GameAnalytics                                                                                                                                                                                                                                                                                   |
| Send Ad Revenue Events To Facebook                                | sendAdRevenueEventsToFacebook                 | Enabling/disabling sending Ad Impression events to Facebook (Banner is not included)                                                                                                                                                                                                                                                               |
| Send Banner Ad Revenue Events To Facebook                         | sendBannerAdRevenueEventsToFacebook           | If you want to include banners while sending ad impression events to Facebook                                                                                                                                                                                                                                                                      |
| Banner Cumulative Revenue Event Thresholds                        | N/A                                           | For filtering banner cumulative revenue events. <br/>**You have to fill sherlock events list for Cumulative Revenue Events as well. Their name have to be like this: ** `ecpmCumulativeGreaterThanX` (X = revenue in cents) <br/>For example: ``` ecpmCumulativeGreaterThan5 ecpmCumulativeGreaterThan50 ecpmCumulativeGreaterThan100```           |
| Interstitial Cumulative Revenue Event Thresholds                  | N/A                                           | For filtering interstitial cumulative revenue events. <br/>**You have to fill sherlock events list for Cumulative Revenue Events as well. Their name have to be like this: ** `ecpmCumulativeGreaterThanX` (X = revenue in cents) <br/>For example: ``` ecpmCumulativeGreaterThan5 ecpmCumulativeGreaterThan50 ecpmCumulativeGreaterThan100```     |
| Rewarded Video Cumulative Revenue Event Thresholds                | N/A                                           | For filtering rewarded video cumulative revenue events. <br/>**You have to fill sherlock events list for Cumulative Revenue Events as well. Their name have to be like this: ** `ecpmCumulativeGreaterThanX` (X = revenue in cents) <br/>For example: ``` ecpmCumulativeGreaterThan5 ecpmCumulativeGreaterThan50 ecpmCumulativeGreaterThan100```   |
| App Open Cumulative Revenue Event Thresholds                      | N/A                                           | For filtering app open cumulative revenue events. <br/>**You have to fill sherlock events list for Cumulative Revenue Events as well. Their name have to be like this: ** `ecpmCumulativeGreaterThanX` (X = revenue in cents) <br/>For example: ``` ecpmCumulativeGreaterThan5 ecpmCumulativeGreaterThan50 ecpmCumulativeGreaterThan100```         |
| Mrec Cumulative Revenue Event Thresholds                          | N/A                                           | For filtering Mrec cumulative revenue events. <br/>**You have to fill sherlock events list for Cumulative Revenue Events as well. Their name have to be like this: ** `ecpmCumulativeGreaterThanX` (X = revenue in cents) <br/>For example: ``` ecpmCumulativeGreaterThan5 ecpmCumulativeGreaterThan50 ecpmCumulativeGreaterThan100```             |
| Send Cumulative Revenue Events To Firebase                        | sendCumulativeRevenueEventsToFirebase         | Enabling/disabling sending Cumulative Revenue events to Firebase                                                                                                                                                                                                                                                                                   |
| Send Cumulative Revenue Events To Adjust                          | sendCumulativeRevenueEventsToAdjust           | Enabling/disabling sending Cumulative Revenue events to Adjust                                                                                                                                                                                                                                                                                     |
| Send Cumulative Revenue Events To GameAnalytics                   | sendCumulativeRevenueEventsToGameAnalytics    | Enabling/disabling sending Cumulative Revenue events to GameAnalytics                                                                                                                                                                                                                                                                              |
| Custom Banner Revenue Events To Firebase                          | N/A                                           | If you want to send custom banner revenue data to Firebase with a given name, add it to this list                                                                                                                                                                                                                                                  |
| Custom Interstitial Revenue Events To Firebase                    | N/A                                           | If you want to send custom interstitial revenue data to Firebase with a given name, add it to this list                                                                                                                                                                                                                                            |
| Custom Rewarded Video Revenue Events To Firebase                  | N/A                                           | If you want to send custom rewarded video revenue data to Firebase with a given name, add it to this list                                                                                                                                                                                                                                          |
| Custom AppOpen Revenue Events To Firebase                         | N/A                                           | If you want to send custom app open revenue data to Firebase with a given name, add it to this list                                                                                                                                                                                                                                                |
| Custom Mrec Revenue Events To Firebase                            | N/A                                           | If you want to send custom Mrec revenue data to Firebase with a given name, add it to this list                                                                                                                                                                                                                                                    |
| Open Mediation Debugger On Test Devices                           | openMediationDebuggerOnTestDevices            | Enabling/disabling opening mediation debugger when game opens                                                                                                                                                                                                                                                                                      |
| Test Device Ids                                                   | adsTestDevices                                | AD IDs (GAID for Google Play Store, IDFA for Apple App Store) for mediation debugger authentication.                                                                                                                                                                                                                                               |
| Ad Impression Count Event Thresholds                              | N/A                                           | For filtering ad impression count events. Only valid for rewarded and interstitials. <br/>**You have to fill sherlock events list for ad impression count events as well. Their name have to be like this: ** `adimpsxXX` (XX = threshold value) <br/>For example: ``` adimpsx30 adimpsx50```                                                      |
| Fullscreen Ad Wrapper Enabled                                     | fullscreenAdWrapperEnabled                    | Fullscreen ad wrapper feature enabled (When disabled, methods can still be called but will fallback to regular rewarded video requests)                                                                                                                                                                                                            |
| Fullscreen Ad Wrapper Interstitials Before Forcing Rewarded Video | fullscreenAdWrapperIntersBeforeForcedRewarded | Number of interstitials that can be shown consequentially. If the sequence exceeds this number, system will force a rewarded video to be displayed, resetting the counter.                                                                                                                                                                         |

## API & Details

### Common

#### Methods

* **Initialize()**
: Starts module initialization. You need to call this at the appropriate place.

* **WhenInitialized(Action callback)**
: Allows you to register `callback` that will be fired only after the module is successfully initialized. Use this to execute logic that requires this module to be initialized first. If the module has already initialized, immediately invokes the callback.

* **WhenFailedToInitialize(Action callback)**
: Allows you to register `callback` that will be fired only after the module fails to initialize for any reason. Use this to handle what should happen in case this module fails to initialize. If the module has already failed to initialize, immediately invokes the callback.

* **WhenReady(Action callback)**
: Combined version of `WhenInitialized` and `WhenFailedToInitialize`. Delays execution of `callback` till module is first initialized or failed to initialize, immediately invoke the callback if it is already initialized or failed to initialize.

* **SetNoAdsActive()**
: When not using `Backpack` package, you need to manually set the no ads state. Track the status of the no ads purchase, and call this method when no ads should be turned off. This call must be made each runtime session. 

#### Fields

* **State**
: Initialization state of the module

* **Instance**
: Instance of the module

* **LateInitialized**
: When the module needs an internet connection but the player became online while playing the game, this becomes `true`

* **Ready**
: If the module is initialized successfully and ready to operate

* **Config**
: Configuration of the module. See [configuration](#configuration)

* **InitializationDuration**
: Initialization duration in seconds

### Banner
* By default, banner is loaded after initialization and shown after being successfully loaded. You can change it on `Vegas Config`.

#### Methods

* **Load()**
: Loads banner manually.

* **Show(string adTag)**
: Shows banner manually. Use it if you need to hide it before. `adTag`: If you have additional tag for it, you can enter. Default is null.

* **Hide()**
: Hides banner.

* **Destroy()**
: Destroys banner.

#### Events

* **Loaded**
: Invokes when banner is successfully loaded.

* **FailedToLoad**
: Invokes when banner couldn't be loaded.

* **Shown**
: Invokes when banner is on screen.

* **Hidden**
: Invokes when banner becomes hidden.

* **Clicked**
: Invokes when user clicks the banner.

* **Destroyed**
: Invokes when banner is destroyed

* **LoadedStatusChanged**
: Invokes when banner availability for display is changed.

* **AdDataObtained(AdDTO)**
: Invokes when a banner data is obtained.

* **RevenueDataObtained(AdDTO, AdRevenueDTO)**
: Invokes when a banner revenue data is obtained.

* **ErrorDataObtained(AdData, AdErrorDTO)**
: Invokes when a banner error data is obtained.

#### Fields

* **Enabled**
: If banner is enabled

* **IsShowing**
: If banner is on screen

* **IsLoaded**
: If banner is loaded

* **IsLoading**
: If banner is loading

* **IsDestroyed**
: If banner is destroyed

* **RetryAttempt**
: Attempt count to reload banner

* **Data**
: Data of the loaded banner. See [AdDTO](#addto)

* **RevenueData**
: Revenue data of the loaded banner. See [AdRevenueDTO](#adrevenuedto)

* **ErrorData**
: Error data of the failed banner. See [AdErrorDTO](#aderrordto)

### Interstitial
* By default, interstitials are loaded after initialization. You can change it on `Vegas Config`.

#### Methods

* **Load()**
: Loads interstitial manually.

* **Show(string adTag)**
: Shows Interstitial. `adTag`: If you have additional tag for it, you can enter. Default is null.

#### Events

* **Loaded**
: Invokes when interstitial is loaded

* **FailedToLoad**
: Invokes when interstitial couldn't be loaded

* **Shown**
: Invokes when interstitial is started

* **FailedToShow**
: Invokes when interstitial couldn't start

* **Clicked**
: Invokes when the user clicks interstitial

* **Closed**
: Invokes when interstitial is closed

* **Capped**
: Invokes when interstitial is capped

* **LoadedStatusChanged**
: Invokes when interstitial availability is changed

* **AdDataObtained(AdDTO)**
: Invokes when an interstitial data is obtained.

* **RevenueDataObtained(AdDTO, AdRevenueDTO)**
: Invokes when an interstitial revenue data is obtained

* **ErrorDataObtained(AdDTO, AdErrorDTO)**
: Invokes when an interstitial error data is obtained.

#### Fields

* **Enabled**
: If interstitial is enabled

* **IsShowing**
: If interstitial is on screen

* **IsLoaded**
: If interstitial is loaded

* **IsLoading**
: If interstitial is loading

* **IsCapped**
: If interstitial is capped due to cap time

* **Cooldown**
: How many seconds left for enabling to show interstitial

* **NextShowTime**
: Exact time when interstitial can be shown

* **RetryAttempt**
: Attempt count to reload interstitial

* **Data**
: Data of the loaded interstitial. See [AdDTO](#addto)

* **RevenueData**
: Revenue data of the loaded interstitial. See [AdRevenueDTO](#adrevenuedto)

* **ErrorData**
: Error data of the failed interstitial. See [AdErrorDTO](#aderrordto)

### Rewarded Video
* By default, rewarded videos are loaded after initialization. You can change it on `Vegas Config`.

#### Methods

* **Load()**
: Loads rewarded video manually

* **Show(string adTag)**
: Shows rewarded video. `adTag`: If you have additional tag for it, you can enter. Default is null.

* **Show(Action onSuccessCallback, Action onFailCallback, string adTag)**
: Shows rewarded video. `onSuccessCallback`: Will be invoked if the user watches rewarded video. (e.g. giving reward) `onFailCallback`: Will be invoked if the user fails watching rewarded video. `adTag`: If you have additional tag for it, you can enter. Default is null.

#### Events

* **Loaded**
: Invokes when rewarded video is loaded

* **FailedToLoad**
: Invokes when rewarded video couldn't be loaded

* **Shown**
: Invokes when rewarded video is started

* **FailedToShow**
: Invokes when rewarded video couldn't start

* **Clicked**
: Invokes when the user clicks rewarded video

* **Closed(bool result)**
: Invokes when rewarded video is closed. `result`: If the user watched rewarded video successfully

* **LoadedStatusChanged**
: Invokes when rewarded video availability is changed

* **AdDataObtained(AdDTO)**
: Invokes when a rewarded video data is obtained.

* **RevenueDataObtained(AdDTO, AdRevenueDTO)**
: Invokes when a rewarded video revenue data is obtained

* **ErrorDataObtained(AdDTO, AdErrorDTO)**
: Invokes when a rewarded video error data is obtained.

#### Fields

* **Enabled**
: If rewarded video is enabled

* **IsShowing**
: If rewarded video is on screen

* **IsLoaded**
: If rewarded video is loaded

* **IsLoading**
: If rewarded video is loading

* **Data**
: Data of the loaded rewarded video. See [AdDTO](#addto)

* **RevenueData**
: Revenue data of the loaded rewarded video. See [AdRevenueDTO](#adrevenuedto)

* **ErrorData**
: Error data of the failed rewarded video. See [AdErrorDTO](#aderrordto)

### AppOpen
* By default, app opens are loaded after initialization. You can change it on `Vegas Config`.

#### Methods

* **Load()**
: Loads app open manually.

* **Show(string adTag)**
: Shows AppOpen. `adTag`: If you have additional tag for it, you can enter. Default is null.

#### Fields

* **IsShowing**
: If app open is on screen

* **IsLoaded**
: If app open is loaded

* **IsLoading**
: If app open is loading

* **IsCapped**
: If app open is capped due to cap time

* **Cooldown**
: How many seconds left for enabling to show app open

* **NextShowTime**
: Exact time when app open can be shown

* **Data**
: Data of the loaded app open. See [AdDTO](#addto)

* **RevenueData**
: Revenue data of the loaded app open. See [AdRevenueDTO](#adrevenuedto)

* **ErrorData**
: Error data of the failed app open. See [AdErrorDTO](#aderrordto)

#### Events

* **Loaded**
: Invokes when app open is loaded

* **FailedToLoad**
: Invokes when app open couldn't be loaded

* **Shown**
: Invokes when app open is started

* **FailedToShow**
: Invokes when app open couldn't start

* **Clicked**
: Invokes when the user clicks app open

* **Closed**
: Invokes when app open is closed

* **Capped**
: Invokes when app open is capped

* **LoadedStatusChanged**
: Invokes when app open availability is changed

* **AdDataObtained(AdDTO)**
: Invokes when an app open data is obtained.

* **RevenueDataObtained(AdDTO, AdRevenueDTO)**
: Invokes when an app open revenue data is obtained.

* **ErrorDataObtained(AdDTO, AdErrorDTO)**
: Invokes when an app open error data is obtained.

### MREC
* By default, Mrec is loaded after initialization. You can change it on `Vegas Config`.
* You should call Vegas.Mrec.Show() to show Mrec ads. Mrec ads is positioned at the center of the screen (0,0) as a 300x250 (widthxheight) rectangle.

#### Methods

* **Load()**
: Loads Mrec manually.

* **Show(string adTag)**
: Shows Mrec manually. `adTag`: If you have additional tag for it, you can enter. Default is null.

* **Hide()**
: Hides Mrec.

* **Destroy()**
: Destroys Mrec.

#### Events

* **Loaded**
: Invokes when Mrec is successfully loaded.

* **FailedToLoad**
: Invokes when Mrec couldn't be loaded.

* **Shown**
: Invokes when Mrec is on screen.

* **Hidden**
: Invokes when Mrec becomes hidden.

* **Clicked**
: Invokes when user clicks the Mrec.

* **Destroyed**
: Invokes when Mrec is destroyed

* **LoadedStatusChanged**
: Invokes when Mrec availability for display is changed.

* **AdDataObtained(AdDTO)**
: Invokes when a Mrec data is obtained.

* **RevenueDataObtained(AdDTO, AdRevenueDTO)**
: Invokes when a Mrec revenue data is obtained.

* **ErrorDataObtained(AdData, AdErrorDTO)**
: Invokes when a Mrec error data is obtained.

#### Fields

* **Enabled**
: If Mrec is enabled

* **IsShowing**
: If Mrec is on screen

* **IsLoaded**
: If Mrec is loaded

* **IsLoading**
: If Mrec is loading

* **IsDestroyed**
: If Mrec is destroyed

* **RetryAttempt**
: Attempt count to reload Mrec

* **Data**
: Data of the loaded Mrec. See [AdDTO](#addto)

* **RevenueData**
: Revenue data of the loaded Mrec. See [AdRevenueDTO](#adrevenuedto)

* **ErrorData**
: Error data of the failed Mrec. See [AdErrorDTO](#aderrordto)

### Others

* **FirstSession**
: If the user opens the game for the first time.

* **IsTesterDevice**
: If the user is tester.

#### User Consent
If `Data Usage Consent Module` is **not** added, you must **manually** set if the user has given consent for data processing or the user is in a country that has a regulation for data processing. 

##### Fields

* **UserConsentGiven**
: `true` if the user has given consent to process data; `false` if otherwise; `null` if unknown

* **IsGDPRRequired**
: `true` if the user is in GDPR required country; `false` if otherwise; `null` if unknown

* **IsCCPARequired**
: `true` if the user is in CCPA required state; `false` otherwise, `null` if unknown

#### eCPM

##### Methods

* **CheckAndSendEcpmAdjustEvent(AdDTO adDTO, double ecpmEventThreshold, string eventName)**
: If `adDTO.EcpmInDollar` is higher `ecpmEventThreshold`, it will trigger an event named as `eventName` on Adjust. **`Sherlock - Adjust` package must be added in project.** This method is in under `VegasUtility` class.

* **CheckAndSendEcpmFirebaseEvent(AdDTO adDTO, double ecpmEventThreshold, string eventName)**
: If `adDTO.EcpmInDollar` is higher `ecpmEventThreshold`, it will trigger an event named as `eventName` on Firebase. **`Sherlock - Firebase` package must be added in project.** This method is in under `VegasUtility` class.

* **CheckAndSendEcpmGameAnalyticsEvent(AdDTO adDTO, double ecpmEventThreshold, string eventName)**
: If `adDTO.EcpmInDollar` is higher `ecpmEventThreshold`, it will trigger an event named as `eventName` on Game Analytics. **`Sherlock - Game Analytics` package must be added in project.** This method is in under `VegasUtility` class.

#### Classes

##### AdDTO

* **Id**
: Ad unit Identifier

* **Format**
: Ad format (e.g. BANNER, INTER, REWARDED, MREC)

* **Tag**
: Placement tags

* **LoadingDuration**
: Loading duration of the ad in seconds. (StartLoadingTime and EndLoadingTime MUST BE SET!)

* **MediationSource**
: Which mediation SDK is serving the ads. (e.g. admost, applovin)

* **Source**
: Source of the ad (e.g. unity, vungle, admob, etc.)

* **CreativeId**
: Used for send data from applovin to game analytics

* **Country**
: Used for send data from applovin to game analytics

* **StartLoadingTime**
: When the ad is requested

* **EndLoadingTime**
: When the ad request is finished

* **WaterfallLatency**
: Ad request's waterfall latency

* **NetworkPlacement**
: Placement name of the external ad network

* **RawData**
: JSON data of the ad data

##### AdRevenueDTO

* **Revenue**
: Revenue of the ad in dollars

* **Ecpm**
: Ecpm value of the ad in cents

* **EcpmInDollar**
: Ecpm value of the ad in dollars

##### AdErrorDTO

* **Reason**
: Reason of the error. Mostly its just an error code. For more information, look at the meditation sdk's docs.

* **OpType**
: When the ad gets this error.

### Debugging Mediation
Like all modules, Vegas provides diagnostic tools via SRDebugger. You can manually load ads and see the details of the ads and also the parameters of `VegasConfig`.

#### Methods

* **OpenDebugMenu()**
: Opens mediation service's debug menu manually.

## Services

### Dummy Mediation
Mediation services won't work on editor. Thus, we made a dummy one, so you can test mediation-related scenarios on editor. To set them, you can go to `Matchingham > Mediation > Services > Dummy Mediation Config` Also, if you want to build your project with dummy service, you must add `MG_VEGAS_DUMMY` to Scripting Define Symbols.

### Applovin MAX Mediation Service
After installing `Vegas-Applovin`, you can go to `Applovin > Integration Manager` and add adapters. **Do not enable `Enable MAX Terms and Privacy Policy Flow`**
After installing our package, go to `Matchingham > Mediation > Services > Applovin MAX Mediation Config`

![Applovin MAX Config](/vegas-ads/vegas-applovin-service.png)

Make sure that, all the fields are filled with correct information. To save them go to: `File > Save Project` 

#### Special cases for Adapters

##### Amazon
Unfortunately, Amazon adapter installation is quite complicated compared to other adapters:
1. Download Amazon APS SDK https://mdtb-sdk-packages.s3.us-west-2.amazonaws.com/unity_aps.zip
2. Extract ZIP and import `APS_Core_x_y_z.unitypackage`
3. Go to `Amazon > Manage SDK`. Install `AppLovin MAX`
4. Go to `Matchingham > Mediation > Services > Applovin MAX Mediation Config`. Make sure that all the fields are filled with correct information.
5. Go to `Applovin > Integration Manager`. Make sure that `Other Settings > Add Amazon Publisher Services SKAdNetworkID's` is enabled. 

### Admost Mediation Service
Nearly all mediation SDKs has its own integration manager for enabling ad networks and adding their adapters. But Admost doesn't. Thus, we made our own integration manager for Admost to make it easier to deal with it. Once you added it from Package Manager UI, you can go to `Matchingham > Mediation > Services > Admost Integration Helper`. 

![Admost Config](/vegas-ads/vegas-admost-service.png)

* Make sure that all fields are filled with correct information. After that, press `Save` button to save settings.
* AMR Adapter is mandatory. Thus, after installing it, you 
can see other adapters.
* If `Data Usage Consent Module` is not added, if you want to set UserConsent values, you need to use `MGAdmostBridge` class and disable `Auto Initialize` on `AdmostConfig` and manually set these values then manually initialize `MGAdmostBridge`.

```csharp
MGAdmostBridge.UserConsentGiven
MGAdmostBridge.IsGDPRRequired
MGAdmostBridge.IsCCPARequired
```

#### Special cases for Adapters

##### Admob
* If you added `Admob` adapter, make sure that `Admob Application Id Ios` and `Admob Application Id Android` fields are filled. **Otherwise, application will crash at launch.**

##### Applovin
* If you added `Applovin` adapter, make sure that `Applovin Sdk Key` is filled. **Otherwise, application will crash at launch.**

## Frequently Asked Questions
* > I'm getting this log: "Meteor is required for Vegas. Will continue after Meteor initialization"
  * You're getting this log because `Meteor` is present in the project and isn't initialized before Vegas. Vegas is using Meteor for its configuration.
* > I'm getting this log: "Backpack is required for Vegas. Will continue after Meteor initialization"
  * You're getting this log because `Backpack` is present in the project and isn't initialized before Vegas. Vegas is using Backpack for its no-ads purchase detection.
* > I'm getting this exception on device: "There isn't any vegas service registered. Please import a vegas service" 
  * It means there is no Vegas Service installed. **If your intention is simulating the ads**, you can use `Dummy Mediation` by adding `MG_VEGAS_DUMMY` scripting define symbol to `Player Settings` 
* > My game has only rewarded videos
  * If you disable `Auto Load Banner After Init` and `Auto Load Interstitial After Init`, Banner and Interstitial won't be loaded unless you **manually** load them.
* > I want to disable showing interstitial on first session only for a while
  * `First Interstitial Show Cap` is here for this reason. **Remember, this timer starts working after Vegas initialization, not game scene opening**
* > I want to disable showing interstitial after closing for a while
  * `Interstitial Show Cap` is here for this reason.
* > I'm getting this error log: "Can't show interstitial. Wait 42 seconds..."
  * It means there is a interstitial cooldown.
* > If user became online while playing game, will Vegas work?
  * Yes
* > If user became offline while playing game, will Vegas handle it?
  * Yes
* > I can't see ads.
  * There might be several reasons. Thus, you need to see the logs. There some examples:
    * If you see "NO FILL" on error logs, it means it's working but mediation source does not give you any ads.
    * Vegas will try loading banner `Banner Reload Try Count` times after `Banner Reload Delay`. After that, Vegas won't try to load banner. This is also applies for interstitial and rewarded videos as well.
* > Ads loading time is very long
  * Well, it's not about Vegas' implementation. Contact with mediation source.