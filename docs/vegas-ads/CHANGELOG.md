# Changelog

## [16.4.0] - 2025-03-07
### Added
* `ad_shown` event with `ad_platform`, `ad_source`, `ad_unit_name`, `ad_format`, `ad_tag`, `ad_creative_id`, `ad_loading_duration` parameters for all ads. 
### Fixed
* `VegasUtility.SendAdAction` won't be called in editor
* `FailureReason` wasn't set properly when the service failed to initialize

## [16.3.1] - 2025-03-06
### Changed
* Rewarded video app open, interstitial and rewarded video start display/end display times are assumed to be provided by the service.

## [16.3.0] - 2025-02-21
### Added
* `AdDTO`: `StartDisplayingTime`
* `AdDTO`: `EndDisplayingTime`
* `AdDTO`: `DisplayDuration`
* `ad_watched` event with `ad_platform`, `ad_source`, `ad_unit_name`, `ad_format`, `ad_tag`, `ad_creative_id`, `ad_loading_duration`, `ad_display_duration` parameters for interstitial, rewarded video and app open ads.

## [16.2.0] - 2025-02-13
### Changed
* If there is a duplicate revenue issue happened, `ad_revenue_duplicate` event will be sent to Firebase, instead of `ad_error_data` event
### Deprecated
* `AdErrorDTO`: `AdOpError.RevenueDuplicate`

## [16.1.3] - 2025-01-02
### Fixed
* `NoAds` item undefined case bug fix.

## [16.1.2] - 2024-12-11
### Added
* `ad_creative_id` parameter to advertisement impression events sent to firebase.

## [16.1.1] - 2024-12-11
### Added
* `ad_creative_id` parameter to advertisement info events sent to firebase.
### Fixed
* Moved new line characters to the outside of brackets in certain cases inside `ValidateExtraParameters`.

## [16.1.0] - 2024-11-22
### Added
* Firebase Ad Impression event: `ad_tag`

## [16.0.0] - 2024-11-11
### Added
* Sending ad revenue events to Facebook logic (banner is optional) (see `Migration Guide`)
* `Config.SendAdRevenueEventsToFacebook`, `Config.SendBannerAdRevenueEventsToFacebook`
### Removed
* `Sherlock` v11.0.0 and below compatiblity (see `Migration Guide`)
* `Sherlock-Adjust` v5.4.0 and below compatiblity (see `Migration Guide`)
### Migration Guide
* Update `Sherlock` and `Sherlock-Adjust` to the latest version
* To be able to send ad revenue to Facebook, you need to install `Sherlock-Facebook`

## [15.0.2] - 2024-10-25
### Added
* Documentation header for `VegasConfig.cs` explaining how `VegasConfig.Validations.cs` should be used

## [15.0.1] - 2024-10-25
### Added
* `VegasConfig.Validations.cs` to contain validation methods for `VegasConfig`

## [15.0.0] - 2024-09-17
### Added
* Default 300 and 500 cumulative revenue event thresholds.
### Changed
* Missing Cumulative Revenue Count events alert level changed to warning.
* _(Internal)_ `BannerAd.OnBannerFailedToLoad`: Using async/await instead of `MEC`
* _(Internal)_ `InterstitialAd.OnInterstitialFailedToLoad`: Using async/await instead of `MEC`
* _(Internal)_ `RewardedVideoAd.OnRewardedVideoFailedToLoad`: Using async/await instead of `MEC`
* _(Internal)_ `AppOpenAd.OnAppOpenFailedToLoad`: Using async/await instead of `MEC`
* _(Internal)_ `MrecAd.OnMrecFailedToLoad`: Using async/await instead of `MEC`
* Minimum Unity version is 2022.3 now
* Handyman dependency: v5.8.2

## [14.1.5] - 2024-05-15
### Fixed
* Fixed VegasFullScreenAdWrapper.ShowFullscreenAd method causing unwanted callbacks

## [14.1.4] - 2024-05-14
### Fixed
* Fixed an exception while sending Firebase event with rewarded_video_loaded parameter

## [14.1.3] - 2024-05-13
### Added
* `MG_SHERLOCK` symbol requirement for validation method link in `VegasConfig.cs`
* `MG_SHERLOCK` symbol requirement for `ProcessAndSendImpressionCountEvents() : void` method body compilation 

## [14.1.2] - 2024-05-09
### Changed
* DateTime references converted to DateTimeUtility

## [14.1.1] - 2024-05-09
### Changed
* Interstitial/AppOpen NextShowDate is fetched from unbiased time if it is available
* Rewarded video interstitial cap calculation uses unbiased time if available

## [14.1.0] - 2024-05-06
### Changed
* These values in `VegasConfig` are no longer remote: `BannerEcpmEventThresholds`, `BannerCumulativeRevenueEventThresholds`, `CustomBannerRevenueEventsToFirebase`, `InterstitialEcpmEventThresholds`, `InterstitialCumulativeRevenueEventThresholds`, `CustomInterstitialRevenueEventsToFirebase`, `RewardedVideoEcpmEventThresholds`, `RewardedVideoCumulativeRevenueEventThresholds`, `CustomRewardedVideoRevenueEventsToFirebase`, `AppOpenEcpmEventThresholds`, `AppOpenCumulativeRevenueEventThresholds`, `CustomAppOpenRevenueEventsToFirebase`, `MrecEcpmEventThresholds`, `MrecCumulativeRevenueEventThresholds`, `CustomMrecRevenueEventsToFirebase`, `CumulativeEcpmEvents`, `MaxEcpmThreshold`, `AdImpressionCountEventThresholds`
* Handyman dependency v5.5.0

## [14.0.0] - 2024-05-02
### Added
* `FullscreenAdWrapper`: Adds a layer to handle displaying interstitials or other full screen ads in place of rewarded videos, based on ecpm.
* `FullscreenAdWrapperEnabled`: Setting to enable or disable `FullscreenAdWrapper` feature. When disabled, requests to its API will fallback to regular Rewarded Video counterparts. (Remote setting: `fullscreenAdWrapperEnabled`)
* `FullscreenAdWrapperIntersBeforeForcedRewarded`: Setting to set number of interstitials that can be shown consequentially before system forces a rewarded video. (Remote setting: `fullscreenAdWrapperIntersBeforeForcedRewarded`)
* `Ecpm` property to `AdData` for ecpm value of the ad.
* `SetRevenue(double)` API to `AdData` for setting revenue manually. This converts the input revenue provided by the network to ECPM.
* Custom analytics event `ad_format_repurpose` to `VegasFullscreenAdWrapper.ShowFullscreenAd` method. This event will be sent when the API picks interstitial ad. You need `Sherlock Firebase` for this feature.
* Documentation section explaining how to show Interstitial Ad, in place of a Rewarded Video, if Rewarded Video is not loaded.
### Changed
* Signature `Shown` event for ads now includes `AdDTO` parameter.
### Migration Guide
* API change might cause compile errors. Please update your code to use the new API. Most notably, `Shown` event for ads now includes `AdDTO` parameter. If you are using this event, you need to update your code to use the new signature.
* To utilize the new system, you can check the docs. Summary: Enable `FullscreenAdWrapperEnabled` and adjust `FullscreenAdWrapperIntersBeforeForcedRewarded` for your needs. Instead of `Vegas.RewardedVideo.Show()` use `Vegas.FullscreenAdWrapper.ShowRewardedVideo()`. Instead of `Vegas.Interstitial.Show` use `VegasFullscreenAdWrapper.ShowFullscreenAd`.

## [14.0.0-preview.4] - 2024-04-01
### Fixed
* Fixed an exception in an event handler when `VegasFullScreenAdWrapper` is initialized in the editor.

## [14.0.0-preview.3] - 2024-03-25
### Fixed
* Compile error when using `LOG4NET`

## [14.0.0-preview.2] - 2024-03-20
### Added
* Custom analytics event `ad_format_repurpose` to `VegasFullscreenAdWrapper.ShowFullscreenAd` method. This event will be sent when the API picks interstitial ad. You need `Sherlock Firebase` for this feature.

## [14.0.0-preview.1] - 2024-03-20
### Changed
* `VegasFullscreenAdWrapper.ShowFullscreenAd` will force pick interstitial if rewarded video is not loaded.
### Added
* Documentation section explaining how to show Interstitial Ad, in place of a Rewarded Video, if Rewarded Video is not loaded.

## [14.0.0-preview.0] - 2024-03-20
### Added
* `FullscreenAdWrapper`: Adds a layer to handle displaying interstitials or other full screen ads in place of rewarded videos, based on ecpm.
* `FullscreenAdWrapperEnabled`: Setting to enable or disable `FullscreenAdWrapper` feature. When disabled, requests to its API will fallback to regular Rewarded Video counterparts. (Remote setting: `fullscreenAdWrapperEnabled`)
* `FullscreenAdWrapperIntersBeforeForcedRewarded`: Setting to set number of interstitials that can be shown consequentially before system forces a rewarded video. (Remote setting: `fullscreenAdWrapperIntersBeforeForcedRewarded`)
* `Ecpm` property to `AdData` for ecpm value of the ad.
* `SetRevenue(double)` API to `AdData` for setting revenue manually. This converts the input revenue provided by the network to ECPM.
### Changed
* Signature `Shown` event for ads now includes `AdDTO` parameter.
### Migration Guide
* API change might cause compile errors. Please update your code to use the new API. Most notably, `Shown` event for ads now includes `AdDTO` parameter. If you are using this event, you need to update your code to use the new signature.
* To utilize the new system, you can check the docs. Summary: Enable `FullscreenAdWrapperEnabled` and adjust `FullscreenAdWrapperIntersBeforeForcedRewarded` for your needs. Instead of `Vegas.RewardedVideo.Show()` use `Vegas.FullscreenAdWrapper.ShowRewardedVideo()`. Instead of `Vegas.Interstitial.Show` use `VegasFullscreenAdWrapper.ShowFullscreenAd`.

## [13.2.0] - 2024-02-14
### Added
* `FailureReason` implementation
### Changed
* `SendAdRevenueEventsToAdjust` is true by default
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0

## [13.1.0] - 2023-12-11
### Added
* AdImpressionCount events; i.e. adimpsx30, adimpsx50

## [13.0.0] - 2023-12-11
### Added
* `SetNoAdsActive() : void` method added for handling no ads state when not using backpack.
### Changed
* `NoAds` property is now for internal use only

## [12.0.3] - 2023-12-06
### Fixed
* `InterstitialAd.AdDataObtained` event propagation fixed
* `RewardedVideoAd.AdDataObtained` event propagation fixed

## [12.0.2] - 2023-12-04
### Changed
* Log message about interstitial cooldown becomes warning instead of error

## [12.0.1] - 2023-11-29
### Fixed
* Compile error

## [12.0.0] - 2023-11-28
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
* Newtonsoft Json dependency: v3.0.2 -> v3.2.1
### Removed
* `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Fixed
* Sample code compile errors
* Compile errors while building on not iOS, tvOS and Android
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.

## [11.2.2] - 2023-11-20
### Removed
* `RevenueMultiplier` field to multiply revenue for testing.

## [11.2.1] - 2023-11-16
### Added
* SROptions field to multiply revenue for testing.
* Check for the revenue if it's duplicate for analytics.
* Ecpm Threshold event debug toggle added to `VegasDebugger`. Use to send threshold debug events for testing thresholds.

### Changed
* Rewarded Video Interstitial Cap is 0 by default

## [11.2.1-prev.1] - 2023-11-13
### Changed
* Rewarded Video Interstitial Cap is 0 by default

## [11.2.1-prev.0] - 2023-11-03
### Added
* SROptions field to multiply revenue for testing.

## [11.2.0-beta.2] - 2023-10-13
### Added
* Check for the revenue if it's duplicate for analytics.

## [11.2.0-beta.1] - 2023-10-09
### Added
* Ecpm Threshold event debug toggle added to `VegasDebugger`. Use to send threshold debug events for testing thresholds.

## [11.1.5] - 2023-09-28
### Fixed
* Potential compile error when using this version with a version of sherlock older than 9.1.2, where AppOpen ad type is defined.

## [11.1.4] - 2023-09-28
### Changed
* Game Analytics ad type mapping updated.

## [11.1.3] - 2023-09-27
### Added
* ad_tag parameter to SendCustomRevenueDataToFirebase
### Fixed
* SendAdDataToFirebase ad format fix.
* SendAdErrorDataToFirebase ad format fix.
* SendAdActionToFirebase ad format fix.

## [11.1.2] - 2023-09-15
### Fixed
* OnRevenueDataObtained invoke bug fix.

## [11.1.1] - 2023-08-31
### Fixed
* Banner state restore fix for Rewarded Video and Interstitial

## [11.1.0] - 2023-08-31
### Added
* Rewarded Video Interstitial Cap setting with `rewardedVideoInterstitialCap` remote config key.

## [11.0.3] - 2023-07-11
### Fixed
* Compile error when `Sherlock` isn't present.

## [11.0.2] - 2023-07-11
### Fixed
* Compile error when `Sherlock Firebase Service` isn't present.
* "revenue_total" => "ad_revenue_total". (Updating Vegas won't change it, this effects fresh installs)

## [11.0.1] - 2023-06-22
### Fixed
* Custom revenue events sending ad_impression event for each custom event bug fix.

## [11.0.0] - 2023-06-09
### Added
* `AdDTO`: `NetworkPlacement`
### Changed
* `X_loaded (X = Type)` event : `waterfall_latency` => `ad_waterfall_latency`

## [10.0.0] - 2023-06-06
### Added
* MRec Ads
* `IAdConfig`: To isolate id of ads type in `IMediationServiceConfig`
  * `IBannerAdConfig`
  * `IInterstitialAdConfig`
  * `IRewardedVideoAdConfig`
  * `AppOpenAdConfig`
  * `MrecAdConfig`

## [9.0.8] - 2023-06-01
### Fixed
* `DummyMediationService`: `Enabled` wasn't set correctly

## [9.0.7] - 2023-05-30
### Fixed
* When `NoAds` is enabled, it may conflict with `Enabled` check on `Banner.Destroy()`
* Compile error when Odin Inspector isn't present

## [9.0.6] - 2023-05-26
### Fixed
* `CumulativeRevenue`: getter had `int` value as `0`, changed to `double` as `0d`

## [9.0.5] - 2023-05-15
### Fixed
* Initializing ads moved to after initialization

## [9.0.4] - 2023-05-15
### Fixed
* Auto Load X After Init wasn't working properly (X = type)

## [9.0.3] - 2023-05-12
### Fixed
* Compile error on `VegasDebugger` when `Sherlock` is not present
* Compile error on `BannerAd` when `Sherlock` is not present
* Compile error on `InterstititalAd` when `Sherlock` is not present
* Compile error on `RewardedVideoAd` when `Sherlock` is not present
* Compile error on `AppOpenAd` when `Sherlock` is not present
 
## [9.0.2] - 2023-05-12
### Fixed
* Compile error on `VegasConfig` when `Sherlock` and `Sherlock-Firebase` are not present

## [9.0.1] - 2023-05-12
### Fixed
* Compile error on `VegasConfig` when `Backpack` and `Sherlock` are not present

## [9.0.0] - 2023-05-12
### Changed
* (!) Complete rework, see Migration Guide for details
### Removed
* Reload Try Count logic: now all ads try to be reloaded forever
### Added
* Cumulative Revenue events are added for Firebase, Adjust and GameAnalytics.
* (!) With this update, Vegas starts sending Cumulative Revenue Events automatically, in case you implemented your own cumulative revenue event logic,  dont forget to disable this feature in VegasConfig, see documentation for more info.
### Migration Guide
* All methods like `Vegas.XY (X = Op, Y = Type)` -> `Vegas.Y.X` (e.g. `Vegas.ShowBanner` -> `Vegas.Banner.Show`)
* All events like `Vegas.YX (X = Op, Y = Type)` -> `Vegas.Y.X` (e.g. `Vegas.BannerShown` -> `Vegas.Banner.Shown`)
* All fields like `Vegas.IsYX (X = Op, Y = Type)` -> `Vegas.Y.IsX` (e.g. `Vegas.IsBannerShowing` -> `Vegas.Banner.IsShowing`)
* `AdData` becomes `AdDTO` and revenue data is moved to another class `AdRevenueDTO`
* If you are subscribed to `XDataObtained (X = Type)` for obtaining revenue data, you need to switch to `Vegas.X.RevenueDataObtained (X = Type)`
* `AdErrorData` becomes `AdErrorDTO`
* eCPM Events are separated now (`EcpmEvents` -> `XEcpmEventThresholds (X = Type)` ). They are still using the same event token. But now, you can filter them by their type. If you have changed it before, you need to change all of them again. 
* (!) You need to update Sherlock to v9+

## [8.0.1] - 2023-03-23
### Fixed
* `NullReferenceException` occurs `BannerRevenueDataObtained` is invoked in `VegasDebugger`

## [8.0.0] - 2023-03-21
### Added
* `AdData`: WaterfallLatency added
* `IMediationService`: `BannerDataInitialized` event
* `IMediationService`: `BannerErrorDataInitialized` event
* `IMediationService`: `BannerRevenueDataInitialized` event
* `IMediationService`: `InterstitialDataInitialized` event
* `IMediationService`: `InterstitialErrorDataInitialized` event
* `IMediationService`: `InterstitialRevenueDataInitialized` event
* `IMediationService`: `RewardedVideoDataInitialized` event
* `IMediationService`: `RewardedVideoErrorDataInitialized` event
* `IMediationService`: `RewardedVideoRevenueDataInitialized` event
* `IMediationService`: `AppOpenDataInitialized` event
* `IMediationService`: `AppOpenErrorDataInitialized` event
* `IMediationService`: `AppOpenRevenueDataInitialized` event
* `BannerDataObtained` event
* `BannerErrorDataObtained` event
* `InterstitialDataObtained` event
* `InterstitialErrorDataObtained` event
* `RewardedVideoDataObtained` event
* `RewardedVideoErrorDataObtained` event
* `AppOpenDataObtained` event
* `AppOpenErrorDataObtained` event
### Removed
* `IMediationService`: `AdDataInitialized` event
* `IMediationService`: `AdErrorDataInitialized` event
* `IMediationService`: `RevenueDataInitialized` event
* `AdDataObtained` event 
* `AdErrorDataObtained` event

## [7.1.2] - 2022-12-20
### Fixed
* While a full-screen ad is showing, if the user becomes online, loading the ad again may be blocked.
### Updated
* New `ModuleDebubger` implementation

## [7.1.1] - 2022-12-09
### Added
* Service registration log
* Better `package.json` management
### Changed
* When there is no service registered Vegas will log an error message instead of throwing an exception.
* When there is no service registered, flags about ads will always be false.

## [7.1.0] - 2022-11-16
### Added
* `AdData`: `RawData`
* `VegasConfig`: Better inspector UI

## [7.0.0] - 2022-11-08
### Fixed
* App Open Ads logs

## [7.0.0-preview.3] - 2022-10-27
### Changed
* `VegasEditorUtility` is a static class now
### Fixed
* `CheckTesterDevice`: logs are moved to main thread

## [7.0.0-preview.2] - 2022-10-26
### Added
* `VegasUtility`: App Open Ads options added

## [7.0.0-preview.1] - 2022-10-21
### Added
* App Open Ads
### Changed
* `NoAds` won't be set if `Backpack` is present
* Vegas will mark tester devices as `IsTesterDevice` instead of calling `OpenDebugMenu`

## [6.0.5] - 2022-09-23
### Fixed
* When becoming online if `noads` is active, banner and interstitial will be loaded.

## [6.0.4] - 2022-09-22
### Fixed
* Now resetting `RewardedVideoReloadCount` after successful rewarded video loading.
* `DummyMediation`: Added internet connection checks.
### Added
* More logs for auto loading ads

## [6.0.3] - 2022-09-22
### Fixed
* Rewarded Video loading workaround when player comes back online

## [6.0.2] - 2022-09-06
### Fixed
* Infobox texts on `VegasConfig`

## [6.0.1] - 2022-09-06
### Changed
* `Meteor` initialization message is `warning` instead of `error` now
* Service null check is more earlier now

## [6.0.0] - 2022-09-01
### Fixed
* `AdData`: `LoadingDuration` was `Seconds` instead of `TotalSeconds`.
* Code refactor with `Handyman`
* **Migration guide**: After vegas update, close unity. Go to `PROJECTFOLDER/Assets/MatchinhamGames/Resources/Config`. Rename `MGDummyMediationConfig.asset` and `MGDummyMediationConfig.asset.meta` to `DummyMediationServiconf-igeC

## [6.0.0-preview.4] - 2022-08-18
### Fixed
* SRDebugger initialize itself even if SRDebugger automatic initialization is disabled. (It's about SRDebugger.)

## [6.0.0-preview.3] - 2022-08-11
### Fixed
* `VegasUtility`: API call in `SendRevenueDataToAdjust`
* `VegasUtility`: `AdPlacement` was using instead of `AdFormat` in `SendRevenueDataToFirebase`

## [6.0.0-preview.2] - 2022-08-03
### Fixed
* Stash wasn't saved because `OnApplicationQuit` is not invoking on iOS and Android.

## [6.0.0-preview.1] - 2022-08-03
### Added
* `VegasDebug`: Diagnostics tool on SRDebugger. SRDebugger must be in project now.
* `OverrideLogFlagsToAllOnDebug`: When `MG_DEBUG` is enabled, you can override `LogFlags` to `All`
* More logs added for debugging
### Changed
* `InterstitialCapped` => `IsInterstitialCapped`
* UserConsentValues won't be used when `DataUsageConsent` module in present
* *Internal* code refactoring
### Fixed
* `InterstitialClosed` event was invoked before `InterstitialTimer`

## [5.5.0] - 2022-08-01
### Added
* **MaxEcpmThreshold**: when `ecpmInDollar` is higher than this, event won't be invoked.
* More debug logs added for ECPM and ad revenue event tracking.

## [5.4.0] - 2022-07-27
### Changed
* **Interstitial Cap and Delay**: Moved from `Load` to `Show`
* `InterstitialCapped`: You can observe if interstitial is capped.
* `NextInterstitialTime` and `FirstSession` value shouldn't be set on public if `Stash` module is present.
### Fixed
* **Dummy Mediation**: `IsInterstitialLoaded` value was set after `InterstitialClosed` event invoked.
* **Dummy Mediation**: `IsRewardedVideoLoaded` value was set after `RewardedVideoClosed` event invoked.

## [5.3.3] - 2022-07-22
### Fixed
* InterstitialCooldown couldn't be more than 60 seconds
* InterstitialTimer couldn't stop after application quited

## [5.3.2] - 2022-07-01
### Changed
* ECPM events and ad revenue events send order

## [5.3.1] - 2022-06-16
### Fixed
* Config was null while registering to `Meteor` Module

## [5.3.0] - 2022-06-15
### Changed
* *(Internal)* Code refactor
### Added
* **Interstitial Load Cap to Vegas Config**: After each successful interstitial show, there will be delay for loading interstitial.
* **Interstitial Load Delay on First Session to Vegas Config**
### Fixed
* Default values added to AdData
* Default values removed for manual User Consent values
* Meteor Config registration fix
* If Vegas is already ready, it won't be initialized again

## [5.2.0] - 2022-06-01
### Added
User Consent values for manual setting if `Data Usage Consent Module` is not added:
* UserConsentGiven
* IsGDPRRequired
* IsCCPARequired

## [5.1.5] - 2022-05-27
### Fixed
* `revenue` calculation fixed when `ecpm` is set

## [5.1.4] - 2022-05-23
### Changed
* `TestDeviceIds` remote setting name is changed to `adsTestDeviceIds`

## [5.1.3] - 2022-05-18
### Changed
* Code refactor on checking tester ids
* Handyman dependency bump

## [5.1.2] - 2022-05-18
### Changed
* OpenDebugMenu method is static now.

## [5.1.1] - 2022-05-18
### Changed
* TesterIDs have a default value now (empty string).

## [5.1.0] - 2022-04-17
### Added
OpenDebugMenu method: To test mediation service further.
Vegas will open debug menu automatically if the device's adid is in the test ids list. This can be changed on remote settings.

## [5.0.1] - 2022-04-29
### Fixed
* Compile error when Sherlock Module is not in project

## [5.0.0] - 2022-04-28
### Changed
* BannerData became static
* InterstitialData became static
* RewardedVideoData became static
* AdDataObtained became static
* AdErrorDataObtained became static

## [4.0.0] - 2022-04-27
### Added
* AdData: Contains info about current ad: e.g. BannerData, InterstitalData, RewardedVideoData
* AdErrorData: Contains error info about current ad: e.g. BannerErrorData, InterstitialErrorData, RewardedErrorData
* AdAction: For internal analytics
* Options for ecpm events for GameAnalytics
* Options for ad revenue events for all analytics services
* AdErrorDataInitialized event: invokes when an ad has errors
* AdDataObtained event: invokes when an ad has revenue information
* SDK Version getter on all mediation services

### Changed
* ECPM Events: Replaced by AdData events
* serviceConfig -> ServiceConfig

## [3.1.1] - 2022-04-22
### Fixed
* Compile error when Meteor Module is not in project.

## [3.1.0] - 2022-04-18
### Added
* EcpmValueObtained event

## [3.0.1] - 2022-04-15
### Added
* Initializer for VegasConfig

## [3.0.0] - 2022-04-12
### Added
* ECPM Thresholds
* ECPM Methods
* ECPM Events
### Changed
* ShowBanner method has `adTag` now.
* DummyMediation is AsyncSingleton instead of AsyncSingletonBehaviour
* IMediationService is no longer inherited from IAsyncInitialize. Becuase, Vegas services are using AsyncSingletonX which already has IAsyncInitialize.
* Logger object is internal for other scripts in Vegas package
### Fixed
* Typos at DummyMediation

## [2.0.1] - 2022-03-16
### Fixed
* Compile fix when Meteor is not installed

## [2.0.0] - 2022-03-09
### Changed
* Namespaces

## [1.2.1] - 2022-03-08
### Changed
* Handyman dependency update

## [1.2.0] - 2022-03-08
* Initial release
