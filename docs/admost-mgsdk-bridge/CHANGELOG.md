# Changelog

## [7.0.0] - 2024-10-22
### Added
* `AdmostAdaptersConfig`: Adjust adapter management (see `Migration Guide`)
### Changed
* Handyman dependency: v5.8.2
* Minimum Unity version is now 2022.3
### Migration Guide
* If you have installed Adjust v5, you must update the package to this version and install `Adjust` adapter. OTHERWISE ADMOST-ADJUST CONNECTION MAY NOT WORK CORRECTLY!

## [6.3.0] - 2024-09-09
### Added
* Setting user id as Adjust id (see `Migration Guide`)
### Removed
* `OnInitialized`: Main thread is no longer needed
### Changed
* Handyman dependency: v5.7.0 -> v5.8.1
### Migration Guide
* Before installing, don't forget to add "com.adjust" to scopes of the registry whose URL is "https://package.openupm.com" (if not exist, add). Alternatively, you can install it from here: https://openupm.com/packages/com.adjust.sdk/

## [6.2.0] - 2024-07-26
### Changed
* Consent Logic (See `Migration Guide`)
* Handyman dependency: v5.3.1 -> v5.7.0
### Migration Guide
* If you are using Usercentrics as your CMP, make sure that `Admost (templateID: Dw8tBVuw1-ixeG)` is added to data processing services list in Usercentrics dashboard. OTHERWISE, ADMOST WILL NOT WORK PROPERLY!

## [6.1.0] - 2024-03-19
### Added
* `DataUsageConsentManager` v10+ support

## [6.0.0] - 2024-02-14
### Added
* `FailureReason` implementation
### Removed
* `Config.AutoInitialize`: Admost Bridge will always be initialized automatically
### Changed
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0

## [5.0.0] - 2023-11-20
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.1.5 -> v5.0.0
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [4.1.2] - 2023-09-19
### Fixed
* `UnityException` when `OnInitialized` is invoked

## [4.1.1] - 2023-03-10
### Changed
* `PreProcessBuildAndroid` removed due to new `IsApiHttps` API

## [4.1.0] - 2023-03-03
### Changed
* `IsApiHttps` added

## [4.0.4] - 2022-10-03
### Changed
* `Vegas - Admost` config file renaming fixed

## [4.0.3] - 2022-09-06
### Changed
* `DataUsageConsentManager` initialization message is `warning` instead of `error`

## [4.0.2] - 2022-09-05
### Changed
* DUCM initialization enforcement

## [4.0.1] - 2022-09-02
### Changed
* Initialization moved to AfterSceneLoad

## [4.0.0] - 01-09-2022
### Changed
* Code refactor with `Handyman`

## [3.0.1] - 2022-06-17
### Changed
* Data Usage Consent Manager API change
* If it's already initialized, it won't be intialized again.

## [3.0.0] - 2022-06-01
### Changed
* Data Usage Consent module dependency is now optional. Thus, you can manually set User Consent values.
* Option for disabling auto initialization added.
* COPPA value added

## [2.2.2] - 2022-05-17
### Changed
* Compile fix

## [2.2.1] - 2022-05-16
### Changed
* Build processor scripts migration from vegas-admost package

## [2.2.0] - 2022-04-29
### Changed
* Admost Config Menu -> Admost Config Window : This enables AMR adapter to be added to the project.

## [2.1.0] - 2022-04-29
### Changed
* JSON Downloader stripped

## [2.0.1] - 2022-04-27
### Changed
* Admost SDK depedency updated

## [2.0.0] - 2022-04-27
### Changed
* Admost adapter integration added

## [1.0.0] - 2022-04-15
### Changed
* Initial release
