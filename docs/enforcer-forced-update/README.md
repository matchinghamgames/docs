# Enforcer Force Update Module

## Overview
Provides a simple API for forcing/notifying players to update the game

## Installing
* Import package from Package Manager UI.
* Open module settings from `Matchingham > App Review`. See [Configuration](#configuration)

## How to use
* Make sure that [configuration](#configuration) is correct.
* Call `Enforcer.Instance.Initialize()` to initialize module.
  * If `Meteor` module is present. **Make sure that `Meteor` is initialized before Enforcer.**

## Configuration
Go to `Matchingham > Forced Update Config`

| Variable                                | Remote Name      | Description                                                                        |
|-----------------------------------------|------------------|------------------------------------------------------------------------------------|
| Enabled                                 | enforcerEnabled  | Enabling/disabling the module                                                      |
| AutoInitialize                          | N/A              | If enabled, you don't need to call Initialize method manually.                     |
| Force Update                            | forceUpdate      | If the player have to update the game. If `true`, the player can't skip the popup. |
| Minimum Version                         | minimumVersion   | Minimum version the game should be.                                                |
| Enforcer Popup Prefab                   | N/A              | In-game UI (prefab) that the player will see                                       |
| App Store Link                          | N/A              | App store link of this game                                                        |
| Play Store Link                         | N/A              | Google Play Store link of the game                                                 |
| Android System Back Button Enabled      | N/A              | Enabling/disabling the feature of closing the popup on Android System Back Button press |

## Custom In-App Review UI

In-app review UI is a prefab with a script with `UpdateRequiredPopup` class. You can either create a variant of the prefab to make simple adjustments or create a completely new prefab with a script inheriting from `UpdateRequiredPopup` class attached. You can write your own script to implement custom behaviour, just make sure it inherits from `UpdateRequiredPopup`. Using a prefab that doesn't have an appropriate script attached will crash the app.

## API & Details

### Common

#### Methods

* **Initialize()**
  : Starts module initialization. You need to call this at the appropriate place.

* **WhenInitialized(Action callback)**
  : Allows you to register `callback` that will be fired only after the module is successfully initialized. Use this to execute logic that requires this module to be initialized first. If the module has already initialized, immediately invokes the callback.

* **WhenFailedToInitialize(Action callback)**
  : Allows you to register `callback` that will be fired only after the module fails to initialize for any reason. Use this to handle what should happen in case this module fails to initialize. If the module has already failed to initialize, immediately invokes the callback.

* **WhenReady(Action callback)**
  : Combined version of `WhenInitialized` and `WhenFailedToInitialize`. Delays execution of `callback` till module is first initialized or failed to initialize, immediately invoke the callback if it is already initialized or failed to initialize.

#### Fields

* **State**
  : Initialization state of the module

* **Instance**
  : Instance of the module

* **LateInitialized**
  : When the module needs an internet connection but the player became online while playing the game, this becomes `true`

* **Ready**
  : If the module is initialized successfully and ready to operate

* **Config**
  : Configuration of the module. See [configuration](#configuration)
