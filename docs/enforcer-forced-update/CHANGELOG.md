# Changelog

## [4.0.0] - 2025-02-19
### Added
* If `Config.enforcerPopupPrefab` is null, initialization will fail.
### Changed
* Handyman dependency: v5.8.2
* Minimum Unity version 2022.3
* When there was a reference for `Config.enforcerPopupPrefab`, but the referenced object is not found, default prefab won't be set, a warning message will be displayed on console.
### Fixed
* Package files can be seen in editor now.

## [3.2.0] - 2024-08-06
### Added
* `IUserResponseHandler` implementation
### Changed
* Handyman dependency: v5.0.0 -> v5.7.0
* Logger dependency: v2.0.0 -> v2.1.0

## [3.1.0] - 2024-04-15
### Added
* Android back button support.

## [3.0.0] - 2023-11-20
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.1.5 -> v5.0.0
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Removed
* `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [2.1.4] - 2023-11-03
### Added
* Validation checks

## [2.1.3] - 2023-02-27
### Changed
* Compile fix when `Meteor` is not present

## [2.1.2] - 2023-01-09
### Changed
* New Debugger implementation

## [2.1.1] - 2022-10-13
### Changed
* `EnforcerConfig`: `Enabled` and `LogFlags` serialization fix
* `EnforcerConfig`: `Enabled` and `LogFlags` `RemoteSetting` attribute usage fix

## [2.1.0] - 2022-09-06
### Changed
* Enforcer initialization will wait `UpdateRequiredPopup`
* Whether `forceUpdate` is enabled or not, `Enforcer` will always be initialized successfully now.

## [2.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`

## [1.2.8] - 2022-03-09
### Changed
* Initial release
