# Changelog

## [6.0.2] - 2025-01-10
### Fixed
* `System.Exception: Field currentActivity or type signature not found` on Editor when app started in Android platform 

## [6.0.1] - 2024-12-13
### Added
* Added `AndroidUtility` class that provides install source information API
* Added `InstallingPackageName` and `PackageSource` user properties

## [6.0.0] - 2024-10-04
### Added
* Firebase Analytics 12.4 or newer compatiblity (See `Migration Guide`)
### Changed
* Handyman dependency: v5.8.2
* Sherlock dependency: v12.1.0
* Firebase MGSDK Bridge dependency: v5.0.0
* External Dependency Manager for Unity dependency: v1.2.183
* Min Unity version: 2022.3
### Migration Guide
* It is recommended to update Firebase packages to 12.4.0 or newer.

## [5.4.0] - 2024-09-09
### Changed
* Setting user id as Adjust id logic: Adjust v5 implementation
* Handyman dependency: v5.3.1 -> v5.8.1
* Firebase MGSDK Bridge dependency: v4.2.1 -> v4.5.0
* Sherlock dependency: v10.2.0-prev.1 -> v12.0.0
### Removed
* Sherlock-Adjust asmdef reference
### Migration Guide
* Before installing, don't forget to add "com.adjust" to scopes of the registry whose URL is "https://package.openupm.com" (if not exist, add). Alternatively, you can install it from here: https://openupm.com/packages/com.adjust.sdk/

## [5.3.0] - 2024-04-04
### Added
* `IFirebaseAnalyticsService.SetUserId(string)` method implementation as wrapper for `FirebaseAnalytics.SetUserId(string)`
* `IFirebaseAnalyticsService.SetUserProperty(string, string)` method implementation as wrapper for `FirebaseAnalytics.SetUserProperty(string, string)`

## [5.2.1] - 2024-03-21
### Fixed
* Consent isn't updated after the user closes the `Advanced Popup (a.k.a. Second Layer)`
### Changed
* Firebase MGSDK Bridge dependency: v4.1.0 -> v4.2.1

## [5.2.0] - 2024-03-19
### Added
* Data Usage Consent Manager dependency: v11.0.0
### Changed
* Consent logic improvements (see `Migration Guide`)
* External Dependency Manager for Unity dependency: v1.2.177 -> v1.2.179
* Firebase App (Core) dependency: v8.1.0 -> v11.8.0
* Firebase Analytics dependency: v8.1.0 -> v11.8.0
* Setting User ID as Adjust AdID logic rework
### Migration Guide
* If you are using Usercentrics as your CMP, make sure that `Google Analytics for Firebase (templateID: diWdt4yLB)` is added to data processing services list and `Google Advertising Products (ID: 755)` is added to global vendors list in Usercentrics dashboard. OTHERWISE, FIREBASE ANALYTICS WILL NOT WORK PROPERLY!

## [5.1.0] - 2024-02-16
### Added
* `FailureReason` implementation
### Changed
* Better timeout logic
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
* Sherlock dependency: v10.0.0 -> v11.1.0
* Firebase MGSDK Bridge dependency: v4.0.0 -> v4.1.0

## [5.0.0] - 2023-11-17
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
* Firebase MGSDK Bridge dependency: v3.1.2 -> v4.0.0
* External Dependency Manager for Unity dependency: v1.2.169 -> v1.2.177
* Sherlock dependency: v8.2.1 -> v10.0.0
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [4.2.3] - 2023-10-02
### Changed
* Removed Initialization condition from sending events.

## [4.2.2] - 2023-09-19
### Added
* `Firebase Bridge` init check added for timeout registration

## [4.2.1] - 2023-09-18
### Fixed
* (Minor) Event logs should include event names

## [4.2.0-preview.1] - 2023-05-04
### Changed
* Firebase auto-collection disabled

## [4.0.3] - 2023-05-04
### Changed
* Setting Adjust AdID as user ID: using cached AdID instead

## [4.0.2] - 2023-03-24
### Changed
* Setting Adjust ADID as user ID is linked to `AdjustAnalyticsService.WhenInitialized`

## [4.0.1] - 2023-03-13
### Changed
* Initialization improvements

## [4.0.0] - 2022-11-23
### Changed
* Setting user id as adjust adid when Adjust SDK is added
* `extraParameters` added to all methods
* MG_METEOR symbol removed

## [3.0.1] - 2022-11-15
### Changed
* `SendCustom`: `ulong` and `uint` casting to `long` and `int`.

## [3.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`

## [2.3.3] - 2022-08-11
### Changed
* IFirebaseAnalyticsService: SendAdImpression: placement => adFormat

## [2.3.2] - 2022-07-21
### Changed
* CheckReady fix

## [2.3.1] - 2022-07-06
### Changed
* Sherlock dependency bump

## [2.3.0] - 2022-07-06
### Changed
* Injector moved to MGFirebaseAnalytics
* MGFirebaseAnalytics is inherited by AsyncSingleton instead of AsyncSingletonBehaviour
* IsReady -> CheckReady

## [2.2.0] - 2022-06-22
### Changed
* New Firebase MGSDK Bridge package compability
* Dependencies bump
### Fixed
* IsReady fix on `SendCustom` method
* IsReady fix on `SendAppOpen` method

## [2.1.0] - 2022-04-27
### Added
* SendAdImpression implementation
### Changed
* Error messages on non-init state overhaul

## [2.0.1] - 2022-03-09
### Changed
* Code cleanup

## [2.0.0] - 2022-03-09
### Added
* Initial Release
