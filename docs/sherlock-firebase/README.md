# Firebase Sherlock Analytics Service

## Overview

This is an analytics service that is tailored for use alongside the Sherlock module. 
For detailed information, please refer to https://matchinghamgames.gitlab.io/docs/sherlock-analytics/.

## Installing

* Import package from Package Manager UI.
* Make sure you use appropriate com.google.firebase.analytics and com.google.firebase.app packages.
* (!)com.google.firebase.* packages needs to have the same version numbers.

## API & Details

* **`SendCustom(eventName: string, parameters: IDictionary<string, object> (null))`**
  : Allows sending a custom, self styled analytics event. Most of the events are sent with this method.

* **`SetUserId(userId: string)`**
  : Set user id for the current user. This is useful for tracking user data across multiple devices.

* **`SetUserProperty(name: string, value: string)`**
  : Set a user property to a given value. This is useful for tracking user data across multiple devices.
  
* **`SendAppOpen(extraParameters: IDictionary<string, object> (null))`**
  : When game initialization is complete, this event should be sent

* **`SendAdImpression(revenue: double, networkName: string, adUnitId: string, placement: string, sdkSource: string, currency: string, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides a specialized ad impression event.

* **`SendCustomInAppPurchase(token: string, itemId: string, currencyCode: string, price: float, extraParameters: IDictionary<string, object> (null))`**
  : Firebase normally collects in app purchase events itself. However, if you want, you can use these to send them as a custom event.
  
* **`SendEarnVirtualCurrency(currency: string, amount: long, extraParameters: IDictionary<string, object> (null))`**
  : This event is used to log whenever the user earns virtual currency in game.
  
* **`SendEarnVirtualCurrency(currency: string, amount: double, extraParameters: IDictionary<string, object> (null))`**
  : Overload of the same analytics event. Uses a double parameter instead of long.
  
* **`SendLevelStart(levelName: string, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides a specialized level start analytics event. This method sends that event.
  
* **`SendLevelEnd(levelName: string, success: bool), extraParameters: IDictionary<string, object> (null)`**
  : Firebase provides a specialized end start analytics event. This method sends that event.
  
* **`SendLevelUp(level: long, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides a specialized event for tracking player level ups. This method sends that event.
  
* **`SendLevelUp(level: long, character: string, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides a specialized event for tracking player level ups. This method sends that event, but with an additional `character` parameter. This allows which character was leveled up, for more detailed usage analytics data in games with multiple character support.
  
* **`SendScreenView(screenName: string, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides a specialized event for tracking which app screen is viewed. This method sends that event.
  
* **`SendScreenView(screenClass: string, screenName: string, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides a specialized event for tracking which app screen is viewed. This method sends that event, but also provides the view class.
  
* **`SendSpendVirtualCurrency(currency: string, itemName: string, amount: long, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides specialized events for tracking where the user spends their virtual currency. This method sends that event
  
* **`SendSpendVirtualCurrency(currency: string, itemName: string, amount: double, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides specialized events for tracking where the user spends their virtual currency.
  
* **`SendTutorialBegin(extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides special analytics events for tracking when the tutorial starts. This method sends that event.
  
* **`SendTutorialEnd(extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides special analytics events for tracking when the tutorial ends. This method sends that event.