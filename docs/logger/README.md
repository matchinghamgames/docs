# Logger Module

## Overview

Provides a logger class and utility methods to customize its behaviour

## Installing

* Install package from Package Manager UI

## How to use

### Without log4net

#### Basic Usage
* `this.Debug("Hello World")` to print "Hello World" debug message
* `this.Info("Hello World")` to print "Hello World" info message
* `this.Warn("Hello World")` to print "Hello World" warning message
* `this.Error("Hello World")` to print "Hello World" error message
* `this.Fatal("Hello World")` to print "Hello World" fatal message
* Log a caught exception using `ex.Log("Hello World")` with "Hello World" message.

#### Advanced Usage

Logger module is a `Logger` class to log various things with given name. You can create a `Logger` object like this:
```csharp
var logger = LogManager.GetLogger("myLogger");
```

We have 6 levels for filtering log messages: `Debug (= 0)`, `Info (= 1)`, `Warning (= 2)`, `Error (= 3)`, `Fatal (= 4)`, `Off (= 5)`
If a log's level is lower than its logger's level or `LogManager.GlobalLevel` then it won't be printed.
To give a logger level, you have two options:

* Creating logger while runtime:
```csharp
var logger = LogManager.GetLogger("myLogger", LogLevel.Debug);
var logger = LogManager.GetLogger("myLogger", () => LogLevel.Debug); // For setting Log Level dynamically
```
* Creating logger as a field
```csharp
[AddToLoggerConfig("myLogger" LogLevel.Info)]
public readonly ILog logger = LogManager.GetLogger("myLogger");
```
`AddToLoggerConfig` attribute adds `myLogger` to `LoggerConfig` with `Info` LogLevel by default. You can change the logger's level there.
To see `LoggerConfig` go to `Matchingham > Logger Config`

* `logger.Debug("Hello World")` to print "Hello World" debug message
* `logger.Info("Hello World")` to print "Hello World" info message
* `logger.Warn("Hello World")` to print "Hello World" warning message
* `logger.Error("Hello World")` to print "Hello World" error message
* `logger.Fatal("Hello World")` to print "Hello World" fatal message

The above logger would print logs in the below format.
```
myLogger: Your log message here
[com.mycompany.myapp]
```

You can also set global flags to restrict logging for all loggers. For example, if
you make the below call, all loggers will be restricted to error logging only even
if they have different flags set themselves.

```csharp
`LogManager.GlobalFlags = LogLevel.Error;`
```

You can use formatting in the logs as well!

```csharp
var logger = new Logger("myLogger", LogLevel.Warning);
logger.WarnFormat("I have {0} pencils and {1} books", 5, 10);
```

The extension methods above in the [Basic Usage](#basic-usage) section uses the logger class behind
the scenes. They basically make calls to instances of the logger class specifically
created for the object they are being called for. The first time you make a call from 
`MyClass` using `this.Debug("Hello World")` call would create
a custom Logger for `MyClass` and cache it, using the name of the class as the logger's name.

```csharp
class MyClass
{
    public void MyAmazingMethod()
    {
        ...
        // Outputs: MyClass: An important log message...\n[com.mycompany.myapp]
        this.Info("An important log message...");
        ...
    }
}
```

### With log4net

log4net is a port of log4j framework to the .NET runtime. To use it, you need to add `LOG4NET` to Scripting Define Symbols in Player Settings.
log4net enables logging on different platforms such as console (default), file and Crashlytics.

#### Usage

* Create logger as a field
```csharp
[AddToLoggerConfig("myLogger" LogLevel.Info, LogLevel.Debug, LogLevel.Error)]
public readonly ILog Logger = LogManager.GetLogger("myLogger");
```

* `logger.Debug("Hello World")` to print "Hello World" debug message
* `logger.Info("Hello World")` to print "Hello World" info message
* `logger.Warn("Hello World")` to print "Hello World" warning message
* `logger.Error("Hello World")` to print "Hello World" error message
* `logger.Fatal("Hello World")` to print "Hello World" fatal message

By default, Logger Module won't allow `Logger`s outside `LoggerConfig`. Thus you need to add it to `LoggerConfig`.
`AddToLoggerConfig` attribute adds `myLogger` to `LoggerConfig` with `Info` LogLevel on Console, `Debug` LogLevel on file and `Error` LogLevel on Crashlytics by default. You can change the logger's levels there.

To see `LoggerConfig` go to `Matchingham > Logger Config`

##### Logging to file

log4net will save log messages to between `Application.persistentDataPath/{LoggerConfig.File}` and `Application.persistentDataPath/{LoggerConfig.File}.{LoggerConfig.MaxSizeRollBackups}`
For example: `Application.persistentDataPath/log.txt`, `Application.persistentDataPath/log.txt.5`

To obtain files, you can use shell via adb on Android or download containers from XCode.
Alternatively, if you add [Unity Native Share Plugin by yasirkula](https://github.com/yasirkula/UnityNativeShare), log files will be added as buttons in SRDebugger. 