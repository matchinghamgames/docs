# Changelog

## [2.1.3] - 2025-01-09
### Added
* Logger SerializableDictionary dependency fix.

## [2.1.2] - 2024-08-08
### Added
* LoggerConfig: MG_DEBUG warning.

## [2.1.1] - 2024-05-13
### Added
* More null reference checks.

## [2.1.0] - 2024-02-09
### Added
* _(non log4net)_,_(Internal)_ `LogMethod`
### Fixed
* _(non log4net)_ this.* extension methods were always working as `Debug` log 

## [2.0.0] - 2023-11-20
### Changed
* (log4net) Console Log Level will be always `Debug` on editor or `MG_DEBUG` symbol is present
* `Warning` -> `Warn`
### Added
* log4net/log4uni implementation (Default disabled, can be enabled by adding `LOG4NET` to scripting define symbols)
* LoggerConfig: For configurating all `Logger`s in one place.
* (log4net) `NativeShare` compatibility for log file sharing
* (log4net) Custom Appender for Crashlytics
* (log4net) Custom Appender for Saving log file to `Application.persistentDataPath`
* (log4net) Custom Filter for handling both `Logger`s and `Level`s

## [1.3.2] - 2023-04-07
### Fixed
* bundle id wasn't at the last line on `GetLogStringFormatted` on some cases

## [1.3.1] - 2022-10-13
### Fixed
* root namespace was missing on runtime asmdef

## [1.3.0] - 2022-09-01
### Fixed
* bundle id will be at the last line on `GetLogStringFormatted`
* Color tag will be only in editor `GetLogStringFormatted`

## [1.2.0] - 2022-09-01
### Changed
* bundle id will be at the last line
* Color tag will be only in editor

## [1.1.0] - 2022-03-08
### Added
* All option
### Changed
* License

## [1.0.0] - 2021-11-16
* Initial Release