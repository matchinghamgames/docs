# Changelog

## [5.1.0] - 2024-02-16
### Added
* `FailureReason` implementation
### Changed
* Better timeout logic
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
* Meteor dependency: v5.0.0 -> v5.1.0

## [5.0.0] - 2023-11-20
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.0.1 -> v5.0.0
* Meteor dependency: v4.0.1 -> v5.0.0
* GameAnalytics MGSDK Bridge dependency: 3.0.0 -> 4.0.0
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [4.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`

## [3.0.1] - 2022-06-02
### Changed
* GA Bridge package dependency
* asmdef references are by name now

## [3.0.0] - 2022-03-09
### Changed
* Namespace

## [2.0.0] - 2022-03-09
* Initial release