# Changelog

## [8.0.0] - 2025-01-16
### Added
* `FailureReason` implementation
### Changed
* If `Admost Bridge` is ready, this won't listen timeout. This may fix rare timeout issues.
* Logger dependency: v2.1.0
* Handyman dependency: v5.8.2
* Sherlock dependency: v12.1.0
* Admost Bridge dependency: v7.0.0
* Minimum Unity version: 2022.3
### Deprecated
* `SendCustom`: Already not working, just added `Obsolete` tag to emphasize it. 

## [7.1.0] - 2024-08-20
### Added
* `TrackAdmobRevenue` implementation

## [7.0.0] - 2024-02-09
### Changed
* Updated `SendIapEvent` implementation

## [7.0.0-prev.0] - 2024-01-24
### Changed
* Updated `SendIapEvent` implementation

## [6.0.0] - 2023-11-17
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.1.4 -> v5.0.0
* Sherlock dependency: v8.0.0 -> v10.0.0
* Admost MGSDK Bridge dependency: v4.0.4 -> v5.0.0
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [5.0.0] - 2022-11-23
* `SendCustom`: `Dictionary` => `IDictionary`

## [4.0.2] - 2022-09-02
* Initialization fix

## [4.0.1] - 2022-09-01
* Dependency bump

## [4.0.0] - 2022-09-01
* Code refactor with `Handyman`

## [3.1.2] - 2022-07-21
* CheckReady fix

## [3.1.1] - 2022-07-06
* Sherlock dependency bump

## [3.1.0] - 2022-07-06
* Injector moved to MGAdmostAnalytics
* IsReady -> CheckReady
* Logger tag is Sherlock_Admost now

## [3.0.0] - 2022-04-15
### Changed
* Initialization depends on Admost MGSDK Bridge package now. Vegas Admost package is not needed anymore. Thus, Sherlock Module can be initialized before Vegas Module.

## [2.0.1] - 2022-03-09
### Fixed
* Vegas - Admost Service dependency added

## [2.0.0] - 2022-03-09
* Initial release
