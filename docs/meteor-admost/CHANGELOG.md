# Changelog

## [6.1.0]- 2024-02-16
### Added
* `FailureReason` implementation
### Changed
* Better timeout logic
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
* Meteor dependency: v5.0.0 -> v5.1.0
* Admost MGSDK Bridge dependency: 5.0.0 -> 6.0.0

## [6.0.0] - 2023-11-20
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
* Meteor dependency: v4.3.4 -> v5.0.0
* Admost MGSDK Bridge dependency: 4.0.0 -> 5.0.0
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [5.0.9] - 2023-11-14
### Added
* Admost Config Editor to copy Admost Configuration in Admost Dashboard format.
* `AdmostRemoteScriptableObject` special ScriptableObject for Admost Configurations.

## [5.0.8] - 2023-07-12
### Fixed
* Fix for invalid cast exception in AdMost Remote Config Service

## [5.0.7] - 2023-07-11
### Changed
* Meteor -> 4.3.0
* Handyman Utility -> 4.3.2
* Logger -> 1.3.2

## [5.0.6] - 2023-05-12
### Fixed
* Admost Editor Utility 'cannot convert from char to string' bug fix.

## [5.0.5] - 2023-05-12

## [5.0.4] - 2023-05-05

## [5.0.3] - 2023-03-10
### Fixed
* Configuration Provider Set Bug.

## [5.0.2] - 2023-03-10
### Added
* Editor functionalities, Admost Remote Sync Capability.

## [5.0.1] - 2022-09-01
### Fixed
* Dependency bump

## [5.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`: All public fields and methods are static now.

## [4.0.0] - 2022-04-15
### Changed
* Initialization depends on Admost MGSDK Bridge package now. Vegas Admost package is not needed anymore. Thus, Meteor Module can be initialized before Vegas Module.

## [3.0.0] - 2022-03-09
### Changed
* Namespace

## [2.0.0] - 2022-03-09
* Initial release