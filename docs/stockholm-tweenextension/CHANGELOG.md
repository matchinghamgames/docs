# Changelog

## [2.0.2] - 2025-02-07
### Changed
* DoTween Kill error checks minor update.

## [2.0.1] - 2024-11-14
### Fixed
* DoTweenView->InitiateReset should call TweenReset.
### Migration Guide
* Refactor any classes that inherit from the DoTweenView class: Ensure that TweenReset is updated, the default behavior was previously to reset the scale to zero. Adjust accordingly to match the new desired behavior.

## [2.0.0] - 2024-09-18
### Changed
* Stockholm dependency: v4.0.1
* Minimum Unity version is now 2022.3
* `MatchinghamGames.StockholmTween.Runtime` namespace -> `MatchinghamGames.StockholmTween`
### Added
* `DOTween` asmdef references added to Runtime asmdef

## [1.0.1] - 2022-08-08
### Fixed
* Dependencies

## [1.0.0] - 2022-08-07
### Added
* Initial release