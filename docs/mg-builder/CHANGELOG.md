# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.5] - 2025-02-03

### Fixed

- Fixed the issue where the `MG_AND_MIN_SDK_VERSION` environment variable was not being correcly set in the `CommandLineArgumentParser` file.

## [2.1.4] - 2025-01-22

### Added

- Added the MG_AND_MIN_SDK_VERSION environment variable to base-ci.yml to set the minimum Android SDK version for the project.

### Changed

- Changed the `APPLE_COREHAPTICS` compile flag behaviour to be available only for `com.apple.corehaptics` package version 1.2.3 and below. The flag is now `APPLE_COREHAPTICS_1_2_3_OR_OLDER`. With the new version of the core haptics package, the flag is not necessary for adding Core Haptics post process workarounds since the package already includes the necessary build settings for Xcode.

## [2.1.3] - 2024-11-19

### Fixed

- Fixed Vibration module not building on iOS builds by adding Apple Core Haptics workaround to Xcode project settings.

## [2.1.2] - 2024-10-15

### Fixed

- Fix unnecessary `using PlasticGui;` that cause compile errors. 

## [2.1.1] - 2024-10-14

### Added

- Added support for the "Always Embed Swift Standard Libraries" option to the post-process workarounds in `BuilderConfig.cs`.

### Changed

- Increased the label width from for UI Elements in `BuilderConfig`

## [2.1.0] - 2024-10-13

### Added

- Introduced **PostProcess Workaround Scripts** to address some Xcode compilation errors related to Adjust, Facebook, Applovin, UserCentrics, and External Dependency Manager (EDM) using `BuilderConfig`. These values default to `true` if the project has the UserCentrics and Adjust SDK installed.
- **Override Facebook PodFile Post Process**: `ProcessPodfileFacebookPostProcessor` removes the redundant `Unity-iPhone` target in the Podfile added by Facebook before EDM and Applovin.
- **Add UserCentrics framework to main target**: `LinkUserCentrictsFrameworkPostProcessor` links UserCentrics.framework and UserCentricsUI.framework to the Xcode project.
- **Add Adjust framework to main target**: `LinkAdjustSignFrameworkPostProcessor` links AdjustSigSdk.framework to the Xcode project.

### Changed

- Modified `BuilderConfig` by grouping YAML-overridable values to reflect the ongoing work in `base-ci.yml`.
- Updated the SDK `com.matchinghamgames.handyman` from `5.3.1` to `5.8.2`

### Known Issues

- There is a conflict between the "Building Specific Data" section in `BuilderConfig` and `base-ci.yml`. Default values in `base-ci.yml` override those in `BuilderConfig`. Further fixes should first be applied in `base-ci.yml`.


## [2.0.4] - 2024-07-30

### Added

- Added EDITOR_UNITY_VERSION environment variable

## [2.0.3] - 2024-07-11

### Added

- Added initial .gitlab-ci.yml file creation menu item to the Unity Editor.


## [2.0.2] - 2024-07-11

### Changed

- Updated manifest parser to consider tools:node attribute in AndroidManifest.xml file.
- Updated version parsers to check if the version is not valid, ie, if the version is not in the Ruby GemVersion standard, it will be set to 0.0.0.

## [2.0.1] - 2024-06-24

### Changed

- Remove namespaces for entry point of the module which makes it backward compatible.
- Refactored entire module, reorganized to follow the Single Responsibility Principle.
- APP_NAME variable is not necessary in gitlab-ci.yml file. It is now getting from editor, transferring to pipeline.

## [2.0.0] - 2024-06-13

### Added

- Added new Parser system , to provide more inside data from Unity Editor.
  - Applovin Dependencies, Package Manifest, mainTemplete.gradle file, AndroidManifest.xml file parsers included to provide which package is being included to the build.
- Created 2 way communication between module and pipeline.
- Created new BuilderConfig, which overrides Editor Build Settings. but YAML(.gitlab-ci.yml) variables overrides all, remains same.
- Add new Environment variables transfer method to builder module, which transfer this variables back to Gitlab-CI pipeline
- Created a new Logger for Builder Module.

### Changed

- Refactored entire module, reorganized to follow the Single Responsibility Principle.
- APP_NAME variable is not necessary in gitlab-ci.yml file. It is now getting from editor, transferring to pipeline.

## [1.5.1] - 2024-05-28

### Added

- shouldBuildAddressables flag added

## [1.5.0] - 2024-02-23

### Added

- AppLauncher Post Processor script

## [1.5.0-prev.0] - 2024-02-13

### Added

- AppLauncher Post Processor script

## [1.4.8] - 2023-10-03

### Fixed

- Fixed missing meta files for Changelog and Readme

## [1.4.7] - 2023-09-28

### Fixed

- Fixed missing meta files for Changelog and Readme

## [1.4.6] - 2023-09-27

### Added

- Use BuildOptions along with EditorUserBuildSettings in iOS Build Player Pipeline. Possible fix for Development build is not working on iOS platform

### Changed

- Make DISABLE_SRDEBUGGER compile flag optional with IS_SRDEBUGGER_FLAG_ACTIVE environment variable

## [1.4.5] - 2023-09-27

### Added

- Use BuildOptions along with EditorUserBuildSettings. Possible fix for AllowDebugging is not working.

### Changed

- Make DISABLE_SRDEBUGGER compile flag optional with IS_SRDEBUGGER_FLAG_ACTIVE environment variable

## [1.4.4] - 2023-09-26

### Added

- DISABLE_SRDEBUGGER compile flag condition add to release builds.

## [1.4.3] - 2023-08-29

### Fixed

- Fix symbol creation, in Unity 2021.3 and above versions.
