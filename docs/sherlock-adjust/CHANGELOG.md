# Changelog

## [7.0.1] - 2024-09-10
### Fixed
* Rare exception while setting attribution info as Firebase user property and Firebase has not been initialized yet.

## [7.0.0] - 2024-09-09
### Added
* Adjust SDK v5.x implementation (see `Migration Guide`)
* Sherlock v12.x implementation 
### Changed
* Adjust dependency: v4.38.0 -> v5.0.2 (see `Migration Guide`)
* Adjust asmdef references (see `Migration Guide`)
* Handyman dependency: v5.7.0 -> v5.8.1
* Sherlock dependency: v11.2.0 -> v12.0.0
### Removed
* Logger usage on main thread only
* Setting user id as Adjust id (see `Migration Guide`)
### Migration Guide
* Before installing, don't forget to add "com.adjust" to scopes of the registry whose URL is "https://package.openupm.com" (if not exist, add). Alternatively, you can install it from here: https://openupm.com/packages/com.adjust.sdk/
* Uninstall older version of Adjust SDK (v4.x) after update if it exists.
* Setting user id as Adjust id is moved to `Admost MGSDK Bridge` v6.3.0+

## [6.9.0] - 2024-08-01
### Changed
* If `Firebase Messaging (FCM)` is installed, it will wait for `Firebase MGSDK Bridge` to be ready now. This fixes a rare exception when Firebase is still not ready while this package is initializing.
* Handyman dependency: v5.5.0 -> v5.7.0
### Migration Guide
* If you update to this version, make sure that `Firebase MGSDK Bridge` v4.4.0 or higher is installed. Otherwise, you will get compile errors about cyclic dependencies!

## [6.8.0] - 2024-06-12
### Added
* Set `trackerName` attribution info as firebase user property
* Set `trackerToken` attribution info as firebase user property

## [6.7.0] - 2024-05-06
### Changed
* Handyman dependency: v5.3.3 -> v5.5.0
* Sherlock dependency: v11.1.1 -> v11.2.0
### Removed
* `Meteor` related logic. See `Migration Guide`.
### Migration Guide
* Configuration no longer has remote values. If you are using any remote values for this module. Make sure that you moved to local. This also enables this module to be able to be initialized before `Meteor`.

## [6.6.0] - 2024-05-02
### Added
* `AdjustSettings` intitialization: (iOS) This enables `AdSupport`, `AdServices`, `AppTrackingTransparency` and `StoreKit` frameworks to be added to the xcode project on post build process. (Android) In addition, this enables `android.permission.INTERNET`, `android.permission.ACCESS_NETWORK_STATE`, `com.google.android.gms.permission.AD_ID` and `com.google.android.finsky.permission.BIND_GET_INSTALL_REFERRER_SERVICE` permissions to be added to the main manifest on post build process.
### Removed
* _(Internal)_ Old Adjust codes (older than v.4.35.2) were removed. Because Adjust dependency is higher than v4.35.2.

## [6.5.1] - 2024-04-22
### Fixed
* `GetAdId()` method was printing logs constantly on editor. Now it gives a random GUID on editor.

## [6.5.0] - 2024-04-18
### Changed
* _(iOS Only)_ Post Process for Privacy Manifest
* Handyman dependency: v5.3.1 -> v5.3.3
* Adjust SDK dependency: v4.36.0 -> v4.38.0
* Sherlock dependency: v11.1.0 -> v11.1.1
### Added
* SendInAppPurchaseRevenueEventForVerification Custom Event added for Adjust IAP Verification.

## [6.5.0-preview.1] - 2024-04-17
### Changed
* _(iOS Only)_ Privacy Manifest post process logic reworked.

## [6.5.0-preview.0] - 2024-04-17
### Changed
* Handyman dependency: v5.3.1 -> v5.3.3
* Adjust SDK dependency: v4.36.0 -> v4.38.0
* Sherlock dependency: v11.1.0 -> v11.1.1
### Added
* _(iOS Only)_ Post Process for Privacy Manifest

## [6.4.0-prev.0] - 2024-04-04
### Added
* SendInAppPurchaseRevenueEventForVerification Custom Event added for Adjust IAP Verification.

## [6.3.0] - 2024-03-19
### Added
* Logging in session callbacks
### Changed
* Consent logic updated (See `Migration Guide`)
### Migration Guide
* Update `DataUsageConsentManager` to 11.0.0 or newer
* If you are using Usercentrics, update `DataUsageConsent-Usercentrics` to 1.1.0 or newer
* If you are using Usercentrics, make sure that `Adjust` and its partners (https://help.adjust.com/en/article/privacy-features-android-sdk#available-partners) is added as `Data Processing Service` and `Google Advertising Products (ID: 755)` in `Global Vendor List` in Usercentrics dashboard. Otherwise, Adjust can't get correct consent status.

## [6.2.0-preview.0] - 2024-03-12
### Added
* Initialization will fail on editor now
* Serializable Dictionary dependency
* Third party data sharing for available partners (https://help.adjust.com/en/article/privacy-features-android-sdk#available-partners) See `Migration Guide`
* If `Facebook SDK` is installed, package will send Facebook ID to Adjust SDK.
* `GetAdID()` method for getting Adjust AdID
### Changed
* Google DMA settings logic updated. (See `Migration Guide`)
* Adjust SDK dependency -> 4.36.0
* Getting AdjustID is more persistent now.
### Removed
* `Congfig.iAdInfoReading`: Adjust is not using this anymore.
### Migration Guide
* If you are updating from 6.1.2 and using Usercentrics: Update `DataUsageConsentManager` package to 10.0.0, install `DataUsageConsent-Usercentrics` package, and make sure that `Adjust`, `Google Ads` and `Facebook SDK` are added as Data Processing Service in Usercentrics dashboard. Otherwise, Adjust can't get correct consent status.

## [6.1.2] - 2024-02-29
### Changed
* Added Google DMA option calls.
### Migration Guide
* This version contains a hotfix and depends on special branch of Data Usage Consent SDK v9.3.2.

## [6.1.1] - 2024-02-21
### Changed
* Removed Google DMA option calls for Usercentrics implementation.

## [6.1.0] - 2024-02-16
### Added
* `FailureReason` implementation
### Changed
* Better timeout logic
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
* Sherlock dependency: v10.0.0 -> v11.1.0

## [6.0.3] - 2024-01-16
### Added
* Google DMA Third Party data sharing

## [6.0.2] - 2023-12-01
### Fixed
* Not compiling while building

## [6.0.1] - 2023-12-01
### Fixed
* Not compiling while building on Android

## [6.0.0] - 2023-11-28
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
* Sherlock dependency: v9.1.0 -> v10.0.0
* Adjust SDK 4.35.2 compatibility
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [5.5.1] - 2023-11-22
### Changed
* `GetAttribution()` API makes a call to `Adjust.getAttribution()` directly.

## [5.5.0-beta.1] - 2023-10-09
### Changed
* Set sandbox mode when `MG_DEBUG` is defined (Dev Build).

## [5.4.0] - 2023-08-01
### Added
* `GetAttribution(): IAttributionInfo` for getting adjust attribution info.
  * When attribution is not available, it returns a null object that returns empty values for each property of the interface.
* `SetAttributionChangeCallback(AttributionChangeDelegate): void` for getting notifications for when the adjust attribution changes. 
  * See https://github.com/adjust/unity_sdk#ad-attribution-callback
### Changed
* `com.adjust.sdk` using statement compilation is no longer dependent on AMR import state.

## [5.3.0] - 2023-05-16
### Added
* More config options from Adjust prefab to `ServiceConfig`
* Offline mode implementation
* Pause state implementation for Android

## [5.2.0] - 2023-05-04
### Added
* `AdjustID`: Cached Adjust AdID
### Changed
* _Internal_: `AppToken` handling refactor

## [5.1.3] - 2023-03-01
### Changed
* Compile fix when Admost MGSDK Bridge is not present

## [5.1.2] - 2023-02-24
### Changed
* UnityException fix on various internal callbacks from Adjust after updating it to 4.33.0+

## [5.1.1] - 2023-02-24
### Changed
* UnityException fix on sending Adjust ID to Admost

## [5.1.0] - 2023-01-09
### Changed
* Uninstall token support on iOS is back (Charlie is necessary for this)
* Token check on initialization

## [5.0.0] - 2022-11-23
### Changed
* `SendCustom`: `Dictionary` => `IDictionary`

## [4.0.2] - 2022-11-16
### Changed
* Other mediation sources enabled on `SendAdRevenue` method

## [4.0.1] - 2022-09-06
### Changed
* Compile error fix when `Meteor` isn't present

## [4.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`: All public fields and methods are static now.
* **Migration guide**: After update, close unity. Go to `PROJECTFOLDER/Assets/MatchinhamGames/Resources/Config`. Rename `MGAdjustAnalyticsConfig.asset` and `MGAdjustAnalyticsConfig.asset.meta` to `AdjustAnalyticsServiceConfig`

## [3.4.3] - 2022-08-10
### Changed
* IAdjustAnalyticsService: `float` fields changed to `double` in all methods 
* Log suppression fix on initialization
* `iapPriceRevenueFactor` default value is 0.85f

## [3.4.2] - 2022-07-21
### Changed
* CheckReady fix

## [3.4.1] - 2022-07-06
### Changed
* Sherlock dependency bump

## [3.4.0] - 2022-07-06
### Changed
* IsReady -> CheckReady
* Injector moved to MGAdjustAnalytics
* serviceConfig is static now
* OnDestroy override fix

## [3.3.5] - 2022-06-22
### Fixed
* Added log for send ad revenue method
* Send methods won't work when IsReady is false.

## [3.3.4] - 2022-06-22
### Fixed
* SendTokenForUninstallTrack lines commented for Unity 2022.1 compatiblity. This method will be revised in future.

## [3.3.3] - 2022-06-14
### Fixed
* ATT status is given now. (Sherlock must be initialized after ATT result is set)

## [3.3.2] - 2022-06-02
### Changed
* Package dependencies updated

## [3.3.1] - 2022-05-13
### Fixed
* Compatible with Adjust 4.30.0+

## [3.3.0] - 2022-04-27
### Changed
* SendAdRevenue method parameters overhaul
* Sherlock dependency updated
* Revenue reporting of Admost is available now

## [3.2.0] - 2022-04-12
### Added
* A new adrevenue method for IAdjustAnalyticsService

## [3.1.0] - 2022-03-22
### Added
* Environment option added to config.

## [3.0.2] - 2022-03-16
### Fixed
* Compile fix when meteor is not installed.

## [3.0.1] - 2022-03-10
### Fixed
* Compile fix on iOS target

## [3.0.0] - 2022-03-09
### Changed
* Namespaces
* Old Meteor dependency removed

## [2.0.0] - 2022-03-09
* Initial release
