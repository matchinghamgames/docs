# Changelog

## [5.0.0] - 2025-02-19
### Added
* If `Config.DefaultAudioSourcePrefab` is null, initialization will fail.
### Changed
* Handyman dependency: v5.8.2
* Minimum Unity version 2022.3
* When there was a reference for `Config.DefaultAudioSourcePrefab`, but the referenced object is not found, default prefab won't be set, a warning message will be displayed on console.

## [4.1.1] - 2024-10-25
### Added
* Added `AudioMixerGroupVolumePreset.Validations.cs` to contain validation methods for `AudioMixerGroupVolumePreset.cs`

## [4.1.0] - 2024-06-13
### Changed
* `PlaySingleAudio`, `PlayLoopedAudio`: now returns `AudioSource`
### Fixed
* Rare exception if the gotten pooled `GameObject` from `AudioSourcePools` has no `AudioSource` component.

## [4.0.1] - 2024-05-06
### Removed
* _(Internal)_ Redundant code removal
### Added
* Namespaces for sample scripts
### Changed
* Handyman dependency: v5.0.0 -> v5.5.0
* Logger dependency: v2.0.0 -> v2.1.0

## [4.0.0] - 2023-11-17
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
### Removed
* `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.

## [3.0.0] - 2023-04-26
### Changed
* Initial release