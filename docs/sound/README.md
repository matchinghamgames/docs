# Apollo Audio Module

## Overview
Audio management with object pooling and Unity's AudioMixer

## Installing
* Import package from Package Manager UI

## How to use
* Make sure to call `Apollo.Instance.Initialize()` at the appropriate place.
* Apollo supports `AudioMixer`s. But `AudioMixer` configuration must be manually done.
  * To change an `AudioMixerGroup`'s volume by using [`AudioMixerGroupVolumePreset`](#audiomixergroupvolumepreset). You must "expose" the volume parameter.
    * To do so, click on an `AudioMixerGroup`, right click on the `Volume` text on the inspector, then click `Expose`
    * Then, click on the `Exposed Parameters` in the `Audio Mixer` tab. Rename the parameter previously you exposed.
    * Now you can add [AudioMixerGroupVolumePreset](#audiomixergroupvolumepreset) in the `Audio Mixer Group Volume Presets` in the configuration

## Sample
* When you import the sample, you need to re-configure `ApolloConfig`. To do that, open the scene comes with the sample. Go to `GameManager` and click `Export To Apollo Config`. Now you can play the sample scene.

## Configuration
Go to > `Matchingham > Audio Config`

| Variable                         | Description                                                                                                                                |
|----------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| Enabled                          | Enabling/disabling the module                                                                                                              |
| Auto Initialize                  | If enabled, you don't need to call Initialize method manually.                                                                             |
| Default Audio Source Prefabs     | The prefab that will be used on initializing the pool. **If you will change this, make sure that the prefab has `AudioSource` component.** |
| Audio Pool Init Size             | Initial size of the audio pool.                                                                                                            |
| Audio Pool Max Size              | Maximum size of the audio pool. If total element count exceeds maximum size, releasing an element means destroying it.                     |
| Audio Source Presets             | Presets for `AudioSource`s. See [AudioSourcePresets](#audiosourcepreset)                                                                   |
| Audio Mixer Group Volume Presets | Volume presets for `AudioMixerGroup`s. See [AudioMixerGroupVolumePreset](#audiomixergroupvolumepreset)                                     |
| Audio Mixer Snapshots            | The list for `AudioMixerSnaaphot`s                                                                                                         |

## API & Details

### Module

#### Fields

* **AudioSourcePools**
  : `GameObjectPool` of `AudioSource`s

#### Methods

* **FindAudioMixerGroupVolumePreset(string groupName) : AudioMixerGroupVolumePreset**
  : returns [`AudioMixerGroupVolumePreset`](#audiomixergroupvolumepreset) with given `groupName`.

* **TryGetAudioSourcePreset(string id, out AudioSourcePreset audioSourcePreset) : bool**
  : returns if an [`AudioSourcePreset`](#audiosourcepreset) exists with given `id`. If exists, returns the [`AudioSourcePreset`](#audiosourcepreset) too.

* **FindAudioMixerSnapshot(string snapshotName) : AudioMixerSnapshot**
  : returns `AudioMixerSnapshot` with given `snapshotName`.

* **GetEmptyAudioSource() : AudioSource**
  : gets an empty audio source from `AudioSourcePools` and returns it

* **TryGetAudioSource(string id, out AudioSource audioSource) : bool**
  : returns if an [`AudioSourcePreset`](#audiosourcepreset) exists with given `id`. If exists, gets an empty audio source from `AudioSourcePools`, applies the [`AudioSourcePreset`](#audiosourcepreset) then returns it.

* **ReleaseAudioSource(AudioSource audioSource)**
  : releases the `AudioSource` to `AudioSourcePools`

* **ApplyPresetToSource(AudioSource audioSource, AudioSourcePreset sourcePreset)**
  : applies `sourcePreset` to `audioSource`

* **PlaySingleAudio(string id, Transform parent = null) : AudioSource**
  : gets an empty audio source from `AudioSourcePools`, applies the [`AudioSourcePreset`](#audiosourcepreset) with given `id`, sets the parent as `parent`, plays one shot, then releases the `AudioSource` to the `AudioSourcePools`.

* **PlaySingleAudio(string id, Vector3 position, Quaternion rotation, Transform parent = null) : AudioSource**
  : gets an empty audio source from `AudioSourcePools`, applies the [`AudioSourcePreset`](#audiosourcepreset) with given `id`, sets the parent as `parent`, the world position as `position`, the world rotation as `rotation`, plays one shot, then releases the `AudioSource` to the `AudioSourcePools`. 

* **PlayLoopedAudio(string id, Transform parent, out Action stopAction) : AudioSource**
  : gets an empty audio source from `AudioSourcePools`, applies the [`AudioSourcePreset`](#audiosourcepreset) with given `id`, sets the parent as `parent`, plays on loop, returns `stopAction`. When `stopAction` invokes, it releases the `AudioSource` to the `AudioSourcePools`.

* **PlayLoopedAudio(string id, Vector3 position, Quaternion rotation, Transform parent, out Action stopAction) : AudioSource**
  : gets an empty audio source from `AudioSourcePools`, applies the [`AudioSourcePreset`](#audiosourcepreset) with given `id`, sets the parent as `parent`, the world position as `position`, the world rotation as `rotation`, plays on loop, returns `stopAction`. When `stopAction` invokes, it releases the `AudioSource` to the `AudioSourcePools`.

### AudioSourcePreset

#### Fields (Serialized)

* **Audio Mixer Group**
  : If null, the clip is output directly to the `AudioListener` in the Scene. Use this property to output the clip to an `AudioMixer` instead.

* **Audio Clips**
  : Reference to the sound clip files that will be played.

* **RandomClip**
  : Selects a random clip from `AudioClips`

* **Bypass Effects**
  : This is to quickly “by-pass” filter effects applied to the audio source. An easy way to turn all effects on/off.

* **Bypass Listener Effects**
  : This is to quickly turn all Listener effects on/off.

* **Bypass Reverb Zones**
  : This is to quickly turn all Reverb Zones on/off.

* **Priority**
  : Determines the priority of this audio source among all the ones that coexist in the scene. (Priority: 0 = most important. 256 = least important. Default = 128.). Use 0 for music tracks to avoid it getting occasionally swapped out.

* **Volume**
  : How loud the sound is at a distance of one world unit (one meter) from the `AudioListener`

* **Pitch**
  : Amount of change in pitch due to slowdown/speed up of the Audio Clip. Value 1 is normal playback speed.

* **Stereo Pan**
  : Sets the position in the stereo field of 2D sounds.

* **Custom Spatial Blend Curve**
  : If you want to use a custom spatial blend distance function or not

* **Spatial Blend**
  : Sets how much the 3D engine has an effect on the audio source.

* **Spatial Blend Curve**
  : 2D (original channel mapping) to 3D (all channels downmixed to mono and attenuated according to distance and direction).

* **Custom Reverb Zone Mix Curve**
  : If you want to use a custom reverb zone distance function or not

* **Reverb Zone Mix**
  : Sets the amount of the output signal that gets routed to the reverb zones. The amount is linear in the (0 - 1) range, but allows for a 10 dB amplification in the (1 - 1.1) range which can be useful to achieve the effect of near-field and distant sounds.

* **Reverb Zone Mix Curve**
  : Amount of signal routed to the reverb zones. Note that the volume property and distance and directional attenuation are applied to the signal first and therefore affect both the direct and reverberated signals.

* **Doppler Level**
  : Determines how much doppler effect will be applied to this audio source (if is set to 0, then no effect is applied).

* **Custom Spread Curve**
  : If you want to use a custom spread distance function or not

* **Spread**
  : Sets the spread angle to 3D stereo or multichannel sound in speaker space.

* **Spread Curve**
  : Angle (degrees 0.0 - 360.0) over distance.

* **Volume Rolloff**
  : How fast the sound fades. The higher the value, the closer the Listener has to be before hearing the sound. (This is determined by a Graph).

* **minDistance**
  : Within the MinDistance, the sound will stay at loudest possible. Outside MinDistance it will begin to attenuate. Increase the MinDistance of a sound to make it ‘louder’ in a 3d world, and decrease it to make it ‘quieter’ in a 3d world.

* **maxDistance**
  : The distance where the sound stops attenuating at. Beyond this point it will stay at the volume it would be at MaxDistance units from the listener and will not attenuate any more.

* **volumeCurve**
  : Amplitude(0.0 - 1.0) over distance.

### AudioMixerGroupVolumePreset

#### Fields (Serialized)

* **AudioMixerGroup**
  : Reference to the `AudioMixerGroup`

* **ExposedParameter**
  : Name of the `ExposedParameter` in `AudioMixer` of the `AudioMixerGroup`

* **MaxVolume**
  : Maximum volume of the `AudioMixerGroup` in dB (between -80 and 20)

* **MaxVolumeLinear**
  : Maximum volume of the `AudioMixerGroup` in linear (between 0 and 10)

* **CurrentVolume**
  : Current volume of the `AudioMixerGroup` in dB

* **CurrentVolumeLinear**
  : Current volume of the `AudioMixerGroup` in linear

* **InitialVolume**
  : Initial volume of the `AudioMixerGroup` in dB

* **InitialVolumeLinear**
  : Initial volume of the `AudioMixerGroup` in linear

* **DontSetVolumeFromStashOnInit**
  : If you want not to set the volume of the `AudioMixerGroup` on module initialization

#### Methods

* **SetVolumeLinear(float value, bool autoSave = true)**
  : Set volume of the `AudioMixerGroup` in linear

* **SetVolume(float value, bool autoSave = true)**
  : Set volume of the `AudioMixerGroup` in dB

### Common

#### Methods

* **Initialize()**
  : Starts module initialization. You need to call this at the appropriate place.

* **WhenInitialized(Action callback)**
  : Allows you to register `callback` that will be fired only after the module is successfully initialized. Use this to execute logic that requires this module to be initialized first. If the module has already initialized, immediately invokes the callback.

* **WhenFailedToInitialize(Action callback)**
  : Allows you to register `callback` that will be fired only after the module fails to initialize for any reason. Use this to handle what should happen in case this module fails to initialize. If the module has already failed to initialize, immediately invokes the callback.

* **WhenReady(Action callback)**
  : Combined version of `WhenInitialized` and `WhenFailedToInitialize`. Delays execution of `callback` till module is first initialized or failed to initialize, immediately invoke the callback if it is already initialized or failed to initialize.

#### Fields

* **State**
  : Initialization state of the module

* **Instance**
  : Instance of the module

* **LateInitialized**
  : When the module needs an internet connection but the player became online while playing the game, this becomes `true`

* **Ready**
  : If the module is initialized successfully and ready to operate

* **Config**
  : Configuration of the module. See [configuration](#configuration)

* **InitializationDuration**
  : Initialization duration in seconds