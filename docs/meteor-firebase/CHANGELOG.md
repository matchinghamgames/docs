# Changelog

## [5.1.0] - 2024-02-16
### Added
* `FailureReason` implementation
* Config: `CustomConfigSettings`: You can skip using custom config settings by setting this to false
* Config: `MinimumFetchInterval`: The minimum interval between successive fetch calls.
* Config: `CustomFetchCacheDuration`: You can set a custom cache duration for fetched values.
* Config: `CustomInitializationTimeoutContext`: You can set timeout context as the default/custom for initialization.
* Config: `StartTimeoutBeforeFirebaseSdkReady`: Start timeout before/after Firebase SDK ready.
* Config: `StopTimeoutAfterFetchingComplete`: Stop timeout after/before fetching complete.
* Better exception messages
### Changed
* Custom timeout logic (see `Added`)
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
* Meteor dependency: v5.0.0 -> v5.1.0
* Firebase MGSDK Bridge dependency: 4.0.0 -> 4.1.0

## [5.0.0] - 2023-11-20
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
* Meteor dependency: v4.3.0 -> v5.0.0
* Firebase MGSDK Bridge dependency: 3.1.1 -> 4.0.0
* Newtonsoft Json dependency: v3.0.2 -> v3.2.1
* External Dependency Manager for Unity dependency: v1.2.176 -> v1.2.177
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [4.0.6] - 2023-07-11
### Updated
* Meteor -> 4.3.0
* Handyman Utility -> 4.3.2
* Logger -> 1.3.2

## [4.0.5] - 2023-05-12
### Fixed
* Null reference exception fix in editor time.

## [4.0.4] - 2023-03-09
### Fixed
* `FirebaseRemoteConfigSync`: it won't show remote values from other providers than Firebase

## [4.0.3] - 2022-09-07
### Fixed
* `int` serialization on editor sync menu

## [4.0.2] - 2022-09-02
### Fixed
* Timeout fix on initialization

## [4.0.1] - 2022-09-01
### Fixed
* Newtonsoft JSON dependency added

## [4.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`: All public fields and methods are static now.
### Added
* Sync support with Firebase servers added.

## [3.1.1] - 2022-07-22
### Changed
* Dependencies bump
* Initialization complete log order 

## [3.1.0] - 2022-06-22
### Changed
* Dependencies bump
* New Firebase MGSDK Bridge package compability

## [3.0.1] - 2022-03-15
### Fixed
* Registration to Meteor Module

## [3.0.0] - 2022-03-09
### Changed
* Namespaces

## [2.0.1] - 2022-03-09
### Fixed
* Namespaces  

## [2.0.0] - 2022-03-09
### Changed
* Namespace

## [1.3.9] - 2022-03-09
* Initial release