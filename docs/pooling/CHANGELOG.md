# Changelog

## [2.0.0] - 17-04-2023
### Changed
* Rework based on Unity's ObjectPool implementation

## [1.0.0] - 09-12-2021
### Changed
* `PoolManager` class added
* Provided extensions for `UnityEngine.Object`s.
