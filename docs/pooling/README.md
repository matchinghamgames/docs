# GameObject Pool

## Overview

Similar to Unity's new `ObjectPool` implementation, but with `GameObject`s and it is serializable.

## Installing

* Install package from Package Manager UI

## How to use

* Create an object using this class.
  * Configure it on the inspector and initialize or initialize on the go.

## API & Details

### Fields (Serialized)

* **Prefab**
  : Source of the GameObjectPool

* **PoolContainer**
  : Parent of the pooled GameObject

* **Initialized**
  : Called when the instance is being taken from the pool. If it's null, the instance will be taken as inactive

* **Got**
  : Called when the instance is being taken from the pool and initialized

* **Released**
  : Called when the instance is being returned to the pool. This could be used to clean up or disable the instance

* **Destroyed**
  : Called when the element can not be returned to the pool due to it being equal to the `MaxSize`

* **InitialSize**
  : The default capacity the stack will be created with

* **MaxSize**
  : The maximum size of the pool. When the pool reaches the max size then any further instances returned to the pool will be ignored and can be garbage collected. This can be used to prevent the pool growing to a very large size"
  
* **CollectionCheck**
  : Collection checks are performed when an instance is returned back to the pool. An exception will be thrown if the instance is already in the pool. Collection checks are only performed in the Editor

### Methods

* **`Initialize(GameObject prefab, Action<GameObject> actionOnInitialized = null, Transform poolContainer = null, Action<GameObject> actionOnGet = null, Action<GameObject> actionOnRelease = null, Action<GameObject> actionOnDestroy = null, bool collectionCheck = true, int maxSize = 10000, int defaultCapacity = 10)`**
  : Initializes the GameObjectPool. **Use this if you didn't set prefab and/or pool container on inspector.** `prefab`: Source of the pooled object. `actionOnInitialized`: Called when the instance is being taken from the pool. If it's null, the instance will be taken as not active. `poolContainer`: Parent of the pooled objects. `actionOnGet`: Called when the instance is being taken from the pool and initialized. `actionOnRelease`: Called when the instance is being returned to the pool. This could be used to clean up or disable the instance. `actionOnDestroy`: Called when the element can not be returned to the pool due to it being equal to the maxSize. `collectionCheck`: Collection checks are performed when an instance is returned back to the pool. An exception will be thrown if the instance is already in the pool. Collection checks are only performed in the Editor. `defaultCapacity`: The default capacity the stack will be created with. `maxSize`: The maximum size of the pool. When the pool reaches the max size then any further instances returned to the pool will be ignored and can be garbage collected. This can be used to prevent the pool growing to a very large size. 

* **`Initialize(Action<GameObject> actionOnInitialized = null, Action<GameObject> actionOnGet = null, Action<GameObject> actionOnRelease = null, Action<GameObject> actionOnDestroy = null, bool collectionCheck = true, int maxSize = 10000, int defaultCapacity = 10)`**
  : Initializes the GameObjectPool. **Use this if you set prefab and/or pool container on inspector.** `actionOnInitialized`: Called when the instance is being taken from the pool. If it's null, the instance will be taken as not active. `actionOnGet`: Called when the instance is being taken from the pool and initialized. `actionOnRelease`: Called when the instance is being returned to the pool. This could be used to clean up or disable the instance. `actionOnDestroy`: Called when the element can not be returned to the pool due to it being equal to the maxSize. `collectionCheck`: Collection checks are performed when an instance is returned back to the pool. An exception will be thrown if the instance is already in the pool. Collection checks are only performed in the Editor. `defaultCapacity`: The default capacity the stack will be created with. `maxSize`: The maximum size of the pool. When the pool reaches the max size then any further instances returned to the pool will be ignored and can be garbage collected. This can be used to prevent the pool growing to a very large size.

* **`Get() : GameObject`**
  : Get a GameObject from the pool.

* **`Get() : PooledGameObject`**
  : Get a new `PooledGameObject` which can be used to return the instance back to the pool when the PooledGameObject is disposed.

* **`Release()`**
  : Release a `GameObject` to the pool.

* **`Clear()`**
  : Destroys all pooled `GameObject`s so they can be garbage collected.

