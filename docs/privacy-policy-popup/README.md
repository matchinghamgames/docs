# Privacy Policy Popup

## Overview

This provides a simple way to show privacy policy, terms and conditions pages to the user. 

## Installing
* Import this package using Unity's Package Manger UI

## How to use
* Make sure that you have correct [configuration](#configuration).
* Call `PrivacyPolicy.Instance.Initialize()` for initialization.
* Use `PrivacyPolicy.DisplayPrivacyPopup()` method to display privacy policy.
* Use `PrivacyPolicy.DisplayTermsAndConditions()` method to display terms and conditions.

## Configuration
Go to `Matchingham > Privacy Policy`

| Variable                            | Description                                                                                                                                                                                                                                                                                                                                                                                                                                    |
|-------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Enabled                             | Enabling/disabling the module                                                                                                                                                                                                                                                                                                                                                                                                                  |
| AutoInitialize                      | If enabled, you don't need to call Initialize method manually.                                                                                                                                                                                                                                                                                                                                                                                 |
| Privacy Policy URL                  | URL of `Privacy Policy`. **It's for redirecting the player to an external website.**                                                                                                                                                                                                                                                                                                                                                           |
| Terms and Conditions URL            | URL of `Terms and Conditions`. **It's for redirecting the player to an external website.**                                                                                                                                                                                                                                                                                                                                                     |

## API & Details

### Privacy Policy

#### Fields

* **PrivacyPolicyURL**
  : Returns the privacy policy URL. **It's for redirecting the player to an external website.**

* **TermsAndConditionsURL**
  : Returns the terms and conditions URL. **It's for redirecting the player to an external website.**

#### Methods

* **DisplayPrivacyPolicy()**
  : Displays privacy policy from external web browser

* **DisplayTermsAndConditions()**
  : Displays terms and conditions from external web browse

### Common

#### Methods

* **Initialize()**
  : Starts module initialization. You need to call this at the appropriate place.

* **WhenInitialized(Action callback)**
  : Allows you to register `callback` that will be fired only after the module is successfully initialized. Use this to execute logic that requires this module to be initialized first. If the module has already initialized, immediately invokes the callback.

* **WhenFailedToInitialize(Action callback)**
  : Allows you to register `callback` that will be fired only after the module fails to initialize for any reason. Use this to handle what should happen in case this module fails to initialize. If the module has already failed to initialize, immediately invokes the callback.

* **WhenReady(Action callback)**
  : Combined version of `WhenInitialized` and `WhenFailedToInitialize`. Delays execution of `callback` till module is first initialized or failed to initialize, immediately invoke the callback if it is already initialized or failed to initialize.

#### Fields

* **State**
  : Initialization state of the module

* **Instance**
  : Instance of the module

* **LateInitialized**
  : When the module needs an internet connection but the player became online while playing the game, this becomes `true`

* **Ready**
  : If the module is initialized successfully and ready to operate

* **Config**
  : Configuration of the module. See [configuration](#configuration)

* **FailureReason**
  : If the module fails to initialize, this will contain the reason of the failure.

* **Logger**
  : Logger of the module
 
* **InitializationDuration**
  : Duration of the initialization process