# Changelog

## [8.0.0] - 2025-01-22
### Changed
* Minimum Unity version: 2022.3
* Module won't need internet connection to be initialized.
### Removed
* Opening in-game popup logic and related assets
* Fetching text from given URL logic
* In Game Markdown Renderer dependency

## [7.0.3] - 2024-08-06
### Added
* Privacy Policy and Terms & Conditions foldout groups added.

### Changed
* Privacy Policy and Terms & Conditions text area size reduction reverted.

## [7.0.2] - 2024-08-05
### Changed
* Privacy Policy and Terms & Conditions text area sizes reduced.

## [7.0.1] - 2024-04-18
### Added
* Android back button enable/disable flag in config.
* Android back button support.
* CloseActivePopup method to enable closing active popup outside assembly.

### Fixed
* HtmlUrl and MarkDownUrl labels all looks the same "Url" label fix.

### Changed
* HTML Privacy Policy URL and Terms & Conditions URL are not global and disconnected from display mode
* Added validation for HTML Privacy Policy and Terms & Conditions URLs
* Added validation for Markdown Privacy Policy and Terms & Conditions URLs

### Migration Guide
* URL fields in the config will be reset with this update! Please ensure the configuration is correctly setup!

## [7.0.0] - 2024-04-18
### Added
* Android back button enable/disable flag in config.
* Android back button support.
* CloseActivePopup method to enable closing active popup outside assembly.

### Fixed
* HtmlUrl and MarkDownUrl labels all looks the same "Url" label fix.

### Changed
* HTML Privacy Policy URL and Terms & Conditions URL are not global and disconnected from display mode
* Added validation for HTML Privacy Policy and Terms & Conditions URLs
* Added validation for Markdown Privacy Policy and Terms & Conditions URLs

### Migration Guide
* URL fields in the config will be reset with this update! Please ensure the configuration is correctly setup!

## [7.0.0-preview.2] - 2024-04-15
### Added
* Android back button enable/disable flag in config.

## [7.0.0-preview.1] - 2024-04-15
### Added
* Android back button support.
* CloseActivePopup method to enable closing active popup outside assembly.

### Fixed
* HtmlUrl and MarkDownUrl labels all looks the same "Url" label fix.

## [7.0.0-preview.0] - 2024-04-05
### Changed
* HTML Privacy Policy URL and Terms & Conditions URL are not global and disconnected from display mode
* Added validation for HTML Privacy Policy and Terms & Conditions URLs
* Added validation for Markdown Privacy Policy and Terms & Conditions URLs

## [6.0.1] - 2024-04-05
### Fixed
* Bug: Non trimmed urls causing exception on iOS native dialog

## [6.0.0] - 2024-03-25
### Fixed
* `DisplayPrivacyPolicy()` and `DisplayTermsAndConditions()` now works properly with `Config.Mode`
### Changed
* `PrivacyPolicyMode` -> `RedirectMode`: `InGameModal` -> `InGamePopup`, `Webpage` -> `OpenWebpage`
### Removed
* Obsolete methods: `ShowPrivacyPolicyPopup()`, `ShowTermsAndConditions()`
### Added
* `Config.Mode`: Became enum toggle button 

## [5.1.1] - 2024-03-18
### Fixed
* Fixed inconsistent define symbol requirement for the definition and usage of `ShouldShowInGameModalConfig()` and `ShowWebpageConfig()`

## [5.1.0] - 2024-03-15
### Added
* Config: `mode`: Select which privacy policy mode to use, `InGameModal` or `Webpage`
* Privacy Policy: `DisplayPrivacyPolicy()`: Display privacy policy according to mode setting
* Privacy Policy: `DisplayTermsAndConditions()`: Display terms and conditions according to mode setting
### Changed
* Privacy Policy: `ShowPrivacyPolicyPopup()`: Marked as obsolete, use `DisplayPrivacyPolicy()` instead
* Privacy Policy: `ShowTermsAndConditions()`: Marked as obsolete, use `DisplayTermsAndConditions()` instead
### Removed
* Config: `fetchTextFromURL`
### Migration Guide
* Previous version's configuration is **not compatible** with this version. Don't forget to update your configuration after updating the package.

## [5.0.0] - 2024-02-20
### Added
* Config: `privacyPolicyURL`: For redirecting the player to an external website, will be used in next `DataUsageConsentManager` release
* Config: `termsAndConditionsURL`: For redirecting the player to an external website, will be used in next `DataUsageConsentManager` release
* Config: `fetchTextFromURL`: When false, the module will use the texts provided at the configuration, instead of fetching from URLs
### Changed
* Config: `requestTimeout` -> `fetchRequestTimeout`
* Config: `privacyPolicyUrl`-> `privacyPolicyTextFetchURL`
* Config: `termsAndConditionsUrl` -> `termsAndConditionsTextFetchURL`
* Config: `termsAndConditions` -> `termsAndConditionsText` 
### Removed
* Config: `workOffline`. See `fetchTextFromURL`
### Fixed
* Offline mode had still required internet connection.
### Migration Guide
* Previous version's configuration is **not compatible** with this version. Don't forget to update your configuration after updating the package.

## [4.2.0] - 2024-02-16
### Added
* `FailureReason` implementation
### Changed
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
* In Game Markdown Renderer dependency: v1.0.1000 -> v1.1.1

## [4.1.0] - 2023-12-21
### Added
* Work Offline mode added. Make sure PP and TC texts are filled before enabling Offline mode

## [4.0.1] - 2023-12-04
### Changed
* Log levels about Privacy Policy URL and Terms & Conditions URL depend on default texts now.

## [4.0.0] - 2023-11-20
### Added
* Unity 2023.2.x compatibility (see `Migration Guide`)
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.1.5 -> v5.0.0
### Removed
* `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* TextMeshPro dependency:
    * Unity 2023.2 or newer users: TextMeshPro package is deprecated. Unity UI v2+ package includes TextMeshPro. It is necessary to remove TextMeshPro package.
    * Unity 2023.1 and older users: TextMeshPro package is no longer a dependency, but it is necessary to install to use this package properly.

## [3.0.3] - 2023-10-09
### Fixed
* Editor time 'Can't save immutable prefab' bug

## [3.0.2] - 2023-01-09
### Added
* New Debugger implementation

## [3.0.1] - 2022-10-03
### Changed
* `Config`: `Enabled` and `LogFlags` are serialized now

## [3.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`

## [2.0.1] - 2022-08-08
### Changed
* Popup sizes changed

## [2.0.0] - 2022-06-17
### Changed
* GetPrivacyPolicy() becomes PrivacyPolicyText
* GetTermsAndConditions() becomes TermsAndConditionsText
* Config fetching revised

## [1.3.0] - 2022-06-06
### Changed
* Code cleanup
* Logger implementation
* Timeout Manager implementation
* Initialization overhaul

## [1.2.4] - 2022-03-09
### Changed
* Initial release
