# Court Rating/Review Module

## Introduction

Provides a simple API to ask user to review the app. Most of the configuration parameters can be changed remotely when `Meteor` module is present.

## Installing

* Import package from Package Manager UI.
* Open module settings from `Matchingham > App Review`. See [Configuration](#configuration)

## Configuration
| Variable                                | Description                                                                                                                                                       |
|-----------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Enabled                                 | Enabling/disabling Court                                                                                                                                          |
| AutoInitialize                          | If enabled, you don't need to call Initialize method manually.                                                                                                    |
| Prompt Prefab                           | In-game UI (prefab) that the player will see before native rating popup when `Biased Request` is enabled. See [Custom In-App Review UI](#custom-in-app-review-ui) |
| Use Biased Request iOS                  | If you want to show an in-game UI before native rating popup on iOS, make sure this is enabled.                                                                   |
| Use Biased Request Android              | If you want to show an in-game UI before native rating popup on Android, make sure this is enabled.                                                               |
| Minimum Stars Required                  | When `Biased Request` is enabled, if the player gives less than this amount of stars, native rating popup won't be opened.                                        |

## How to use
* Call `Court.Instance.Initialize()` to initialize module.
* Call `RequestJudgement(Action)` anywhere after initialization to ask user to review the app.

## Custom In-App Review UI

In-app review UI is a prefab with a script an attached script inheriting from `RatingPromptBase` class. You can either create a variant of the prefab to make simple adjustments or create a completely new prefab with a script inheriting from `RatingPromptBase` class attached. You can write your own script to implement custom behaviour, just make sure it inherits from `RatingPromptBase`. Using a prefab that doesn't have an appropriate script attached will crash the app.

## API & Details

### Methods

* **RequestJudgement(Action callback)**
  : Starts rating flow. If `UseBiasedRequest` is true, in-game UI will be shown first. If the player gives at least `MinimumStarsRequired`, native rating popup will be shown then. If `UseBiasedRequest` is false, native rating popup will be shown immediately. `callback` will be invoked after native rating popup closes.

### Common

#### Methods

* **Initialize()**
  : Starts module initialization. You need to call this at the appropriate place.

* **WhenInitialized(Action callback)**
  : Allows you to register `callback` that will be fired only after the module is successfully initialized. Use this to execute logic that requires this module to be initialized first. If the module has already initialized, immediately invokes the callback.

* **WhenFailedToInitialize(Action callback)**
  : Allows you to register `callback` that will be fired only after the module fails to initialize for any reason. Use this to handle what should happen in case this module fails to initialize. If the module has already failed to initialize, immediately invokes the callback.

* **WhenReady(Action callback)**
  : Combined version of `WhenInitialized` and `WhenFailedToInitialize`. Delays execution of `callback` till module is first initialized or failed to initialize, immediately invoke the callback if it is already initialized or failed to initialize.

#### Fields

* **State**
  : Initialization state of the module

* **Instance**
  : Instance of the module

* **LateInitialized**
  : When the module needs an internet connection but the player became online while playing the game, this becomes `true`

* **Ready**
  : If the module is initialized successfully and ready to operate

* **Config**
  : Configuration of the module. See [configuration](#configuration)