# Changelog

## [4.0.0] - 2025-02-19
### Added
* If `Config.promptPrefab` is null, initialization will fail.
### Changed
* Handyman dependency: v5.8.2
* Minimum Unity version 2022.3
* When there was a reference for `Config.promptPrefab`, but the referenced object is not found, default prefab won't be set, a warning message will be displayed on console.  

## [3.1.0] - 2024-05-06
### Removed
* `Meteor` related logic. See `Migration Guide`.
### Changed
* Handyman dependency: v5.0.0 -> v5.5.0
* Logger dependency: v2.0.0 -> v2.1.0
### Migration Guide
* Configuration no longer has remote values. If you are using any remote values for this module. Make sure that you moved to local. This also enables this module to be able to be initialized before `Meteor`.

## [3.0.0] - 2023-11-20
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.1.5 -> v5.0.0
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Removed
* `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [2.0.4] - 2023-11-03
### Added
* Validation checks
### Fixed
* Doc path updates
* Unreachable code warning fix

## [2.0.3] - 2023-01-09
### Added
* New Debugger implementation

## [2.0.2] - 2022-09-27
### Fixed
* `CourtConfig`: `RemoteSetting` attribute usage was incorrect.
### Added
* `CourtDebugger`: `UseBiasedRequest` and `MinimumStarsRequired` from `CourtConfig` views

## [2.0.1] - 2022-09-06
### Changed
* `Meteor` initialization message is `warning` instead of `error` now

## [2.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`: All public fields and methods are static now.

## [1.2.0] - 2022-07-18
### Changed
* UseBiasedRequest is platform based now
* MinimumStarsRequired is remote value now
### Added
* Logger implementation
### Fixed
* Config wasn't registered to Meteor
* Asmdef rootNamespace was missing

## [1.1.4] - 2022-03-09
* Initial release