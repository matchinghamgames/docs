# Sherlock Unity

Unity Adapter for Sherlock Analytics Module. It contains special and custom event structures specified in Unity Analytics document [Unity - Manual: Standard Events (unity3d.com)](https://docs.unity3d.com/Manual/UnityAnalyticsStandardEvents.html)

* This adapter requires unity analytics enabled in your build.
* No initialization process, i.e. no time wasted in initialization.
* No dependency to other modules except Sherlock.
