# Changelog

## [5.0.0] - 2023-11-17
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.1.4 -> v5.0.0
* Sherlock dependency: v8.0.0 -> v10.0.0
* Analytics Library dependency: v3.6.12 -> v3.8.1
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.

## [4.0.0] - 2022-11-23
### Changed
* `SendCustom`: `Dictionary` => `IDictionary`
* LevelEvents: levelName won't be sent when levelIndex is not null

## [3.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`

## [2.0.4] - 2022-07-08
### Changed
* Compile error fix while building

## [2.0.3] - 2022-07-07
### Changed
* SendCustom instead of SendEvent in SendCustom and SendAdImpression 

## [2.0.2] - 2022-07-07
### Changed
* Compile error fix when ENABLE_CLOUD_SERVICES_ANALYTICS is false

## [2.0.1] - 2022-07-06
### Changed
* Sherlock dependency bump

## [2.0.0] - 2022-07-06
### Changed
* Injector moved to MGUnityAnalytics
* IUnityAnalyticsService moved to Sherlock
* SendLevelup -> SendLevelUp
* MGUnityAnalytics is inherited by AsyncSingleton instead of AsyncSingletonBehaviour
* Initialization result added to CheckReady method
* asmdef root namespace fix

## [1.0.0] - 2022-07-05
### Changed
* Initial Release
