# Changelog

## [7.1.1] - 2025-03-07
### Changed
* Optimize asset post processor for managing config containers.

## [7.1.0] - 2025-03-07
### Changed
* Config object list management is done automatically now.

## [7.0.0] - 2025-02-27
### Changed
* Handyman dependency: v7.1.0
* Min Unity version: 2022.3
* Log messages are simplified
* Configs are initialized before initializing the module or while registering them to the module
* Cache is initialized before initializing the module
* Internet connection is checked before initialing the services
* _(Internal)_ Small refactors
### Fixed
* `FailureReason` wasn't always set properly
* Cache wasn't updating if the module is late-initialized

## [6.1.1] - 2025-01-09
### Fixed
* Module correctly sets FailureReason when there is no network.

## [6.1.0] - 2025-01-07
### Changed
* Module no longer requires internet connection! Instead, when offline, it will use cached values with `FailedToInitialize` state

## [6.0.0] - 2024-08-06
### Added
* ✨Colors✨ in `RemoteSettingDrawer` and `RemoteConfigWindow` to distinguish providers. 
### Changed
* Handyman dependency: v5.3.1 -> v5.7.0
* `RemoteConfigWindow`: Won't be compiled if Odin Inspector is not installed.
### Removed
* _(Internal)_ `RemoteProviderUtilities`: Merged with `MeteorEditorUtility`

## [5.1.0] - 2024-02-16
### Added
* `FailureReason` implementation
### Changed
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
### Fixed
* `MeteorConfig`: Remote config variables sorted alphabetically by name with invariant culture.

## [5.0.3] - 2024-01-31
### Fixed
* Exception when there is a null object in `ConfigList`

## [5.0.2] - 2024-01-25
### Changed
* `MeteorConfig`: Remote config variables sorted alphabetically by name with invariant culture.

## [5.0.1] - 2023-12-04
### Changed
* `MeteorConfig`: Remote Variables will always be ordered by `Name` alphabetically.
* Possible editor performance improvements
### Migration Guide
* After update, you may see huge diff in `MeteorConfig` on git. Don't worry, it is very likely to happen due to ordering remote variables.

## [5.0.0] - 2023-11-20
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.0 -> v5.0.0
* Handyman dependency: v1.3.1 -> v1.4.0
* Newtonsoft Json dependency: v3.0.2 -> v3.2.1
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Removed
* (Obsolete) `UseAsDefaultValue`
* `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [4.3.5] - 2023-11-15
### Added
* Local cache invalidation on version change

## [4.3.4] - 2023-11-14
### Added
* `RemoteScriptableObject<T>` for specialized editor functionalities.

## [4.3.3] - 2023-08-03
### Fixed
* Bugfix: meteor editor cache is updated on every meteor init
### Changed
* MgCache folder moved to library folder, thus will be ignored by git

## [4.3.2] - 2023-07-28
### Changed
* Loading from cache and extracting value from config objects now replace null strings with empty strings.

## [4.3.1] - 2023-07-25
### Changed
* Editor initializer on delayed call, prevent `MeteorConfig` read failure error

## [4.3.0] - 2023-07-10
### Added
* Editor default value cache logic update.

## [4.2.1] - 2023-04-13
### Added
* Data Usage Consent Manager initialization dependency

## [4.2.0] - 2023-03-09
### Changed
* Internal `ConfigLists` became a getter of `MeteorConfig.ConfigLists`. This enables custom config support for auto-initialization.
### Added
* When an exception occurs during syncing values, the value will be in the log string.

## [4.1.1] - 2023-01-09
### Added
* New Debugger implementation
* `null` check on config registration

## [4.1.0] - 2022-12-23
### Changed
* Drawer for `RemoteSetting` attribute: Used Odin Inspector. Better distinction between consecutive remote settings. If `JSONParser` is used, a button added for copying the JSON serialized version of the object to the clipboard  

## [4.0.3] - 2022-09-23
### Fixed
* `Default Provider` label restored on config
### Added
* Auto fetching remote variables on config window opening

## [4.0.2] - 2022-09-07
### Fixed
* Duplicate menu scripts

## [4.0.1] - 2022-09-01
### Fixed
* Newtonsoft JSON dependency added

## [4.0.0] - 2022-09-01
### Added
* Custom drawer added to `[RemoteSetting]` attribute. Now, you can see remote setting name and provider on inspector.
* Properties are supported now. You must use them like this: `[field: RemoteSetting("variableName")]`
* Private fields are supported now.
* Objects with `[Serializable]` attribute are supported now. You must specify parser on `[RemoteSetting]` attribute (e.g. `[RemoteSetting("variableName", parser: RemoteSettingAttribute.Parser.JSON)]`)
* **Migration guide**: All provider names are changed. You must reset `MeteorConfig` and reconfigure providers.
### Changed
* Code refactor with `Handyman`: All public fields and methods are static now.

## [3.1.4] - 2022-07-28
### Fixed
* Compile errors on Unity 2020.3.x

## [3.1.3] - 2022-07-22
### Changed
* Dependencies bump
### Fixed
* Late initialization: Service Count log fix

## [3.1.2] - 2022-07-22
### Fixed
* Service initialization order fix
* Late Service initialization order fix

## [3.1.1] - 2022-07-22
### Fixed
* Service initialization order fix 

## [3.1.0] - 2022-06-02
### Changed
* **(Internal)** Meteor class is inherited from AsyncSingleton now.
### Added
* Initialization will fail when there is no service registered.

## [3.0.2] - 2022-04-28
### Changed
* **(Internal)** Logger implementation rework
* **(Internal)** code cleanup
### Fixed
* Service registration duplication fix
### Removed
* Dispose method

## [3.0.1] - 2022-03-15
### Fixed
* Null ref exception when more than two services registered.

## [3.0.0] - 2022-03-09
### Changed
* Namespace Revert

## [2.0.0] - 2022-03-09
### Changed
* Namespace

## [1.4.1] - 2022-03-09
* Initial release