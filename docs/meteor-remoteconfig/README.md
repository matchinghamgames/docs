# Meteor Remote Config Module

## Overview

Meteor module provides a unified methodology for receiving remote configuration from different sources, or even multiple
sources at the same time.

## Installing

* Install package from Package Manager UI
* Install one or more Meteor Service Packages from Package Manager UI. These services are what actually handle
  fetching and loading remote settings.

## How to use

* Open settings from `Matchingham > Remote Settings > Configure`. You can also configure services individually
  from `Matchingham > Remote Settings > Services`.
* Put the settings you want to be remote configurable into `Scriptable Object`s. **Meteor requires remote objects to be
  in `Scriptable Object`s or they cannot be tracked!**
    * To mark an object as remote, simply enter `[RemoteSetting("variableName")]` attribute **just above** the object
      declaration.
      For example:
  ```csharp
  [RemoteSetting("variableName")]
  public bool variableName;
  ```
    * If you want multiple different fields to be managed by the same remote config field, you need to mark one of them
      the default value. So in case remote fetch fails, the system will have a default value. This is achieved by
      adding `true` next to `variableName` in the attribute:
      ```csharp
      public class MyConfig : ScriptableObject
      {
          [RemoteSetting("myConfigEnabled", useAsDefault: true)]
          public bool enabled;
      }
      
      public class TheirConfig : ScriptableObject
      {
          [RemoteSetting("myConfigEnabled")]
          public bool isMyConfigEnabled;
      }
      ```
    * If you want to use a custom class or `List` or `Array`, rather than primitive types and `string`, make sure that
      the object's class and its fields are serializable. And add `parser: RemoteSettingAttribute.Parser.JSON` next
      to `variableName` in the attribute:
  ```csharp
  [Serializable]
  public class CustomClass
  {
    public bool a;
    public int b;
  }
  
  [RemoteSetting("customObject", parser: RemoteSettingAttribute.Parser.JSON)]
  public CustomClass customObject;
  
  [RemoteSetting("customObjectArray", parser: RemoteSettingAttribute.Parser.JSON)]
  public CustomClass[] customObjectArray;
   
  [RemoteSetting("customIntList", parser: RemoteSettingAttribute.Parser.JSON)]
  public List<int> customIntList;
  ```
    * You can use properties **if it has a setter**. And add `field: ` **before** the `RemoteSetting` attribute:
  ```csharp
  [field: SerializeField] // if you want to serialize it, you need this as well
  [field: RemoteSetting("variableName")]
  public bool variableName { get; set; }
  ```
    * After v4.3.4 you can use `RemoteScriptableObject<T>` object with RemoteConfigService of your choice to make use of specialized editor functionalities. 
	For example;
      ```csharp
      public class MyConfig : RemoteScriptableObject<AdmostRemoteConfigService>
      {
          [RemoteSetting("myAdmostConfig")]
          public bool enabled;
      }
	  
      public class MyConfig : RemoteScriptableObject<FirebaseRemoteConfigService>
      {
          [RemoteSetting("myFirebaseConfig")]
          public bool enabled;
      }
      ```
* For initialization:
    * `ScriptableObject`s created to feed data provider has to be registered before initialization. To do that these
      objects need to be added in **configList** list. For example like this;

      ```csharp
      myConfig = Resources.Load<MyConfig>($"Config/{nameof(MyConfig)}");
      Meteor.RegisterConfig(myConfig);
      ```

    Or, you must add them to `Config Lists` in [configuration](#configuration)
    * Make sure to call `Meteor.Instance.Initialize()` method in the appropriate place. **Most of the modules uses
      Meteor. Thus, it is important to initialize Meteor before others.**

* After initialization, just get values from the `ScriptableObject`s like you normally do, Meteor will replace the
  values with the remote ones. **You need to access the same `ScriptableObject` registered to Meteor. Even if you have the same reference to the `ScriptableObject`, they will have different address on the memory. In other words, they will become different instances of the same `ScriptableObject`.**

  ```csharp
  myConfigAgain = Resources.Load<MyConfig>($"Config/{nameof(MyConfig)}");

  Debug.Log(myConfig.enabled == myConfigAgain.enabled) // output will always be false if myConfig.enabled was different initially and changed by Meteor.
  ```

## Configuration

Meteor collects fields and properties having `RemoteSetting` attribute in scriptable objects that registered to Meteor.
It then maps them to a default remote provider if there are any imported. These mapping data can be changed from the
module settings from `Matchingham > Remote Settings > Configure` menu.

When initialized, meteor first crates a dictionary holding the default values for each remote setting. Then it attempts
to initialize all imported remote provider services with the collected default values passed in as fall back. When a
service is initialized, it notifies Meteor that its values are ready, so meteor syncs remote settings that are mapped to
the initialized service. When all services are initialized, meteor initialization is complete.

The only thing you need to do at this point is, mapping some remote settings to the new provider service you coded. Just
like with other services, you can do this from Meteor settings from `Matchingham > Remote Settings > Configure` menu.

| Variable                           | Description                                                                                                                                                                                                                                                                 |
|------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Enabled                            | Enabling/disabling Meteor                                                                                                                                                                                                                                                   |
| AutoInitialize                     | If enabled, you don't need to call Initialize method manually.                                                                                                                                                                                                              |
| Default Provider                   | Default provider for remote variables. When Meteor detects a remote variable, Meteor will attach this provider.                                                                                                                                                             |
| Remote Variables                   | You can observe remote variables and change the providers of them.                                                                                                                                                                                                          |
| Config Lists                       | You can observe fetched module config files by Meteor. This are also used for syncing with remote providers on editor (Currently only Firebase is supported). Thus, it is **recommended** to put your **own** `ScriptableObject`s that have `RemoteSetting` attribute here. |

## API & Details

### Common

#### Methods

* **Initialize()**
  : Starts module initialization. You need to call this at the appropriate place.

* **WhenInitialized(Action callback)**
  : Allows you to register `callback` that will be fired only after the module is successfully initialized. Use this to execute logic that requires this module to be initialized first. If the module has already initialized, immediately invokes the callback.

* **WhenFailedToInitialize(Action callback)**
  : Allows you to register `callback` that will be fired only after the module fails to initialize for any reason. Use this to handle what should happen in case this module fails to initialize. If the module has already failed to initialize, immediately invokes the callback.

* **WhenReady(Action callback)**
  : Combined version of `WhenInitialized` and `WhenFailedToInitialize`. Delays execution of `callback` till module is first initialized or failed to initialize, immediately invoke the callback if it is already initialized or failed to initialize.

#### Fields

* **State**
  : Initialization state of the module

* **Instance**
  : Instance of the module

* **LateInitialized**
  : When the module needs an internet connection but the player became online while playing the game, this becomes `true`

* **Ready**
  : If the module is initialized successfully and ready to operate

* **Config**
  : Configuration of the module. See [configuration](#configuration)

* **InitializationDuration**
  : Initialization duration in seconds

## Services

### Admost Remote Config Service

If you are using `Admost` service, make sure that you have entered `Application ID` in `Admost Config`. You can find it
on `Matchingham > Admost Config`. After entering the required information, go to `File > Save Project`

### Firebase Remote Config Service

If you are using `Firebase` service, make sure that your configuration is correct. [Make sure that you read Firebase's documentation](https://firebase.google.com/docs/unity/setup)

There is additional configuration for Firebase. You can go to `Matchingham > Remote Settings > Services > Firebase Config`

| Variable           | Description                                                                                                                                                                                                                                          |
|--------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| fetchTimeout       | The timeout specifies how long the client should wait for a connection to the Firebase Remote Config servers.                                                                                                                                        |
| fetchCacheDuration | The amount of time to keep previously fetch data available in milliseconds. If cached data is available that is newer than this value, then the function returns immediately and does not fetch any data. A value of zero will always cause a fetch. |

Also, `Firebase` enables us to operate in edit mode. Thus, we can sync values from Firebase. You can do it by `Matchingham > Remote Settings > Services > Firebase Remote Config Sync`

### GameAnalytics Remote Config Service

If you are using `GameAnalytics` service, make sure that your configuration is correct. You can find it on `Window > GameAnalytics > Select Settings` [Make sure that you read GameAnalytics' documentation](https://gameanalytics.com/docs/s/topic/0TO6N000000XZDuWAO/unity-sdk)

