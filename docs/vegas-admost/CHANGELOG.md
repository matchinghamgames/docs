# Changelog

## [10.1.1] - 2024-05-13
### Changed
* Updated vegas ad interface implementations
* Replace `DateTime.UtcNow` with `DateTimeUtility.UtcNow`

## [10.1.0] - 2024-02-14
### Added
* `FailureReason` implementation
### Changed
* Admost MGSDK Bridge dependency: v5.0.0 -> v6.0.0
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
* Vegas dependency: v12.0.0 -> v13.2.0

## [10.0.0] - 2023-11-17
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
* Vegas dependency: v10.0.0 -> v12.0.0
* Admost MGSDK Bridge dependency: v4.1.1 -> v5.0.0
* External Dependency Manager for Unity dependency: v1.2.169 -> v1.2.177
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Removed
* Adjust SDK asmdef dependency
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [9.0.0] - 2023-06-02
### Changed
* Vegas v10 update

## [8.0.0] - 2023-05-12
### Changed
* Rework with Vegas v9

## [7.0.0] - 2022-11-08
### Changed
* App Open Ads implementation: It's not supported by Admost, but it's implemented because of common interface.

## [6.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`
* **Migration guide**: After vegas-admost update, close unity. Go to `PROJECTFOLDER/Assets/MatchinhamGames/Resources/Config`. Rename `MGAdmostMediationConfig.asset` and `MGAdmostMediationConfig.asset.meta` to `AdmostMediationServiceConfig`.
Rename `MGAdmostAdaptersContainerConfig.asset` and `MGAdmostAdaptersContainerConfig.asset.meta` to `AdmostAdaptersContainerConfig` and move them to `PROJECTFOLDER/Assets/MatchinhamGames/Editor/Config`

## [5.1.1] - 2022-05-17
### Changed
* Admost Bridge dependency bump

## [5.1.0] - 2022-05-17
### Added
* Debugging mediation implemented

## [5.0.4] - 2022-05-16
### Changed
* Some build processor scripts migrated to Admost MGSDK Bridge

## [5.0.3] - 2022-04-29
### Changed
* JSON Downloader is back, but stripped AMR adapter.
* Preprocessbuild on iOS reverted
* Preprocessbuild on Android reverted
* Postprocessbuild on iOS reverted
* Admost Bridge dependency bump

## [5.0.2] - 2022-04-29
### Changed
* Vegas dependency updated

## [5.0.1] - 2022-04-29
### Changed
* iOS and Android panes replaced.
### Fixed
* Preprocessbuild on iOS
* Preprocessbuild on Android
* Postprocessbuild on iOS

## [5.0.0] - 2022-04-27
### Added
* Impression events implemented
* Integration manager methods mostly (for downloading JSON from Admost) migrated to Admost MGSDK Bridge
### Changed
* AdInfo replaced by AdData

## [4.0.1] - 2022-04-19
### Changed
* Dependency overhaul

## [4.0.0] - 2022-04-15
### Added
* AdInfo struct for better ad data
### Changed
* MGAdmostMediation is AsyncSingleton instead of AsyncSingletonBehaviour
* OnEcpmEventSent event is now EcpmValueObtained
* From now on, Vegas Admost Service won't initialize Admost SDK. Initialization is moved to another package called Admost MGSDK Bridge. Vegas Admost Service will initialize after that package.
### Removed
* ECPM methods moved to Vegas Module
* ECPM thresholds moved to Vegas Module

## [3.0.3] - 2022-03-16
### Fixed
* Compile fix when meteor firebase is not installed

## [3.0.2] - 2022-03-16
### Fixed
* Compile fix when meteor is not installed

## [3.0.1] - 2022-03-10
### Changed
* Compile fix on iOS target

## [3.0.0] - 2022-03-09
### Changed
* Initial release
