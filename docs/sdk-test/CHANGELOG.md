# Changelog

## [4.1.2] - 10-03-2023
- the changes in 4.1.2 version of the code

## [4.1.1] - 10-03-2023
* `PreProcessBuildAndroid` removed due to new `IsApiHttps` API

## [4.1.0] - 03-03-2023
* `IsApiHttps` added

## [4.0.4] - 03-10-2022
* `Vegas - Admost` config file renaming fixed

## [4.0.3] - 06-09-2022
* `DataUsageConsentManager` initialization message is `warning` instead of `error`

## [4.0.2] - 05-09-2022
* DUCM initialization enforcement

## [4.0.1] - 02-09-2022
* Initialization moved to AfterSceneLoad

## [4.0.0] - 01-09-2022
* Code refactor with `Handyman`

## [3.0.1] - 17-06-2022
* Data Usage Consent Manager API change
* If it's already initialized, it won't be intialized again.

## [3.0.0] - 01-06-2022
* Data Usage Consent module dependency is now optional. Thus, you can manually set User Consent values.
* Option for disabling auto initialization added.
* COPPA value added

## [2.2.2] - 17-05-2022
* Compile fix

## [2.2.1] - 16-05-2022
* Build processor scripts migration from vegas-admost package

## [2.2.0] - 29-04-2022
* Admost Config Menu -> Admost Config Window : This enables AMR adapter to be added to the project.

## [2.1.0] - 29-04-2022
* JSON Downloader stripped

## [2.0.1] - 27-04-2022
* Admost SDK depedency updated

## [2.0.0] - 27-04-2022
* Admost adapter integration added

## [1.0.0] - 15-04-2022
* Initial release
