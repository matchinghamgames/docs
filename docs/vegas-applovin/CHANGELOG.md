# Changelog

## [13.1.1] - 2025-03-06
### Changed
* Start/End display time data set at appropriate places for Interstitial, Rewarded Video and AppOpen ads

## [13.1.0] - 2025-02-07
### Added
* Amazon Publisher Services plugin v2+ migration (See `Migration Guide`)
### Fixed
* `_enableAndroidExtraBannerFeatures`, `_enableAndroidExtraInterstitialFeatures`, `_enableAndroidExtraRewardedVideoFeatures`, `_enableIosExtraBannerFeatures`, `_enableIosExtraInterstitialFeatures`, `_enableIosExtraRewardedVideoFeatures` weren't serialized. 
### Migration Guide
* If you are using Amazon, you need to re-download and re-import the plugin from here: https://mdtb-sdk-packages.s3.us-west-2.amazonaws.com/unity_aps.zip.

## [13.0.0] - 2024-10-22
### Added
* Applovin MAX SDK v8+ UPM migration (See `Migration Guide`)
### Removed
* `FacebookAdapterDetector` (See `Migration Guide`)
* SKADNetworkID post process logic when Amazon Adapter is installed (See `Migration Guide`)
### Migration Guide
* **Before updating** go to `Applovin > Integration Manager`, click `Unity Package Manager Migration > Upgrade All Adapters and Migrate to UPM` button.
* If you have installed Amazon adapter, go to `Applovin > Integration Manager`. Make sure that `Other Settings > Add Amazon Publisher Services SKAdNetworkID's` is enabled. Otherwise Amazon's SKADNetworkIDs won't be installed.
* It is recommended to remove `APPLOVIN_FACEBOOK` from scripting define symbols on all targets.

## [12.0.2] - 2024-10-22
### Fixed
* Compile error after updating Applovin MAX SDK to v8 (See `Migration Guide`)
### Migration Guide
* Don't migrate Applovin MAX SDK to UPM yet. This update aims to fix compile error only. For UPM migration, update to 13.0.0 first.

## [12.0.1] - 2024-10-03
### Fixed
* Adding `APPLOVIN_FACEBOOK` if `Assets/MaxSdk/Mediation/Facebook/Editor/Dependencies.xml` exists but `AppLovinMediationFacebookAdapter`does not exist inside.

## [12.0.0] - 2024-09-17
### Fixed
* `BannerAd`: Log tags on some messages
### Changed
* _(Internal)_ `BannerAd`: Using async/await instead of `MEC`
* _(Internal)_ `UnitBannerAd`: Using async/await instead of `MEC`
* Minimum Unity version is 2022.3 now
* Vegas dependency: v15.0.0

## [11.3.0] - 2024-09-16
### Added
* Applovin MAX SDK v7 compatibility
### Removed
* `Config.Is For Children` due to MAX SDK v7
* COPPA support: MAX SDK v7 removed the API
### Changed
* Handyman dependency: v5.8.1 -> v5.8.2
* Vegas dependency: v14.1.2 -> v14.1.5

## [11.2.0] - 2024-09-09
### Changed
* Setting user id as Adjust id logic: Adjust v5 implementation
* Handyman dependency: v5.5.0 -> v5.8.1

## [11.1.2] - 2024-07-11
### Changed
* Data usage consent related code compile error without certain packages 
* Facebook `SetAdvertiserTrackingEnabled` call compile error without certain packages

## [11.1.1] - 2024-05-09
### Changed
* Replaced `DateTime.UtcNow` calls with `DateTimeUtility.UtcNow`

## [11.1.0] - 2024-05-06
### Changed
* Config: `SetAdjustIdAsUserId` is no longer remote.
* Vegas dependency: v14.0.0.preview.0 -> v14.0.0
* Handyman dependency: v5.3.1 -> v5.5.0

## [11.0.0] - 2024-05-02
### Changed
* Vegas dependency: v13.1.0 -> v14.0.0-preview.0
* Update implementation to reflect the changes to Vegas API

## [11.0.0-preview.3] - 2024-03-25
### Added
* Banner size adjustment for adaptive banners (google admob)

## [11.0.0-preview.2] - 2024-03-21
### Fixed
* Consent isn't updated after the user closes the `Advanced Popup (a.k.a. Second Layer)`

## [11.0.0-preview.1] - 2024-03-21
### Added
* Facebook Adapter detection (see `Migration Guide`)
### Removed
* `Config.FacebookAdapterAdded` (see `Migration Guide`)
### Changed
* Consent logic improvements (see `Migration Guide`)
* Setting User ID as Adjust AdID logic rework
### Migration Guide
* If you are using Usercentrics as your CMP, make sure that `Applovin (templateID: fHczTMzX8)` and `Facebook Audience Network (templateID: ax0Nljnj2szF_r)` are added to data processing services list in Usercentrics dashboard. OTHERWISE, APPLOVIN WILL NOT WORK PROPERLY!
* If Facebook adapter is installed, `APPLOVIN_FACEBOOK` will be added to Scripting Define Symbols in iOS target automatically.

## [11.0.0-preview.0] - 2024-03-20
### Changed
* Vegas dependency: v13.1.0 -> v14.0.0-preview.0
* Update implementation to reflect the changes to Vegas API

## [10.3.2] - 2024-03-05
### Fixed
* Compile error in `ApplovinMediationServiceConfig.cs` when Meteor Module is not present.  

## [10.3.1] - 2024-02-18
### Added
* `enableAndroidExtraBannerFeatures` remote setting for toggling fast banner and banner revenue optimization features on Android
* `enableAndroidExtraInterstitialFeatures` remote setting for toggling interstitial revenue optimization features on Android
* `enableAndroidExtraRewardedVideoFeatures` remote setting for toggling rewarded video revenue optimization features on Android
* `enableIosExtraBannerFeatures` remote setting for toggling fast banner and banner revenue optimization features on iOS
* `enableIosExtraInterstitialFeatures` remote setting for toggling interstitial revenue optimization features on iOS
* `enableIosExtraRewardedVideoFeatures` remote setting for toggling rewarded video revenue optimization features on iOS

## [10.3.0] - 2024-02-14
### Added
* `FailureReason` implementation
### Changed
* Handyman dependency: v5.0.0 -> v5.3.1
* Vegas dependency: v12.0.2 -> v13.1.0
* Logger dependency: v2.0.0 -> v2.1.0

## [10.2.3] - 2024-02-06
### Fixed
* BUG: Banner state can be overridden by the SDK when `AutoShowAfterBanner` is `false` and `FastBanner` is shown, but    
  hidden by program code before normal banner unit is loaded. In this case, the SDK would show the loaded banner,    
  overriding the hidden state enforced by the app. This should now be fixed.

## [10.2.2] - 2024-01-11
### Fixed
* `UnitBannerAd` id definition.

## [10.2.1] - 2023-12-15
### Fixed
* Compile error while building on iOS
### Changed
* Vegas dependency: v12.0.1 -> v12.0.2

## [10.2.0] - 2023-12-13
### Added
* `Android Banner Placement` configuration with `androidBannerPlacement` remote key and default value of `BottomCenter`
* `Android Banner Vertical Offset` configuration with `androidBannerVerticalOffset` remote key and default value of `0`
* `iOS Banner Placement` configuration with `iosBannerPlacement` remote key and default value of `BottomCenter`
* `iOS Banner Vertical Offset` configuration with `iosBannerVerticalOffset` remote key and default value of `0`
* Added separate force ad switch intervals for interstitial and rewarded video
* Interstitial default force ad switch interval is set to 60 seconds
* Rewarded video force ad switch interval is set to 120 seconds
### Changed
* Documentation to reflect added configuration
* Rewarded/Interstitial force switch property conditional compilation code to prevent potential compile issues
* Updated multiple interstitial/rewarded video force ad switch logic.
* From now on, if ad force switch interval is set to 0, force switch feature is turned off

## [10.2.0-prev.1] - 2023-12-13
### Added
* `Android Banner Placement` configuration with `androidBannerPlacement` remote key and default value of `BottomCenter`
* `Android Banner Vertical Offset` configuration with `androidBannerVerticalOffset` remote key and default value of `0`
* `iOS Banner Placement` configuration with `iosBannerPlacement` remote key and default value of `BottomCenter`
* `iOS Banner Vertical Offset` configuration with `iosBannerVerticalOffset` remote key and default value of `0`
### Changed
* Documentation to reflect added configuration
* Rewarded/Interstitial force switch property conditional compilation code to prevent potential compile issues

## [10.2.0-prev.0] - 2023-12-12
### Added
* Added separate force ad switch intervals for interstitial and rewarded video
* Interstitial default force ad switch interval is set to 60 seconds
* Rewarded video force ad switch interval is set to 120 seconds
### Changed
* Updated multiple interstitial/rewarded video force ad switch logic.
* From now on, if ad force switch interval is set to 0, force switch feature is turned off

## [10.1.0] - 2023-12-01
### Added
* Amazon: `APPLOVIN_AMAZON` will be added to Scripting Define Symbols automatically if Amazon SDK is added. 

## [10.0.0] - 2023-11-30
### Removed
* Adjust SDK asmdef dependency
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
* Vegas dependency: v11.0.3 -> v12.0.1
* Newtonsoft Json dependency: v3.0.2 -> v3.2.1
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
* `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [9.0.0] - 2023-11-28
### Added
* Android/iOS fast interstitial ad unit id
* Android/iOS fast rewarded video ad unit id
### Removed
* Android/iOS use fast banner boolean (empty/null fast banner id means no fast banner)
* Android/iOS fast banner timeout option (timeout = 0 means no timeout)
* Android/iOS banner revenue optimization option (when fast banner is available, rev. opt. is automatically enabled)
* Android/iOS interstitial revenue optimization option (when fast interstitial is available, rev. opt. is automatically enabled)
* Android/iOS interstitial ad unit ids list
* Android/iOS first interstitial timeout option (timeout = 0 means no timeout)
* Android/iOS rewarded video revenue optimization option (when fast rewarded video is available, rev. opt. is automatically enabled)
* Android/iOS rewarded video ad unit ids list
* Android/iOS first rewarded video timeout option (timeout = 0 means no timeout)
### Changed
* `Use Fast Banner` property is updated to return true when fast banner id is set for current platfor
* `Use Banner Revenue Optimization` property is updated to return true when fast banner id is set for current platfor
* `Banner Ad Unit Ids` returns a list: fast banner id, default banner id. When fast banner is not set, only contains default banner id
* `Fast Banner Timeout Enabled` returns true if fast banner timeout is bigger than 0
* `Use Interstitial Revenue Optimization` returns true if fast interstitial field is set for current platform
* `First Interstitial Timeout Enabled` returns true if first interstitial timeout for current platform is bigger than 0

## [8.2.8-prev.1] - 2023-11-07
### Fixed
* Fast banner/Banner revenue optimization auto show bugfix

## [8.2.8-prev.0] - 2023-10-19
### Fixed
* Fast banner to Default/Optimized flow timeout unregister.

## [8.2.7] - 2023-10-16
### Changed
* Version bump.

## [8.2.6] - 2023-10-13
### Fixed
* Banner/Interstitial/Rewarded video amazon Load Internal callbacks are invoked from main thread now.
* Banner ads are only initialized after they are switched to. Multiple banner routine should no longer work when banner revenue optimization is enabled.
* Banner null ref fix when fast banner and multi banner is disabled
### Added
* Interstitial/Rewarded Video Timeout feature. (doc to be updated on actual release)
* Validation error mesages to banner unit id, fast banner unit id, banner unit id list under banner revenue optimization.
* Init attempt logs
* Fast Banner null id check
### Changed
* Banner ad unit pooling.
* Logs are tweaked

## [8.2.6-beta.4] - 2023-10-13
### Added
* Interstitial/Rewarded Video Timeout feature. (doc to be updated on actual release)

## [8.2.6-beta.3] - 2023-10-13
### Fixed
* Banner/Interstitial/Rewarded video amazon Load Internal callbacks are invoked from main thread now.

## [8.2.6-beta.2] - 2023-10-12
### Changed
* Banner ad unit pooling.
### Fixed
* Banner ads are only initialized after they are switched to. Multiple banner routine should no longer work when banner revenue optimization is enabled.

## [8.2.6-beta.1] - 2023-10-10
### Fixed
* Banner null ref fix when fast banner and multi banner is disabled
### Added
* Validation error mesages to banner unit id, fast banner unit id, banner unit id list under banner revenue optimization.

## [8.2.6-beta.0] - 2023-09-18
### Added
* Init attempt logs
### Changed
* Logs are tweaked

## [8.2.5] - 2023-09-15
### Fixed
* Duplicate init checks

## [8.2.4] - 2023-09-15
### Changed
* Added loading/loaded checks to business banner load method. Fixes bug related to TimeoutManager duplicate key.
* Added direct auto load after close to multi rewarded video/interstitial. Vegas waits for all units to be shown before triggering load

## [8.2.3] - 2023-09-13
### Changed
* Unit Banner/Interstitial/Rewarded Video Ad classes trim their assigned ids from now on. 

## [8.2.2] - 2023-09-08
### Fixed
* Bug: Android fast banner timeout remote config error at runtime.

## [8.2.1] - 2023-09-08
### Changed
* Updated workflow of Fast Banner, Banner/Interstitial/Rewarded Video Revenue Optimization
* Updated document to match new workflow
* Removed redundant `OnValueChanged` attributes from timeout fields

## [8.2.0] - 2023-09-07
### Added
* Added timeout to fast banner on android/iOS
* Remote setting: `androidFastBannerTimeoutEnabled`
* Remote setting: `androidFastBannerTimeout`
* Remote setting: `iosFastBannerTimeoutEnabled`
* Remote setting: `iosFastBannerTimeout`
### Fixed
* Multi banner not working when fast banner is disabled

## [8.1.7] - 2023-09-05
### Fixed
* Minor Fix; FACEBOOK SDK missing case handle.

## [8.1.6] - 2023-09-05
### Changed
* Full documentation.

## [8.1.5] - 2023-09-04
### Removed
* Test `#define ODIN_INSPECTOR` line was removed

## [8.1.4] - 2023-09-04
### Added
* Documentation for fast banner, banner revenue optimization, interstitial revenue optimization and rewarded video revenue optimization features.

## [8.1.3] - 2023-08-28
### Fixed
* Fixed a bug preventing selected banner to be forcefully switched even if the force switch interval has passed

## [8.1.2] - 2023-08-25
### Fixed
* Fixed a bug that caused selected banner to be null (disabled banner) when no existing banner is present

## [8.1.1] - 2023-08-21
### Added
* Remote setting: `androidFastBanner`
* Remote setting: `androidBannerRevenueOptimization`
* Remote setting: `androidInterstitialOptimization`
* Remote setting: `androidRewardedVideoOptimization`
* Remote setting: `iosFastBanner`
* Remote setting: `iosBannerRevenueOptimization`
* Remote setting: `iosInterstitialOptimization`
* Remote setting: `iosRewardedVideoOptimization`
### Removed
* Redundant `BannerRevenueOptimization` class

## [8.1.0] - 2023-08-21
### Added
* Business Interstitial logic implementation
* Business Rewarded logic implementation
### Changed
* Banner business implementation event propagation update

## [8.0.1] - 2023-08-16
### Fixed
* Multi banner initial show first shown date fix

## [8.0.0] - 2023-08-11
### Added
* Fast banner support
* Banner revenue optimization

## [7.1.0] - 2023-06-09
### Added
* Vegas v11 update: `NetworkPlacment` added to `AdDTO`

## [7.0.1] - 2023-06-06
### Fixed
* Vegas v10 dependency

## [7.0.0] - 2023-06-06
### Changed
* Vegas v10 update
### Added
* MRec ads

## [6.0.2] - 2023-05-30
### Changed
* `Banner.IsDestroyed` is true by default

## [6.0.1] - 2023-05-26
### Fixed
* Compile error when `APPLOVIN_AMAZON` is not present in scripting define symbols

## [6.0.0] - 2023-05-12
### Changed
* Rework with Vegas 9+
* Using cached Adjust ID

## [5.0.2] - 2023-03-17
### Fixed
* `Amazon`: Setting user consent status removed

## [5.0.1] - 2023-03-14
### Fixed
* `Amazon`: Using wrong id on `LoadInterstitial` 

## [5.0.0] - 2023-03-21
### Added
* New ad data events implementation
* `SetAdjustIdAsUserId` became remote setting
* `AdaptiveBanner` became remote setting
### Fixed
* `BannerData`: `StartLoadingTime` is set on `BannerLoaded` event
### Changed
* `FacebookAdapterAdded` is true by default

## [4.1.0-preview.4] - 2023-02-27
### Fixed
* PostProcessBuildIOS: Some SKAdNetworkIDs for APS are corrected

## [4.1.0-preview.3] - 2023-02-27
### Changed
* PostProcessBuildIOS: Added more SKAdNetworkIDs for APS

## [4.1.0-preview.2] - 2023-02-24
### Added
* PostProcessBuild added for Amazon's SKAdNetworkIDs on iOS
* APS Unity Plugin 1.5.x compatibility

## [4.1.0-preview.1] - 2023-02-17
### Added
* ExtraParameters support for all ad types.

## [4.0.2] - 2023-01-26
### Fixed
* Fixed crashing when `IsInterstitialLoaded`, `IsRewardedVideoLoaded` and `IsAppOpenLoaded` are checked before MaxSDK is initialized. 

## [4.0.1] - 2022-12-21
### Fixed
* `IsRewardedVideoShowing` wasn't set properly.

## [4.0.0-preview.8] - 2022-11-29
### Fixed
* `AmazonAdapter`: Rewarded Video Request wasn't called correctly.

## [4.0.0-preview.7] - 2022-11-29
### Changed
* `AmazonAdapter`: Interstitial request is `APSVideoAdRequest` instead of `APSInterstitialAdRequest`  

## [4.0.0-preview.6] - 2022-11-28
### Added
* Settings for Amazon adapter. They can also be changed remotely.

## [4.0.0-preview.5] - 2022-11-08
### Fixed
* `IsInterstitialLoaded` can cause an exception when `InterstitialID` is empty.
* `IsRewardedVideoLoaded` can cause an exception when `RewardedVideoID` is empty.
* `IsAppOpenLoaded` can cause an exception when `AppOpenID` is empty.

## [4.0.0-preview.4] - 2022-11-01
### Changed
* `SetAdjustIdAsUserId` is true by default
### Fixed
* Amazon init check removed on initialization

## [4.0.0-preview.3] - 2022-10-27
### Fixed
* XRevenuePaidEvent: logs are moved to main thread

## [4.0.0-preview.2] - 2022-10-26
### Fixed
* `LoadAppOpen`: `Format` was wrong

## [4.0.0-preview.1] - 2022-10-21
### Changed
* Will call mediation debugger on initialization if the device is marked as tester. This behaviour moved from Vegas to services.
### Added
* Amazon SDK implementation
* App Open Ads implementation

## [3.0.2] - 2022-09-06
### Changed
* `DUCM` initialization message is `warning` instead of `error` now

## [3.0.1] - 2022-09-05
### Added
* DUCM initialization enforcement
* If app id is invalid, service won't be initialized

## [3.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`
* **Migration guide**: After vegas-applovin update, close unity. Go to `PROJECTFOLDER/Assets/MatchinhamGames/Resources/Config`. Rename `MGApplovinMediationConfig.asset` and `MGApplovinMediationConfig.asset.meta` to `ApplovinMediationServiceConfig`.

## [2.4.0] - 2022-07-18
### Changed
* Injector merged with MGApplovinIntegration
### Fixed
* Meteor asmdef ref was missing

## [2.3.2] - 2022-07-05
### Changed
* FBAN ATE flag call overhaul

## [2.3.1] - 2022-06-17
### Changed
* Data Usage Consent Manager API change

## [2.3.0] - 2022-06-14
### Added
* Extra parameters for Banner
* Set Adjust ADID as MAX User ID option added

## [2.2.1] - 2022-06-01
### Fixed
* Compile fix when Data Usage Consent module is not installed 

## [2.2.0] - 2022-06-01
### Changed
* Data Usage Consent module dependency is now optional. Thus, you can manually set User Consent values.

## [2.1.0] - 2022-05-17
### Added
* Debugging mediation implemented

## [2.0.0] - 2022-04-27
### Added
* AdData, AdErrorData implementation
### Removed
* ECPM events

## [1.0.2] - 2022-04-26
### Fixed
* Banner, interstitial and rewarded video ad unit ids are seperated now.
 
## [1.0.1] - 2022-04-19
### Removed
* _initState: We can reach it from AsyncSingleton, so there is no need to cache it.

## [1.0.0] - 2022-04-12
* Initial release