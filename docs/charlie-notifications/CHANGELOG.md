# Changelog

## [6.0.0] - 2024-12-24
### Changed
* Notification API switched to builder model for creating notifications
* Samples are updated to the new API
* Documentation updated for the new API

### Fixed
* Inconsistent behaviour between iOS/Android platforms when DeliveryDelay is not set but RepeatInterval is set

## [5.4.1-prev.0] - 2024-12-05
### Added
* `Manual Notification Permission Request` config option to override default initialization flow.
  * Call `Charlie.Instance.Initialize()` when you want to initialize Charlie from game code.
### Changed
* Unity min version is now 2022.3.*

## [5.4.0] - 2024-08-13
### Added
* `CancelAllDisplayedNotifications` method: Cancels all notifications displaying on the notification center.
### Changed
* **(iOS Only)** Charlie requires users to be online (See `Migration Guide`)
### Fixed
* Missing `field:` on the attributes on `timeRangeStart` and `timeRangeStart`
### Migration Guide
* On iOS 17, when the user is offline, Charlie gets stuck after the notification authorization. Therefore, from now on Charlie requires internet connection on iOS devices. 

## [5.3.0] - 2024-06-12
### Added
* `IUserResponseHandler`: implementation
### Changed
* Handyman dependency v5.3.3 -> v5.7.0

## [5.2.0] - 2024-04-03
### Changed
* Handyman dependency: v5.3.1 -> v5.3.3
* Stash is initialized and deserialized at initialization time asynchronously.

## [5.1.1] - 2024-02-21
### Changed
* Mobile Notifications dependency: v2.3.1 -> v2.3.2: Fixes permission issues on Android

## [5.1.0] - 2024-02-15
### Added
* `FailureReason` implementation
### Changed
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
* Mobile Notifications dependency: v2.3.0 -> v2.3.1

## [5.0.2] - 2023-12-04
### Removed
* `iOSNotificationPlatform.Logger`: Will use `Charlie.Logger` instead
### Changed
* Log message for invalid delivery date is more detailed now

## [5.0.1] - 2023-11-29
### Fixed
* Compile error while targeting iOS

## [5.0.0] - 2023-11-20
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.1.5 -> v5.0.0
* Mobile Notifications dependency: v2.1.0 -> v2.3.0
* Newtonsoft Json dependency: v3.0.2 -> v3.2.1
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Removed
* `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [4.0.4] - 2023-09-01
### Fixed
* Compile errors when all or certain Sherlock packages are not imported

## [4.0.3] - 2023-08-17
### Fixed
* timeRangeStart and timeRangeEnd configurations were ineffective.
### Changed
* Default timeEnd value is set to be 22:00.

## [4.0.2] - 2023-02-08
### Fixed
* Compile error when `MG_DEBUG` is not present in scripting define symbols

## [4.0.1] - 2023-01-30
### Fixed
* `iOSNotificationPlatform`: Repeating notifications weren't working when `DeliveryDate` is used instead of `DeliveryInterval`
### Added
* `iOSNotificationPlatform`: Added error message when `DeliveryTime` is invalid

## [4.0.0] - 2023-12-04
### Fixed
* `iOSNotificationPlatform`: Repeating notifications
### Changed
* Logic rework
* GameNotification -> Notification
* Sample code revised
### Added
* Event sending by using Sherlock if `ExtraData` is an object of `NotificationEventData`
* `ValidateNotification` method

## [3.1.0] - 2022-10-12
### Changed
* Config menu will redirect to selecting `CharlieConfig` instead of custom window

## [3.0.2] - 2022-09-06
### Fixed
* Sample code revised
* `Meteor` initialization message is `warning` instead of `error`

## [3.0.1] - 2022-09-01
### Fixed
* Charlie wasn't enabled by default

## [3.0.0] - 2022-09-01
* Code refactor with `Handyman`: All public fields and methods are static now.

## [2.0.6-preview.1] - 2022-08-29
* iOSNotificationPlatform: RepeatCount is 1

## [2.0.5] - 2022-03-10
### Fixed
* Old meteor dependency removed

## [2.0.4] - 2022-03-09
### Changed
* Root namespace on asmdef files

## [2.0.3] - 2022-03-09
### Changed
* Package name

## [2.0.2] - 2022-03-09
* Initial release