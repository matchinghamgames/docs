# Changelog

## [1.1.2] - 2024-03-25
### Fixed
* Compile error when using `LOG4NET` 

## [1.1.1] - 2024-03-25
### Fixed
* `ApplyConsents` was invoked when the user wasn't required to give consent in the user's location
* JSON serialization is now handled by `Newtonsoft.Json` package instead of `JsonUtility`
### Changed
* Data Usage Consent Manager dependency -> 11.0.0 -> 11.1.0

## [1.1.0] - 2024-03-19
### Added
* `DUCM` v11 support: `TCString`, `ACString`
* `LogUSPData`
### Changed
* JSON serialization is now handled by `Newtonsoft.Json` package instead of `JsonUtility`
* Usercentrics SDK dependency -> 2.13.0 -> 2.13.2

## [1.0.0] - 2024-03-11
### Added
* Initial release