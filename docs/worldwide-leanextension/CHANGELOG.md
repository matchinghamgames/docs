# Changelog

## [2.2.0] - 08.06.2023
### Changed
* Dependency Update for WorldWide.
### Fix
* Additional check is added if cache is deleted.

## [2.1.1] - 28.03.2023
### Changed
* Version bump for worldwide.

## [2.1.0] - 28.03.2023
### Changed
* Fix: Lean LateUpdate case.

## [2.0.0] - 20.03.2023
### Changed
* Feat: Service Module logic for WorldWide Lean extension.

## [1.0.2] - 26-02-2023
### Changed
* Reference update, yml update.

## [1.0.0] - 25-02-2023
### Changed
* Initialization.
