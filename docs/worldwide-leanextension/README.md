## WorldWide Lean Extension

`WorldWide` Lean extension for Localization Manager.

## Installing

* Import this package using Unity's Package Manager UI.

## How to use
* Make sure to call `WorldWide.Instance.Initialize()` at the appropriate place.

## API & Details
