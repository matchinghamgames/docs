# Changelog

## [6.0.1] - 2025-02-13
### Removed
* In PlayPass projects, InitialInventory feature has been removed due to circular dependency.

## [6.0.0] - 2024-12-18
### Fixed
* Compile error when `Meteor` module is not present.
### Changed
* Minimum Unity version: 2022.3
* Handyman dependency: v5.8.2
* Stash dependency: v1.5.0
* Logger dependency: v2.1.0

## [5.1.0] - 2024-10-16
### Added
* Asset Import Postprocess script to check and fix asset database desync

## [5.0.0] - 2024-06-13
### Added
* Initial inventory editor
* Remote configuration for initial inventory with key `initialInventory` (Data serialized as JSON)
### Changed
* Updated documentation

## [4.0.0] - 2023-11-20
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
* Stash dependency: v1.3.1 -> v1.4.0
### Removed
* `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.

## [3.0.1] - 2023-07-17
### Fixed
* BUG: `Destroying assets is not permitted to avoid data loss` error
  when closing add item window

## [3.0.0] - 2023-07-04
### Fixed
* Rare exception while initializing
* `Item`: `Consumable` is hidden due to deprecation

## [3.0.0-preview.1] - 2023-02-09
### Changed
* `Item`: `Consumable` won't be used anymore, thus marked as obsolete

## [2.1.3] - 2023-02-01
### Fixed
* ItemAdded event called twice when the item is first added to the inventory bug fix.

## [2.1.2] - 2023-01-18
### Fixed
* ModuleDebugger can cause `NullReferenceException` due to trying to access data from non-initialized module.

## [2.1.1] - 2023-01-09
### Added
* New Debugger implementation

## [2.1.0] - 2022-10-13
### Added
* `BackpackConfig` selection menu

## [2.0.1] - 2022-09-26
### Fixed
* `AutoSave` wasn't working
### Changed
* *Internal* code refactor

## [2.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`: All public fields and methods are static now.

## [1.8.1] - 2022-06-30
### Fixed
* Double save when AutoSave is enabled

## [1.8.0] - 2022-06-30
### Added
* AutoSave added: After each AddItem and RemoveItem, Backpack can be saved instead of OnApplicationQuit

## [1.7.6] - 2022-03-09
### Updated
* Namespaces on asmdef files

## [1.7.5] - 2022-03-08
* Initial release