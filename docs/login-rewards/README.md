# Login Rewards

Easily implement daily login bonuses

## How To Use

* Import package via package manager
* Open config from `Matchingham > Login Rewards Config`.
* Make sure enabled is checked
* Create a pool in the config inspector.
  * If you want the rewards of a pool to match the days of a month, select `Monthly` from the `Date Binding` dropdown.
    * If you check the `Auto Reset` checkbox, when month ends, pool will reset back to day 1.
  * If you want, you can create custom pools with any amount of rewards you want. Just select `AppStart` from the `Date Binding` dropdown.
    * If you check the `Auto Reset` checkbox, when collection entry count number of days pass from the first launch date, the day will be reset to the first day.
  
* Call `LoginRewardsManager.Intance.GetLoginRewards(string)` to get `LoginRewards` data and provide the reward pool id as the parameter. This data contains info about:
    * Which reward is the current one
    * Is the current reward collected
    * What are the rewards day by day
* Use `LoginRewardsManager.Instance.Collect(LoginRewards)` to collect current day's reward. 
    * Note that if you are not using Backpack Module, you need to manually give the rewards to the player. This will only update date and day index information
      but will not give the player anything.

## API & Details

### Login Rewards Manager


* **GetDailyRewards()**: Returns a `LoginRewards` object containing information about the reward calendar.

```c#
public class LoginRewards
{
      public string poolId;                     // Id of the pool this reward belongs to
      public int dayIndex;                      // Which day's reward is it?
      public RewardState dayState;              // Reward's availability and claim state
      public List<RewardEntry> dayRewards;      // What are the rewards for each day?
      public DateTime startDate;                // When was this pool started?
      public int numberOfDays;                  // How many days are there in the pool?
}
```

* **Collect(LoginReward)**: Call when user claims the reward.



* **WhenInitialized(Action)**: Allows you to register a callback that will be fired only
  after the module is successfully initialized. Use this to execute logic that requires
  this module to be initialized first. If the module has already initialized, immediately
  invokes the callback



* **WhenFailedToInitialize(Action)**: Allows you to register a callback that will be fired only after
  the module fails to initialize for any reason. Use this to handle what should happen
  in case this module fails to initialize. If the module has already failed to initialize, immediately
  invokes the callback.



* **WhenReady(Action)**: Combined version of `WhenInitialized` and `WhenFailedToInitialize`.
  Delays execution of callback till module is first initialized or failed to initialize, immediately invoke
  the callback if it is already initialized or failed to initialize.
