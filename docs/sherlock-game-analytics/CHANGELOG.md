# Changelog

## [6.1.0] - 2024-02-16
### Added
* `FailureReason` implementation
### Changed
* Better timeout logic
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
* Sherlock dependency: v10.0.0 -> v11.1.0

## [6.0.1] - 2023-11-28
### Fixed
* Sherlock dependency

## [6.0.0] - 2023-11-17
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.1.5 -> v5.0.0
* Sherlock dependency: v9.1.2 -> v10.0.0
* GameAnalytics MGSDK Bridge dependency: v3.0.0 -> v4.0.0
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [5.0.3] - 2023-09-28
### Changed
* Sherlock dependency version to 9.1.2 for forcing update to AdType enum in Sherlock package.

## [5.0.1] - 2023-01-31
### Fixeed
* `NullGameAnalyticsService`: compile fix

## [5.0.0] - 2022-11-23
### Changed
* `SendCustom`: `Dictionary` => `IDictionary`

## [4.0.0] - 2022-11-16
### Changed
* `SendImpressionEvent` changed

## [3.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`

## [2.2.3] - 2022-07-21
### Fixed
* CheckReady fix

## [2.2.2] - 2022-07-07
### Fixed
* Namespace of NullGameAnalyticsService

## [2.2.1] - 2022-07-06
### Changed
* Sherlock dependency bump

## [2.2.0] - 2022-06-02
### Changed
* GA Bridge package dependency
* asmdef references are by name now
* Sherlock dependency bump
* Double initialization fixed
* Internal Code refactor
* No need to add `GA_SpecialEvents` script since it's marked as `RequiredComponent` on `GameAnalytics` script
* SendImpressionEvent workaround on admost
* Initialization control on method calling overhaul
* MGGameAnalyticsService is inherited by AsyncSingleton instead of AsyncSingletonBehaviour

## [2.1.0] - 2022-04-27
### Added
* SendImpressionEvent implementation
### Changed
* asmdef file name

## [2.0.2] - 2022-03-09
### Changed
* Package name

## [2.0.1] - 2022-03-09
### Fixed
* Namespace on asmdef files

## [2.0.0] - 2022-03-09
### Added
* Initial release
