# MG Play Pass Module

## Overview

The `PlayPass Module` package is specifically designed for games that are accepted into the Google Play Pass program.

### Initialization Flow
![Initialization Flow](/playpass/initFlow.jpg)

## Installing
* Import this package using Unity's Package Manager UI.

## How to use
* Ensure that you call `PlayPass.Instance.Initialize()` at the appropriate point in your code.
  * This package depends on the Dealer (IAP) SDK, so make sure to initialize Dealer before calling the PlayPass initialization.
  * Since the Data Usage Consent SDK asks for user consent, **it is crucial to initialize the PlayPass package before initializing the Data Usage Consent SDK.**
* Create an `IAPProduct` that signifies the presence of a PlayPass license when it exists. 
  * Add "NoAds" and possibly other sub-items to manage monetization and user consent-related content.
  * Alternatively, make the appropriate changes in your application solely based on the existence of this `IAPProduct`.
* Subscribe to events;
  * To InitializationProgressChanged event to monitor the progress of the initialization.
  * To OnLicenseActivation event to manage accompanied feature on/off cases.

## Configuration

Go to Matchingham > Play Pass > Config to configure the module.

| Variable                | Description                                                                 |
| ----------------------- | --------------------------------------------------------------------------- |
| Enabled                 | Enabling/disabling the module                                               |
| Play Pass Product       | An IAPProduct which the module understands license is active when purchased |
| Request Timeout         | Total time to wait until the process is cancelled                           |
| Info Timeout Milestone  | see InitializationProgressChanged event                                     |
| Final Timeout Milestone | see InitializationProgressChanged event                                     |

## API & Details

### PlayPass Module

#### Fields

* **IsLicenseActive**: Returns a value indicating whether the product appears to be purchased.

* **IsInconclusive**: Returns 'true' when the license appears to be active, but the application cannot confirm it during initialization.

#### Methods

* **OnApplicationFocus(bool)**: Call this method from OnApplicationFocus to make sure 'Restore' is triggered.

#### Events

* **InitializationProgressChanged**: 
  : This event is triggered in the following scenarios:
  * **Verifying**: Occurs when **IsInconclusive** is true, and half the duration of the 'Info Timeout' has elapsed.
  * **Takes_TooLong**: Occurs when **IsInconclusive** is true and the full duration of the 'Info Timeout' has elapsed.
  * **Inconclusive**: Occurs when **IsInconclusive** is true and the 'Final Timeout' period has elapsed.
  * **Finalized**: Occurs when the **State** is either 'Initialized' or 'Failed to Initialize', i.e. the process has concluded.

* **OnLicenseActivation**: This event is triggered right after the cached license state has been updated.

### Common

#### Methods

* **Initialize()**
  : Starts module initialization. You need to call this at the appropriate place.

* **WhenInitialized(Action callback)**
  : Allows you to register `callback` that will be fired only after the module is successfully initialized. Use this to execute logic that requires this module to be initialized first. If the module has already initialized, immediately invokes the callback.

* **WhenFailedToInitialize(Action callback)**
  : Allows you to register `callback` that will be fired only after the module fails to initialize for any reason. Use this to handle what should happen in case this module fails to initialize. If the module has already failed to initialize, immediately invokes the callback.

* **WhenReady(Action callback)**
  : Combined version of `WhenInitialized` and `WhenFailedToInitialize`. Delays execution of `callback` till module is first initialized or failed to initialize, immediately invoke the callback if it is already initialized or failed to initialize.

#### Fields

* **State**
  : Initialization state of the module

* **Instance**
  : Instance of the module

* **LateInitialized**
  : When the module needs an internet connection but the player became online while playing the game, this becomes `true`

* **Ready**
  : If the module is initialized successfully and ready to operate

* **Config**
  : Configuration of the module. See [configuration](#configuration)

* **InitializationDuration**
  : Initialization duration in seconds
