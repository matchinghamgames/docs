# Changelog

## [1.2.0] - 2024-12-09
### Fixed
* Compile error when `LOG4NET` is added to scripting define symbols
* Inconsistency with its logger's name in `Logger Config`
### Changed
* _(iOS Only)_ `FailureReason` is `ThirdParty` now
* Dealer dependency: v7.0.0
* Package name: `PlayPass Module`
### Added
* Code stripping protection
* Better failure explanations on invalid config and incompatible platforms
* Handyman dependency: v5.8.2
* Logger dependency: v2.1.0

## [1.1.0] - 2024-10-09
### Updated
* Dealer 6.0.0 dependency.

## [1.0.1] - 2024-08-22
### Changed
* Removed Handyman dependency and added Dealer Dependency.
### Fixed
* Compile error when DataUsageConsent is not present fix.

## [1.0.0] - 2024-08-09
### Added
* Initial release