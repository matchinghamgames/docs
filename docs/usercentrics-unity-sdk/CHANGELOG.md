# Changelog

## [2.18.6] - 2025-02-05
### Fixed
* [Fix] - An error occurring when trying to migrate from version 7 to 8.
* [Fix] - Issue causing SDK initialization to fail when using a specific version with Google's ATPs disabled.
### Changed
* [Improvement] iOS - Enhanced accessibility by increasing the contrast of the focus indicator.
* [Improvement] Flutter - Exposing the stack trace when SDK initialization fails.

## [2.18.5] - 2025-01-23
### Fixed
* [Fixes] - Issue causing a temporary unstable SDK state when calling the clearUserSession() API
  * Android Bug Fixes:
    * [Fix] Memory leak that occurred when displaying the banner's First Layer
  * iOS Bug Fixes:
    * [Fix] Accessibility by addressing instances where multiple links had identical link text

## [2.18.4] - 2025-01-07
### Fixed
* Android Bug Fixes:
  * [Fix] Inconsistencies when showing the banner using the windowFullscreen parameter
* Other Fixes:
  * [Fix] First Layer links were not underlined
  * [Fix] Google Consent Mode edge case when setting analytics_storage flag
  * [Fix] Clear User Session edge case when using caching certain requests

## [2.18.3] - 2024-12-11
### Added
* [Feature] Adjust Consent Mediation - Removed support for Adjust version 4
* [Improvement] Flutter - Exposed legalBasis field in UsercentricsCMPData object
### Fixed
* Android Bug Fixes:
  * [Fix] Full-screen mode was not properly applied on certain devices, causing the banner to be displayed within the safe area
  * [Fix] Banner content was hidden by the selfie camera when full-screen mode was activated

## [2.18.2] - 2024-11-27
### Added
* [Feature] Manual Resurface - Added support for manual resurface in the mobile SDK, which can be trigged through Admin Interface
* [Improvement] readLess Label - Exposed readLess label in UsercentricsLabels object

## [2.18.1] - 2024-11-20
### Changed
* [Improvement] Caching solutions - Improved our cache solution to provide faster initializations and better UX

## [2.18.0] - 2024-11-14
### Fixed
* [Fix] Crash when attempting to restore user session with controller id that only has TCF component.
  * Android Bug Fix:
    * [Fix] Fixed issues related to dismissing banners in inactive activities.

## [2.17.3] - 2024-10-17
### Changed
* [Feature] Right-to-left Language - Added support for right-to-left (RTL) languages in our SDK for enhanced localization and global accessibility.
### Fixed
* [Fix] General fixes and performance improvements

## [2.17.2] - 2024-10-01
### Changed
* [Improvement] Google Consent Mode - Update to improve compatibility and ensure smoother performance with Google Consent Mode

## [2.17.1] - 2024-09-25
### Changed
* [Improvement] Google Consent Mode - Update to improve compatibility and ensure smoother performance with Google Consent Mode

## [2.17.0] - 2024-09-26
### Added
* [Feature] TCF V5 - Updated to new TCF Policy version
  * Updated TCF banner second's layer to reflect the latest addition of Special Purpose 3
  * Updated TCF banner second's layer to include information related to the CMP’s maximum device storage duration
* [Feature] Adjust Consent Mediation
  * Adding support for new Adjust version 5
  * Adjust version 4 will be supported until October 31, 2024
### Fixed
* iOS Bug Fixes:
  * [Fix] ironSource Consent Mediation for CCPA not being applied
  * [Fix] HTML text not being rendered correctly

## [2.16.0] - 2024-09-04
### Added
* [Feature] New timeout mode - In this version, you are able to customize the global timeout for the maximum amount of time that the SDK should take to fully initialize, just set the new parameter on the UsercentricsOptions

## [2.15.6] - 2024-08-15
### Fixed
* [Fix] Toggle visibility of Deny Button on Second Layer for TVs
* [Fix] Stability improvements when initializing the SDK

## [2.15.5] - 2024-07-31
### Changed
* [Improvement] Google Consent Mode: Enhanced implementation when integrating with the TCF framework
* [Improvement] Deprecating copy field from UsercentricsLabels
### Fixed
* React Native Fixes:
  * [Fix] Crash when invoking showSecondLayer(...) method without arguments
  * [Fix] Adding missing labels in UsercentrisLabels object

## [2.15.4] - 2024-07-23
### Fixed
* [Fix] Rare crash when initializing the SDK with TCF configuration
* [Fix] Unity Ads SDK Mediation adjusts

## [2.15.2] - 2024-06-12
### Added
* [Improvement] Firebase Consent Mediation: Improved integration with its flags
### Fixed
* Android Bug Fix:
  * [Fix] Fixed issues related to displaying banners in inactive activities.
* Other Fixes:
  * [Fix] Addressed sporadic crashes during TCF banner initializations on iOS
  * [Fix] Enhanced code efficiency for smoother consent banner displays.

## [2.15.1] - 2024-05-29
### Added
* [Improvement] Consent Mediation - Specific implementation for Firebase Analytics and Advertising SDK
* [Improvement] Reset method deprecated for the SDK, React Native, Flutter and Unity
* [Improvement] Added support for new TCF languages
* [Improvement] Changed TCF resurface period: from 13 months to 390 days
### Fixed
* [iOS Bug Fix] UI improvement
* [General Fixes] Some DPSs had the cookie storage wrongly displayed

## [2.15.0] - 2024-04-25
### Added
* [Improvement] Upgrade to latest Kotlin version 1.9.23
* [Improvement] Officially changed Android SDK minimum version to API 21
* [Improvement] Releasing iOS SDKs that supports manual linkage
* [Improvement] General improvements on Google Consent Mode
* [Improvement] Apple Privacy Manifest - Added new requirements to support new compliance rules, check them out here
### Fixed
* iOS Bug Fixes:
  * [Fix] Crashes on Xcode 15.2 when using new Apple's libraries linkage mechanism
  * [Fix] IronSource Consent Mediation integration tweaks to support their latest release
* Other Fixes:
  * [Fix] Links not supported on banner message customizations were being displayed
  * [Fix] Controller ID card were not visible in certain conditions when Google Additional Providers were not present
  * [Fix] GDPR Banner was reappearing after a very specific scenario that changed the rule if it should be resurfaced or not

## [2.14.2] - 2024-04-10
### Added
* [Improvement] Supports Read More on Banner Message - Adds support to have a "read more" button on banner message displayed on the First Layer
### Fixed
* Android Bug Fixes:
  * [Fix] Sporadically crashes on TCF banner initializations
* iOS Bug Fixes:
  * [Fix] Sporadically crashes on getUserSessionData API
* Other Fixes:
  * [Fix] Stability improvements

## [2.14.0] - 2024-04-04
### Added
* [Feature] Show banner in fullscreen for Android - For scenarios where your app is set to show in fullscreen, we are now enabling the banner to also respect that settings, check out our documentation for this API
* [Feature] Resurface Banner for Additional Tech Providers - When changing Additional Tech Providers from Google, we are now enabling an option on Admin Interface to choose whether this should cause the banner to resurface or not
* [Improvement] Added SDK version to the README file
* [Improvement] Updated External Dependency Manager plugin to version 1.2.179
* [Improvement] Reset API will be deprecated in future versions
### Fixed
* iOS Bug Fixes:
  * [Fix] UI improvements on spacing between labels


## [2.13.2] - 2024-03-13
### Added
* [Feature] Clear User Session - Introducing a new API designed to simplify the process of clearing user sessions. Explore it further here
* [Improvement] Google Consent Mode Granular Choices - Enhances integration with Google SDKs by updating to the latest changes. Explore the details here
* [Improvement] Adjust Granular Consent - By Using Consent Mediation, we have fully integrated with Adjust SDK updates associated with the DMA
### Fixed
* iOS Bug Fixes:
  * [Fix] Adjusts in landscape mode where labels were not fully aligned with other elements of the screen
  * [tvOS Fix] Numerous layout modifications have been made to address the arrangement of titles and the rendering of other elements in languages that result in larger text sizes
* Other Fixes:
  * [Fix] Removes deprecated field TCFVendor::deviceStorage
  * [Fix] In certain scenarios, the 'Save Settings' button color was not customizable

## [2.13.0] - 29-02-2024
### Added
* [Feature] New Usercentrics Ready API - The API's new properties introduce a feature that, through Rulesets, allows identification of scenarios where the banner may be bypassed in specific locations, check it out
* [Improvement] Removes TCF 2.0 warnings - Since TCF 2.0 has been deprecated, we've removed all warnings regarding the usage of version 2.0 of this framework
* [Improvement] New TCF 2.2 Stacks - Added support for Stacks 44 and 45
### Fixed
* Android Bug Fixes:
  * [Fix] Fully removes support for TLS 1.2 in Android versions lower than 5.0 (API Level 20)
  * [Minor Fix] Prefab was not holding the correct value for DisableSystemBackButton
* iOS Bug Fixes:
  * [Fix] Banner was not correctly rendered given some specific programmatic customization scenarios
  * [Fix] Fixes layout constraints when changing the language for CCPA banners
* Other Fixes:
  * [Fix] General improvement on resurfacing logic for any Legal Basis change on TCF Purposes

## [2.12.1-rc.0] - 2024-02-21
### Fixed
* GUID of the files
### Migration Guide
* v2.12.0 has wrong GUID for the files, please update to v2.12.1-rc.0. To keep the versioning same with Usercentrics, we are releasing the new version with the correct GUIDs as a release candidate.

## [2.12.0] - 2024-02-15
### Added
* [Feature] Consent choice persistence - Prevents repeated banner displays across geographies for users moving across different legal frameworks (i.e. in case of frequent travellers)
* [Feature] - Added Assembly Definitions
* [Improvement] - Displaying the count of third-party vendors by category title
* [Improvement] First Layer customization demo - Showcasing our First Layer personalization by using all the power from our Customization API 💪
### Fixed
* Android Bug Fixes:
  * [Fix] Improved UX on language selector popup
* iOS Bug Fixes:
  * [Fix] Enhancing the UI of First-layer Popups and Sheets for Optimal Display on iPads
* Other Fixes:
  * [Fix] Aligned purposes descriptions/illustrations and vendor data with GVL translation
  * [Fix] Edge case when certain services, hidden by their respective categories, were visible"
  * [Fix] Minor issues on session restoration when using TCF
### Migration Guide
* Don't use this package if you are using `DataUsageConstentManager` v9.3.x or older