# Changelog
 
## [1.2.0] - 2024-11-11
### Added
* Timeout logic when facebook hasn't been initialized yet.
### Changed
* Handyman dependency: v5.8.2
* Facebook Bridge dependency: v3.2.0
### Fixed
* Compile error when `log4net` is enabled

## [1.1.0] - 2024-04-19
### Changed
* Unity dependency: v2020.3 -> v2022.3
* Logger dependency: v1.3.0 -> v2.1.0
* Handyman dependency: v4.1.4 -> v5.3.3
* Sherlock dependency: v9.2.0 -> v11.2.0
* Facebook Bridge dependency: v1.0.0 -> v3.1.1

### Fixed
* Logger migration related compile error.

## [1.0.1] - 2023-10-05
### Added
* SendCustom event log capability

## [1.0.0] - 2023-10-05
### Changed
* Initial release
