# Changelog

## [5.0.0] - 2024-12-04
### Changed
* Consent logic rework (see `Migration Guide`)
* Handyman dependency: v5.8.2
* Vegas dependency: v16.1.0
* Min Unity version: 2022.3
* Package name: `Vegas Service - Unity LevelPlay`
### Migration Guide
* If you are using Usercentrics as your CMP, make sure that `ironSource (templateID: 9dchbL797)` and `Facebook Audience Network (templateID: ax0Nljnj2szF_r)` are added to data processing services list in Usercentrics dashboard. OTHERWISE, IRONSOURCE WILL NOT WORK PROPERLY!

## [4.2.0] - 2024-09-09
### Changed
* Handyman dependency: v5.3.1 -> v5.8.1
* Setting user id as Adjust id logic (see `Migration Guide`)
### Migration Guide
* Before installing, don't forget to add "com.adjust" to scopes of the registry whose URL is "https://package.openupm.com" (if not exist, add). Alternatively, you can install it from here: https://openupm.com/packages/com.adjust.sdk/

## [4.1.1] - 2024-05-10
### Added
* `UnityEngine` using statement to `IronSourceMediationService.cs`
### Changed
* Updated implementation to match new vegas interfaces
* Replaced `DateTime.UtcNow` calls with `DateTimeUtility.UtcNow`

## [4.1.1-preview.0] - 2024-05-10
### Added
* `UnityEngine` using statement to `IronSourceMediationService.cs`
### Changed
* Updated implementation to match new vegas interfaces
* Replaced `DateTime.UtcNow` calls with `DateTimeUtility.UtcNow`

## [4.1.0] - 2024-02-14
### Added
* `FailureReason` implementation
### Changed
* Handyman dependency: v5.0.0 -> v5.3.1
* Vegas dependency: v12.0.0 -> v13.1.0
* Logger dependency: v2.0.0 -> v2.1.0

## [4.0.0] - 2023-11-16
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
* Vegas dependency: v10.0.0 -> v12.0.0
* Will be compiled only on Editor, Android, iOS and tvOS (see `Migration Guide`)
### Removed
* Adjust SDK asmdef dependency
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android, iOS or tvOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_TVOS || UNITY_ANDROID`

## [3.0.0] - 2023-06-06
### Changed
* Vegas v10 update

## [2.0.0] - 2023-05-12
### Changed
* Rework with Vegas v9

## [1.0.2] - 2022-12-21
### Changed
* `IsRewardedVideoShowing` wasn't set properly.
* Possible duplicate entry prevention while parsing SKAdNetworkIDs.

## [1.0.1] - 2022-12-09
### Changed
* Code stripping disabled

## [1.0.0] - 2022-12-07
### Changed
* PostProcessor compile fix on Android

## [1.0.0-preview.4] - 2022-11-24
### Changed
* PostProcessor save fix

## [1.0.0-preview.3] - 2022-11-24
### Changed
* Parsing and postprocessing SKAdNetworkIds added
* Adaptive banner support added

## [1.0.0-preivew.2] - 2022-11-14
### Changed
* Initializer added
* editor asmdef naming fix

## [1.0.0-preivew.1] - 2022-11-14
### Changed
* Initial release
