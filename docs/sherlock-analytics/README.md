# Sherlock Analytics Module

## Overview

Sherlock module is a proxy module for initializing multiple analytics services at the same time. It also
provides an event importer that can import adjust analytics events from a CSV file, and generates
the same events for all other analytics services that support custom event mapping.

## Installing

* Import package from Package Manager UI.
* Import analytics services that you want to use from Package Manager UI.

## How to use

* Go to `Matchingham > Analytics > Config`. Make sure `Enabled` is `true`.
* Make sure to call `Sherlock.Instance.Initialize()` at the appropriate place.
* Go to `Matchingham > Analytics > Import Events` and open `Event Importer`.
    * `Event Importer` parses events from a file in CSV format.
        ```csv
        A,B,[optional]C,[optional]D,[optional]E,[optional]F
        ```
      A: Event name; this will be used for Firebase as an event name
      B: Adjust Token
      C: iOS Adjust Token; If you don't have different adjust token for iOS, leave empty
      D: Conversion Fine Value; The updated conversion value
      E: Conversion Coarse Value; The updated coarse conversion value; 0 = Low, 1 = Medium, 2 = High
      F: Conversion Lock Window; Whether to send the postback before the conversion window ends; 0 = false, 1 = true
      * Example: 
      ```csv 
      level2_start,9qktjs # If you have only one token and don't have any conversion 
      level2_start,9qktjs,d3fe51 # If you have different token on iOS but not any conversion value
      level2_start,9qktjs,,1,1,1 # If you have only one token on iOS but not any conversion value
      level2_start,9qktjs,d3fe51,1,1,1 # If you have everything
      ```
      
* Each supported service provides an interface. To send analytics to a certain service, request that service from Sherlock Analytics Module and use the returned instance.
    * If you request a supported service that is not imported, Sherlock will return a Null Object that will mock the API calls. This allows the code base to remain the same even if some analytics services are removed from the project.
      

Sample level analytics calls:
```csharp
Sherlock.Service<IAdjustAnalyticsService>().SendCustom(SherlockUtility.GetLevelEventName(level, "start"));
Sherlock.Service<IFirebaseAnalyticsService>().SendCustom(SherlockUtility.GetLevelEventName(level, "start"));
Sherlock.Service<IFirebaseAnalyticsService>().SendLevelStart($"level{level}");
```

Sample custom analytics event calls:
```csharp
Sherlock.Service<IAdjustAnalyticsService>().SendCustom(EventNames.GameStart);
Sherlock.Service<IFirebaseAnalyticsService>().SendCustom(EventNames.GameStart);
```
* The `EventName` class is auto generated. This happens when CSV file is imported from the event importer.

## How does it work

Sherlock event definition logic works centered around adjust events. Below is a sample adjust events CSV file
```csv
iap_cons,9qhpc0
iap_cons_existingpurchasepending,uacknj
iap_cons_paymentdeclined,dd8nu8
iap_cons_productunavailable,9u28pf
iap_cons_purchasingunavailable,4y3cd3
iap_cons_signatureinvalid,7skdqp
iap_subs,hzhgi
iap_noncons,qnm8wl
level1_complete,t494ux,2,1,0
level1_fail,347c9h
level1_start,q9hv5v
level2_complete,ws75fg,dsf34f,3,2,1 
level2_fail,s4xg0l,dsf34f
level2_start,9qktjs,d3fe51
game_start,yl9fo9,j3g34e
```

This file is imported to the project using the `Matchingham > Analytics > Import Events`. After the file is selected,
hit the `Import and Generate Event Names` button.

![Importer](/sherlock-analytics/analytics-import-events-editor.png)

Once this operation is complete, the file will be parsed and events will be generated. You can see and 
further configure these events from the settings inspector. To Open settings inspector, use the 
`Matchingham > Analytics > Config` menu.

![Config menu](/sherlock-analytics/analytics-config.png)

Adjust requires developers to send conversion events if the user doesn't allow ATT. Conversion event
and value configuration is configured here.
* If your `Adjust token` is separated by platforms. You can override this for iOS by enabling `Override Adjust Token for iOS` and set `Adjust Token iOS`
* To make an event a conversion event, check the `Is Conversion Event` checkbox and set the `Conversion Value` field

![Config Editor](/sherlock-analytics/analytics-config-editor.png)

## Configuration
Go to `Matchingham > Analytics > Config`

| Variable                                    | Description                                                                                                                                          |
|---------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| Enabled                                     | Enabling/disabling Court                                                                                                                             |
| AutoInitialize                              | If enabled, you don't need to call Initialize method manually.                                                                                       |
| Events                                      | Events data. See also [How does it work](#how-does-it-work)                                                                                          |
| Use Default Timeout For Services            | If true, all services will use default timeout duration                                                                                              |
| Custom Timeout For Services                 | If `Use Default Timeout For Services` is false, you can enter a custom timeout for services here                                                     |
| DefaultECPMEventTag                         | Default ECPM Event Tag. Change this if your even naming is different, otherwise use default: `ecpmGreaterThan`                                       |
| DefaultECPMEventFormatter                   | Default ECPM Event Formatter. Change this if your even naming is different, otherwise use default: ``                                                |
| DefaultCumulativeRevenueEventTag            | Default Cumulative Revenue Event Tag. Change this if your even naming is different, otherwise use default: `cumulativeRevenueGreaterThan`            |
| DefaultCumulativeRevenueEventFormatter      | Default Cumulative Revenue Event Formatter. Change this if your even naming is different, otherwise use default: ``                                  |
| DefaultCumulativeRevenueCountEventTag       | Default Cumulative Revenue Count Event Tag. Change this if your even naming is different, otherwise use default: `cumulativeRevenueCountGreaterThan` |
| DefaultCumulativeRevenueCountEventFormatter | Default Cumulative Revenue Count Event Formatter. Change this if your even naming is different, otherwise use default: ``                            |
| DefaultIAPEventTag                          | Default IAP Event Tag. Change this if your even naming is different, otherwise use default: `iap_`                                                   |
| DefaultLevelEventTag                        | Default Level Event Tag. Change this if your even naming is different, otherwise use default: `level`                                                |
| DefaultLevelEventFormatter                  | Default Level Event Formatter. Change this if your even naming is different, otherwise use default: ``                                               |
| DefaultAdImpressionEventTag                 | Default AdImpression Event Tag. Change this if your even naming is different, otherwise use default: `adimpsx`                                       |

## API & Details

### Sherlock

#### Methods
  
* **`Service<TAnalyticsService>()`**
  : Returns current implementation of the requested service type. Currently requested service types:
    
  * [IAdjustAnalyticsService](#adjust-analytics-service)
  * [IFirebaseAnalyticsService](#firebase-analytics-service)
  * [IAdmostAnalyticsService](#admost-analytics-service)
  * [IGameAnalyticsService](#game-analytics-analytics-service)
  * [IUnityAnalyticsService](#unity-analytics-service)

* **`RegisterService(IAnalyticsService service)`**
  : Used to register analytics services to Sherlock module. **This method is only used by analytics service adapters.**

### Common

#### Methods

* **`Initialize()`**
  : Starts module initialization. You need to call this at the appropriate place.

* **`WhenInitialized(Action callback)`**
  : Allows you to register `callback` that will be fired only after the module is successfully initialized. Use this to execute logic that requires this module to be initialized first. If the module has already initialized, immediately invokes the callback.

* **`WhenFailedToInitialize(Action callback)`**
  : Allows you to register `callback` that will be fired only after the module fails to initialize for any reason. Use this to handle what should happen in case this module fails to initialize. If the module has already failed to initialize, immediately invokes the callback.

* **`WhenReady(Action callback)`**
  : Combined version of `WhenInitialized` and `WhenFailedToInitialize`. Delays execution of `callback` till module is first initialized or failed to initialize, immediately invoke the callback if it is already initialized or failed to initialize.

* **`SetNoAdsActive()`**
  : When not using `Backpack` package, you need to manually set the no ads state. Track the status of the no ads purchase, and call this method when no ads should be turned off. This call must be made each runtime session.

#### Fields

* **`State`**
  : Initialization state of the module

* **`Instance`**
  : Instance of the module

* **`LateInitialized`**
  : When the module needs an internet connection but the player became online while playing the game, this becomes `true`

* **`Ready`**
  : If the module is initialized successfully and ready to operate

* **`Config`**
  : Configuration of the module. See [configuration](#configuration)

* **`InitializationDuration`**
  : Initialization duration in seconds
  
### Adjust Analytics Service

* **`SendCustom(eventName: string, parameters: IDictionary<string, object> (null))`**
  : Allows sending a custom, self-styled analytics event. Most of the events are sent with this method.

* **`SendAdRevenue(revenue: float, networkName: string, adUnitId: string, placement: string, sdkSource: string, currency: string)`**
  : Ad revenue tracking event
  
* **`SendECPMEvent(eventName: string, ecpm: float, currency: string)`**
  : ECPM tracking event.

* **`SendInAppPurchaseRevenueEvent(eventName: string, price: double, currency: string)`**
  : In-app Purchase revenue event.
  
* **`SendInAppPurchaseRevenueEvent(eventName: string, transactionId: string, purchaseToken: string, price: double, currency: string, purchaseDate: DateTime?)`**
  : Overload of `SendInAppPurchasEvent`. This accepts additional `transactionId`, `purchaseToken` and `purchaseDate` parameters.

* **`UpdateConversion(conversionValue: ConverisonValue)`**
  : Updates conversion value

* **`GetAttribution(): Task<IAttributionData>`**
  : Returns attribution data
 
* **`SetAttributionChangeCallback(callback: AttributionChangedDelegate)`**
  : To track changes on attribution data

### Admost Analytics Service

* **`SendCustom(eventName: string, parameters: IDictionary<string, object> (null))`**
  : Allows sending a custom, self-styled analytics event. Most of the events are sent with this method.

* **`SendIapEvent(tags: string[], identifier: string, receipt: string, isoCurrencyCode: string, localPrice: float)`**
  : Admost in-app purchase analytics event. Accepts custom tags to categorize in app purchases. These tags can indicate things like which interface the purchase was made from.
  
* **`TrackAdmobRevenue(adFormat: string, revenue: double, placementId: string, adUnitId: string)`**
  : If you are using Admob, you can use this to send impression-level revenue data to Admost analytics.
  
### Firebase Analytics Service

* **`SendCustom(eventName: string, parameters: IDictionary<string, object> (null))`**
  : Allows sending a custom, self styled analytics event. Most of the events are sent with this method.

* **`SetUserId(userId: string)`**
  : Set user id for the current user. This is useful for tracking user data across multiple devices.

* **`SetUserProperty(name: string, value: string)`**
  : Set a user property to a given value. This is useful for tracking user data across multiple devices.

* **`SendAppOpen(extraParameters: IDictionary<string, object> (null))`**
  : When game initialization is complete, this event should be sent

* **`SendAdImpression(revenue: double, networkName: string, adUnitId: string, placement: string, sdkSource: string, currency: string, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides a specialized ad impression event.

* **`SendCustomInAppPurchase(token: string, itemId: string, currencyCode: string, price: float, extraParameters: IDictionary<string, object> (null))`**
  : Firebase normally collects in app purchase events itself. However, if you want, you can use these to send them as a custom event.
  
* **`SendEarnVirtualCurrency(currency: string, amount: long, extraParameters: IDictionary<string, object> (null))`**
  : This event is used to log whenever the user earns virtual currency in game.
  
* **`SendEarnVirtualCurrency(currency: string, amount: double, extraParameters: IDictionary<string, object> (null))`**
  : Overload of the same analytics event. Uses a double parameter instead of long.
  
* **`SendLevelStart(levelName: string, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides a specialized level start analytics event. This method sends that event.
  
* **`SendLevelEnd(levelName: string, success: bool), extraParameters: IDictionary<string, object> (null)`**
  : Firebase provides a specialized end start analytics event. This method sends that event.
  
* **`SendLevelUp(level: long, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides a specialized event for tracking player level ups. This method sends that event.
  
* **`SendLevelUp(level: long, character: string, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides a specialized event for tracking player level ups. This method sends that event, but with an additional `character` parameter. This allows which character was leveled up, for more detailed usage analytics data in games with multiple character support.
  
* **`SendScreenView(screenName: string, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides a specialized event for tracking which app screen is viewed. This method sends that event.
  
* **`SendScreenView(screenClass: string, screenName: string, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides a specialized event for tracking which app screen is viewed. This method sends that event, but also provides the view class.
  
* **`SendSpendVirtualCurrency(currency: string, itemName: string, amount: long, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides specialized events for tracking where the user spends their virtual currency. This method sends that event
  
* **`SendSpendVirtualCurrency(currency: string, itemName: string, amount: double, extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides specialized events for tracking where the user spends their virtual currency.
  
* **`SendTutorialBegin(extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides special analytics events for tracking when the tutorial starts. This method sends that event.
  
* **`SendTutorialEnd(extraParameters: IDictionary<string, object> (null))`**
  : Firebase provides special analytics events for tracking when the tutorial ends. This method sends that event.

### Game Analytics Analytics Service

* **`SendCustom(eventName: string, parameters: IDictionary<string, object>)`**
  : Track any type of design event that you want to measure i.e. GUI elements or tutorial steps. Custom dimensions are not supported. `eventName`: String can consist of 1 to 5 segments. Segments are seperated by ':' and segments can have a max length of 16. (e.g. segment1:anotherSegment:gold). `parameters`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

* **`SendDesignEvent(eventName: string)`**
  : Track any type of design event that you want to measure i.e. GUI elements or tutorial steps. Custom dimensions are not supported. `eventName`: String can consist of 1 to 5 segments. Segments are seperated by ':' and segments can have a max length of 16. (e.g. segment1:anotherSegment:gold).

* **`SendDesignEvent(eventName: string, eventValue: float)`**
  : Track any type of design event that you want to measure i.e. GUI elements or tutorial steps. Custom dimensions are not supported. `eventName`: String can consist of 1 to 5 segments. Segments are seperated by ':' and segments can have a max length of 16. (e.g. segment1:anotherSegment:gold). `eventValue`: Number value of event.

* **`SendDesignEvent(eventName: string, customFields: IDictionary<string, object>)`**
  : Track any type of design event that you want to measure i.e. GUI elements or tutorial steps. Custom dimensions are not supported. `eventName`: String can consist of 1 to 5 segments. Segments are seperated by ':' and segments can have a max length of 16. (e.g. segment1:anotherSegment:gold). `customFields`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

* **`SendDesignEvent(eventName: string, eventValue: float, customFields: IDictionary<string, object>)`**
  : Track any type of design event that you want to measure i.e. GUI elements or tutorial steps. Custom dimensions are not supported. `eventName`: String can consist of 1 to 5 segments. Segments are seperated by ':' and segments can have a max length of 16. (e.g. segment1:anotherSegment:gold). `eventValue`: Number value of event. `customFields`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

* **`SendBusinessEvent(currency: string, amount: int, itemType: string, itemId: string, cartType: string)`**
  : Track any real money transaction in-game. `currency`: Currency code in ISO 4217 format. (e.g. USD). `amount`: Amount in cents (int). (e.g. 99). `itemType`: Item Type bought. (e.g. Gold Pack). `itemId`: Item bought. (e.g. 1000 gold). `cartType`: Cart type.

* **`SendBusinessEvent(currency: string, amount: int, itemType: string, itemId: string, cartType: string, customFields: IDictionary<string, object>)`**
  : Track any real money transaction in-game. `currency`: Currency code in ISO 4217 format. (e.g. USD). `amount`: Amount in cents (int). (e.g. 99). `itemType`: Item Type bought. (e.g. Gold Pack). `itemId`: Item bought. (e.g. 1000 gold). `cartType`: Cart type. `customFields`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

* **`SendErrorEvent(severity: ErrorSeverity, message: string)`**
  : Set up custom error events in the game. You can group the events by severity level and attach a message. `severity`: Severity of error. `message`: Error message (Optional, can be nil).

* **`SendErrorEvent(severity: ErrorSeverity, message: string, customFields: IDictionary<string, object>)`**
  : Set up custom error events in the game. You can group the events by severity level and attach a message. `severity`: Severity of error. `message`: Error message (Optional, can be nil). `customFields`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

* **`SendResourceEvent(flowType: ResourceFlowType, currency: string, amount: float, itemType: string, itemId: string)`**
  : Analyze your in-game economy (virtual currencies). You will be able to see the flow (sink/source) for each virtual currency. `flowType`: Add or subtract resource. `currency`: One of the available currencies set in Settings (Setup tab). `amount`: Amount sourced or sinked. `itemType`: One of the available currencies set in Settings (Setup tab). `itemId`: Item id (string max length=16).

* **`SendResourceEvent(flowType: ResourceFlowType, currency: string, amount: float, itemType: string, itemId: string, customFields: IDictionary<string, object>)`**
  : Analyze your in-game economy (virtual currencies). You will be able to see the flow (sink/source) for each virtual currency. `flowType`: Add or subtract resource. `currency`: One of the available currencies set in Settings (Setup tab). `amount`: Amount sourced or sunk. `itemType`: One of the available currencies set in Settings (Setup tab). `itemId`: Item id (string max length=16). `customFields`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

* **`SendAdEvent(adAction: AdAction, adType: AdType, adSdkName: string, adPlacement: string)`**
  : Track fill-rate of your ads. `adAction`: Action of ad (for example loaded, show). `adType`: Type of ad (for video, interstitial). `adSdkName`: Name of ad SDK. `adPlacement`: Placement of ad or identifier of the ad in the app.

* **`SendAdEvent(adAction: AdAction, adType: AdType, adSdkName: string, adPlacement: string, duration: long)`**
  : Track fill-rate of your ads. `adAction`: Action of ad (for example loaded, show). `adType`: Type of ad (for video, interstitial). `adSdkName`: Name of ad SDK. `adPlacement`: Placement of ad or identifier of the ad in the app. `duration`: Duration of ad video.

* **`SendAdEvent(adAction: AdAction, adType: AdType, adSdkName: string, adPlacement: string, noAdReason: AdError)`**
  : Track fill-rate of your ads. `adAction`: Action of ad (for example loaded, show). `adType`: Type of ad (for video, interstitial). `adSdkName`: Name of ad SDK. `adPlacement`: Placement of ad or identifier of the ad in the app. `noAdReason`: Error reason for no ad available.

* **`SendAdEvent(adAction: AdAction, adType: AdType, adSdkName: string, adPlacement: string, customFields: IDictionary<string, object>)`**
  : Track fill-rate of your ads. `adAction`: Action of ad (for example loaded, show). `adType`: Type of ad (for video, interstitial). `adSdkName`: Name of ad SDK. `adPlacement`: Placement of ad or identifier of the ad in the app. `duration`: Duration of ad video. `customFields`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

* **`SendAdEvent(adAction: AdAction, adType: AdType, adSdkName: string, adPlacement: string, duration: long, customFields: IDictionary<string, object>)`**
  : Track fill-rate of your ads. `adAction`: Action of ad (for example loaded, show). `adType`: Type of ad (for video, interstitial). `adSdkName`: Name of ad SDK. `adPlacement`: Placement of ad or identifier of the ad in the app. `duration`: Duration of ad video. `customFields`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

* **`SendAdEvent(adAction: AdAction, adType: AdType, adSdkName: string, adPlacement: string, noAdReason: AdError, customFields: IDictionary<string, object>)`**
  : Track fill-rate of your ads. `adAction`: Action of ad (for example loaded, show). `adType`: Type of ad (for video, interstitial). `adSdkName`: Name of ad SDK. `adPlacement`: Placement of ad or identifier of the ad in the app. `noAdReason`: Error reason for no ad available. `customFields`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

* **`SendImpressionEvent(adUnitId: string, adUnitFormat: string, networkName: string, placement: string, creativeId: string, revenue: double, countryCode: string, sdkName: string, sdkVersion: string, rawData: string)`**
  : Track impression of your ads. `adUnitId`: Unit id of the ad `adUnitFormat`: Format of the ad `networkName`: Network of the ad `placement`: Placement of the ad `creativeId`: Creative id of the `revenue`: Revenue of the ad `countryCode`: Country of the ad `sdkName`: Mediation SDK name `sdkVersion`: Mediation SDK version `rawData`: raw impression data

* **`SendProgressionEvent(progressionStatus: ProgressionStatus, progression01: string)`**
  : Measure player progression in the game. It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place. `progressionStatus`: Status of added progression. `progression01`: 1st progression (e.g. world01). 

* **`SendProgressionEvent(progressionStatus: ProgressionStatus, progression01: string, score: int)`**
  : Measure player progression in the game. It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place. `progressionStatus`: Status of added progression. `progression01`: 1st progression (e.g. world01). `score`: The player's score. 

* **`SendProgressionEvent(progressionStatus: ProgressionStatus, progression01: string, progression02: string)`**
  : Measure player progression in the game. It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place. `progressionStatus`: Status of added progression. `progression01`: 1st progression (e.g. world01). `progression02`: 2nd progression (e.g. level01). 

* **`SendProgressionEvent(progressionStatus: ProgressionStatus, progression01: string, progression02: string, int score)`**
  : Measure player progression in the game. It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place. `progressionStatus`: Status of added progression. `progression01`: 1st progression (e.g. world01). `progression02`: 2nd progression (e.g. level01). `score`: The player's score.

* **`SendProgressionEvent(progressionStatus: ProgressionStatus, progression01: string, customFields: IDictionary<string, object>)`**
  : Measure player progression in the game. It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place. `progressionStatus`: Status of added progression. `progression01`: 1st progression (e.g. world01). `customFields`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

* **`SendProgressionEvent(progressionStatus: ProgressionStatus, progression01: string, progression02: string, progression03: string)`**
  : Measure player progression in the game. It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place. `progressionStatus`: Status of added progression. `progression01`: 1st progression (e.g. world01). `progression02`: 2nd progression (e.g. level01). `progression03`: 3rd progression (e.g. phase01).

* **`SendProgressionEvent(progressionStatus: ProgressionStatus, progression01: string, score: int, customFields: IDictionary<string, object>)`**
  : Measure player progression in the game. It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place. `progressionStatus`: Status of added progression. `progression01`: 1st progression (e.g. world01). `score`: The player's score. `customFields`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

* **`SendProgressionEvent(progressionStatus: ProgressionStatus, progression01: string, progression02: string, progression03: string, score: int)`**
  : Measure player progression in the game. It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place. `progressionStatus`: Status of added progression. `progression01`: 1st progression (e.g. world01). `progression02`: 2nd progression (e.g. level01). `progression03`: 3rd progression (e.g. phase01). `score`: The player's score.

* **`SendProgressionEvent(progressionStatus: ProgressionStatus, progression01: string, progression02: string, customFields: IDictionary<string, object>)`**
  : Measure player progression in the game. It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place. `progressionStatus`: Status of added progression. `progression01`: 1st progression (e.g. world01). `progression02`: 2nd progression (e.g. level01).

* **`SendProgressionEvent(progressionStatus: ProgressionStatus, progression01: string, progression02: string, score: int, customFields: IDictionary<string, object>)`**
  : Measure player progression in the game. It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place. `progressionStatus`: Status of added progression. `progression01`: 1st progression (e.g. world01). `progression02`: 2nd progression (e.g. level01). `score`: The player's score. `customFields`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

* **`SendProgressionEvent(progressionStatus: ProgressionStatus, progression01: string, progression02: string, progression03: string, customFields: IDictionary<string, object>)`**
  : Measure player progression in the game. It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place. `progressionStatus`: Status of added progression. `progression01`: 1st progression (e.g. world01). `progression02`: 2nd progression (e.g. level01). `progression03`: 3rd progression (e.g. phase01). `customFields`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

* **`SendProgressionEvent(progressionStatus: ProgressionStatus, progression01: string, progression02: string, progression03: string, score: int, customFields: IDictionary<string, object>)`**
  : Measure player progression in the game. It follows a 3 hierarchy structure (world, level and phase) to indicate a player's path or place. `progressionStatus`: Status of added progression. `progression01`: 1st progression (e.g. world01). `progression02`: 2nd progression (e.g. level01). `progression03`: 3rd progression (e.g. phase01). `score`: The player's score. `customFields`: Custom fields to add to the event. Dictionary of key-value pairs. Only string or numbers allowed as values. Custom fields are only stored in raw events and can only be used for data export (i.e. not visible in the tool).

### Unity Analytics Service

* **`SendGameStart(IDictionary<string, object> parameters = null)`**
  : Player began a new game (useful for games with a distinct beginning and ending)`**.

* **`SendGameOver(int? index = null, string name = null, IDictionary<string, object> parameters = null)`**
  : Player ended the game (useful for games with a distinct beginning and ending).

* **`SendLevelStart(int? index = null, string name = null, IDictionary<string, object> parameters = null)`**
  : Player started a level.

* **`SendLevelEnd(int? index = null, string name = null, IDictionary<string, object> parameters = null)`**
  : Player successfully completed a level.

* **`SendLevelFail(int? levelIndex = null, string name = null, IDictionary<string, object> parameters = null)`**
  : Player lost a level.

* **`SendLevelQuit(int? levelIndex = null, string name = null, IDictionary<string, object> parameters = null)`**
  : Player quit out of a level.

* **`SendLevelSkip(int? levelIndex = null, string name = null, IDictionary<string, object> parameters = null)`**
  : Player skipped past a level.

* **`SendLevelUp(int? levelIndex = null, string name = null, IDictionary<string, object> parameters = null)`**
  : Player increased in rank or RPG-style experience level.

* **`SendScreenView(string name, IDictionary<string, object> parameters = null)`**
 : Player opened a screen in the UI, such as a high score or settings screen.

* **`SendCutSceneStart(string name, IDictionary<string, object> parameters = null)`**
  : Player began watching a cinematic cutscene.

* **`SendCutSceneSkip(string name, IDictionary<string, object> parameters = null)`**
  : Player skipped past a cinematic cutscene.

* **`SendFirstInteraction(string actionId = null, IDictionary<string, object> parameters = null)`**
  : Player completed any interaction after opening the game for the first time.

* **`SendTutorialBegin(string tutorialId = null, IDictionary<string, object> parameters = null)`**
  : Player began a tutorial.

* **`SendTutorialStep(int stepIndex, string tutorialId = null, IDictionary<string, object> parameters = null)`**
  : Player passed a milestone in a tutorial.

* **`SendTutorialEnd(string tutorialId = null, IDictionary<string, object> parameters = null)`**
  : Player completed a tutorial.

* **`SendTutorialSkip(string tutorialId = null, IDictionary<string, object> parameters = null)`**
  : Player skipped a tutorial.

* **`SendNotificationEnable(IDictionary<string, object> parameters = null)`**
  : Player enabled push notifications.

* **`SendNotificationClick(string messageId, IDictionary<string, object> parameters = null)`**
  : Player responded to a pushed message.

* **`SendChatMsgSent(IDictionary<string, object> parameters = null)`**
  : Player sent a chat message.

* **`SendAchievementUnlocked(string achievementId, IDictionary<string, object> parameters = null)`**
  : Player completed an achievement.

* **`SendAchievementStep(int stepIndex, string achievementId, IDictionary<string, object> parameters = null)`**
  : Player completed a milestone towards an achievement.

* **`SendUserSignUp(string authNetwork, IDictionary<string, object> parameters = null)`**
  : Player connected with a social network.

* **`SendSocialShare(string shareType, string socialNetwork, string senderId = null, string recipientId = null, IDictionary<string, object> parameters = null)`**
  : Player shared something such as an invite or gift through a social network.

* **`SendSocialAccept(string shareType, string socialNetwork, string senderId = null, string recipientId = null, IDictionary<string, object> parameters = null)`**
  : Player accepted something shared through a social network.

* **`SendStoreOpen(StoreType storeType, IDictionary<string, object> parameters = null)`**
  : Player opened a store.

* **`SendStoreItemClick(StoreType storeType, string itemId, string itemName, IDictionary<string, object> parameters = null)`**
  : Player selected an item in a store.

* **`SendIapTransaction(string transactionContext, float price, string itemId, string itemType = null, string level = null, string transactionId = null, IDictionary<string, object> parameters = null)`**
  : Player spent real-world money to make an in-app purchase.

* **`SendItemAcquired(AcquisitionType currencyType, string transactionContext, float amount, string itemId, string itemType = null, string level = null, string transactionId = null, IDictionary<string, object> parameters = null)`**
  : Player acquired a resource within the game.

* **`SendItemSpent(AcquisitionType currencyType, string transactionContext, float amount, string itemId, float balance, string itemType = null, string level = null, string transactionId = null, IDictionary<string, object> parameters = null)`**
  : Player expended an item within the game.

* **`SendAdOffer(bool rewarded, string network = null, string placementId = null, IDictionary<string, object> parameters = null)`**
  : Player had an opportunity to watch an ad.

* **`SendAdStart(bool rewarded, string network = null, string placementId = null, IDictionary<string, object> parameters = null)`**
  : Player started watching an ad.

* **`SendAdComplete(bool rewarded, string network = null, string placementId = null, IDictionary<string, object> parameters = null)`**
  : Player finished watching an ad.

* **`SendAdSkip(bool rewarded, string network = null, string placementId = null, IDictionary<string, object> parameters = null)`**
  : Player skipped an ad before completion.

* **`SendPostAdAction(bool rewarded, string network = null, string placementId = null, IDictionary<string, object> parameters = null)`**
  : Player completed an action prompted by an ad.