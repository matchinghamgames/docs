# Changelog

## [12.1.1] - 2024-10-25
### Added 
* Separate code file (`SherlockConfig.Validations.cs`) to contain validation methods of `SherlockConfig.cs`

## [12.1.0] - 2024-09-11
### Added
* `ConversionValue:` `ToString` override method
* `AnalyticsEvent`: `ToString` override method
* While importing events from CSV, if there are existing events in the config, you will be asked if you want to overwrite or skip.
* After importing events, events added or overwritten will be logged (Make sure that you don't clear your console on recompiling).
### Changed
* `ImportEvents` -> `EventImporter`
* Handyman dependency v5.8.1 -> v5.8.2

## [12.0.0] - 2024-09-09
### Removed
* `IAdjustAnalyticsSerivce`: `SendAdRevenue(string source, string payload)`: Adjust SDK v5 doesn't support this anymore.
* `IAdjustAnalyticsSerivce`: `SendInAppPurchaseRevenueEventForVerification(string eventName, string productId, string purchaseToken, double price, string currency)`
* `IAdjustAnalyticsSerivce`: `SendInAppPurchaseRevenueEventForVerification(string eventName, string productId, string transactionId, string receipt, double price, string currency)`: Adjust SDK v5 doesn't support this anymore.
* `IAdjustAnalyticsSerivce`: `SendInAppPurchaseRevenueEvent(string eventName, string transactionId, double price, string currency)`
* `IAdjustAnalyticsSerivce`: `UpdateConversion(int conversionValue)`: Adjust SDK v5 doesn't support this anymore.
* `IAdjustAnalyticsSerivce`: `GetAttribution(): IAttributionInfo`: Adjust SDK v5 doesn't support this anymore.
* `SherlockUtility`: `IsConversionEvent(string eventName)`: Adjust SDK v5 doesn't support this anymore.
* `SherlockUtility`: `ConversionValue(string eventName)`: Adjust SDK v5 doesn't support this anymore.
* `SherlockUtility`: `TryGetConversionValue(string eventName, out int conversionValue)`: Adjust SDK v5 doesn't support this anymore.
* `IAttributionInfo`: `adid`: Adjust SDK v5 doesn't support this anymore.
### Added
* `ConversionValue`, `CoarseValue` structs
* `IAdjustAnalyticsSerivce`: `SendInAppPurchaseRevenueEvent(string eventName, string productId, string transactionId, purchaseToken, double price, string currency, DateTime? purchaseDate)`: Unified logic for both Android and iOS.
* `IAdjustAnalyticsSerivce`: `UpdateConversion(ConversionValue conversionValue)`
* `IAdjustAnalyticsSerivce`: `GetAttribution(): Task<IAttributionInfo>`
* `SherlockUtility`: `TryGetConversionValue(string eventName, out ConversionValue? conversionValue)`
* `Config`: Search box for `events`.
### Changed
* CSV parsing (See `Migration Guide`)
* Handyman dependency: v5.5.0 -> v5.8.1
### Migration Guide
* Due to changes on `conversionValue` on Adjust SDK v5, CSV parsing logic has been changed. See documentation for further details.
* If you are using `Sherlock - Adjust` service, you need to update it to v8.x

## [11.4.0] - 2024-08-20
### Added
* `TrackAdmobRevenue` event for admost analytics.

## [11.3.1] - 2024-05-13
### Fixed
* Implemented missing API in `NullAdjustAnalyticsService`

## [11.3.0] - 2024-05-06
### Removed
* All logic related to `Meteor`. See `Migration Guide`
### Changed
* Handyman dependency: v5.3.1 -> v5.5.0
### Added
* Facebook Analytics Ad Impression Event: `IFacebookAnalytics.SendAdImpression(float, string)`
### Migration Guide
* All values in configuration is no longer remote. If you are using any remote values for this module. Make sure that you moved to local. This also enables this module to be able to be initialized before `Meteor`.

## [11.2.0] - 2024-04-18
### Added
* SendInAppPurchaseRevenueEventForVerification Custom Event added for Adjust IAP Verification.
* `IFirebaseAnalyticsService.SetUserId(string)` API for exposing related FirebaseAnalytics API
* `IFirebaseAnalyticsService.SetUserProperty(string, string)` API for exposing related FirebaseAnalytics API

## [11.2.0-prev.1] - 2024-04-04
### Added
* `IFirebaseAnalyticsService.SetUserId(string)` API for exposing related FirebaseAnalytics API
* `IFirebaseAnalyticsService.SetUserProperty(string, string)` API for exposing related FirebaseAnalytics API

## [11.2.0-prev.0] - 2024-04-04
### Added
* SendInAppPurchaseRevenueEventForVerification Custom Event added for Adjust IAP Verification.

## [11.1.1] - 2024-03-25
### Fixed
* Compile error when using `LOG4NET`

## [11.1.0] - 2024-02-16
### Added
* `FailureReason` implementation
### Changed
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0

## [11.0.0] - 2024-02-09
### Added
* [OPTIONAL] `DefaultAdImpressionEventTag` configuration to manage Ad Impression Event Tag in Vegas.
### Changed
* `IAdmostAnalyticsService.SendIapEvent` method now requests an args object as parameter.

## [11.0.0-prev.1] - 2024-02-06
### Added
* [OPTIONAL] `DefaultAdImpressionEventTag` configuration to manage Ad Impression Event Tag in Vegas.

## [11.0.0-prev.0] - 2024-01-24
### Changed
* `IAdmostAnalyticsService.SendIapEvent` method now requests an args object as parameter.

## [10.0.0] - 2023-11-17
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
### Removed
* `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.

## [9.2.1] - 2023-10-09
### Added
* Config event validation checks
### Fixed
* Compile error due to not implemented interface members in `NullFacebookAnalyticsService`

## [9.2.0] - 2023-10-05
### Added
* Facebook Analytics Service, Send Custom Event.

## [9.2.0-prev.1] - 2023-10-05
### Added
* Facebook Analytics Service Support.

## [9.1.2] - 2023-09-28
### Changed
* Added AppOpen ad type to AdType enum for GameAnalytics.

## [9.1.1] - 2023-08-01
### Fixed
* Compile error due to not implemented interface members in `NullAdjustAnalyticsService`

## [9.1.0] - 2023-08-01
### Added
* `IAttributionInfo` interface
* `AttributionChangedDelegate(IAttributionInfo): void` delegate
* `GetAttribution(): IAttributionInfo` method to `IAdjustAnalyticsService`
* `SetAttributionChangeCallback(AttributionChangedDelegate): void` method to `IAdjustAnalyticsService`

## [9.0.1] - 2023-06-14
### Changed
* `SherlockUtility`: `GetToken`: If the token isn't found in Sherlock.Config, the message becomes warning.

## [9.0.0] - 2023-05-12
### Added
* Custom timeout support for service initialization 
### Changed
* `SherlockUtility`: eCPMEvent, CumulativeRevenueEvent, CumulativeRevenueCountEvent, IAPEvent, LevelEvent naming becomes dynamic with respect to configuration

## [8.3.1] - 2023-05-05
### Added
* Cumulative revenue event naming conventions in SherlockUtility. 

## [8.2.1] - 2023-04-13
### Changed
* _(internal)_ Version define symbols overhaul
### Added
* Data Usage Consent Manager initialization dependency

## [8.2.0] - 2023-01-24
### Changed
* `Import Events` feature has been altered.
  * IOS token column can be added in the csv file.

## [8.1.1] - 2023-01-09
### Added
* New Debugger implementation

### [8.1.0] - 2022-11-29
### Added
* `Override Adjust Token for iOS`: For events which have separate tokens for iOS

## [8.0.0] - 2022-11-23
### Changed
* `Dictionary` => `IDictionary` for all methods.
* Extra parameters added to `IFirebaseAnalyticsService`

## [7.0.0] - 2022-11-16
### Changed
* `IGameAnalyticsService`: `rawData` added to `SendAdImpression`, for further compatibility

## [6.0.3] - 2022-10-12
### Fixed
* `Config`: `Enabled` and `LogFlags` wasn't serialized.

## [6.0.2] - 2022-09-06
### Changed
* `Meteor` initialization message is `warning` instead of `error` now

## [6.0.1] - 2022-09-01
### Fixed
* Compile fix on null services

## [6.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`: All public fields and methods are static now.
### Removed
* `LevelTimeStatManager` moved to `game-utilities` package

## [5.1.2] - 2022-08-11
### Changed
* IFirebaseAnalyticsService: SendAdImpression: placement => adFormat

## [5.1.1] - 2022-08-10
### Changed
* IAdjustAnalyticsService: `float` => `double` in all methods

## [5.1.0] - 2022-07-20
### Added
* LevelTimeStatManager: DisposeLevel method

## [5.0.0] - 2022-07-20
### Changed
* LevelTimeStatManager: `level` is `ulong` now

## [4.0.4] - 2022-07-19
### Fixed
* LevelTimeStatManager: OnDestroy method wasn't overriding base class

## [4.0.3] - 2022-07-08
### Fixed
* Compile error while building

## [4.0.2] - 2022-07-08
### Fixed
* Compile error while building

## [4.0.1] - 2022-07-06
### Fixed
* Compile error on NullUnityGameAnalytics

## [4.0.0] - 2022-07-06
### Changed
* Null Services won't print logs on initialization
* Null Services initialization result will always be failure.
### Added
* More logs on initialization
* IUnityAnalyticsService

## [3.3.0] - 2022-06-27
### Added
* SherlockUtility: `GetLevelEventName` overload for `uint`, `long` and `ulong`.

## [3.2.3] - 2022-06-02
### Fixed
* Compile error when Adjust service is not present

## [3.2.2] - 2022-06-02
### Changed
* Default values on config are set

## [3.2.1] - 2022-04-27
* Compile fix on NullGameAnalyticsService

## [3.2.0] - 2022-04-27
### Added
* IFirebaseAnalyticsService.SendAdImpression
* IGameAnalyticsService.SendImpressionEvent
### Changed
* IAdjustAnalyticsService.SendAdRevenue parameter names overhaul

## [3.1.0] - 2022-04-12
### Added
* A new adrevenue method for IAdjustAnalyticsService

## [3.0.1] - 2022-03-09
### Fixed
* Dummy Services' namespaces

## [3.0.0] - 2022-03-09
### Changed
* Namespace

## [2.0.1] - 2022-03-09
### Changed
* Dummy Services namespace correction
* Dummy GA Service define symbol fix

## [2.0.0] - 2022-03-09
* Initial release