# Backtrace Integration

## 1- Disable UNITY Cloud Diagnostics

![backtrace-cloud-diagnostics](/unity-pipe/backtrace-cloud-diagnostics.png)

## 2- Install the latest io.backtrace.unity package

Install the latest package by following this [Git URL](https://github.com/backtrace-labs/backtrace-unity.git)

![backtrace_package](/unity-pipe/backtrace_package.png)


## 3- Initialize the Backtrace client with GameObject

- In this step, you create the Backtrace Configuration asset, create a new GameObject, add the Backtrace Client component to the GameObject, then add the Backtrace Configuration to the Backtrace Client component.
- You can add the Backtrace Client component to any GameObject in your game scene.
- Open your project in the Unity Editor.
- From the Unity Editor menu, select Assets > Backtrace > Configuration. (Note: If no Backtrace option appears under Assets then close and reopen the project, and confirm the package is installed correctly.)
- Go to GameObject > Create Empty.
- Enter a name for the new GameObject.
- In the Inspector, select Add Component.
- Search for “Backtrace”, then select Backtrace Client.
- From the Assets folder, drag the Backtrace Configuration file to the Backtrace Configuration field.

## 4- Configure the server address

- The server address is required to submit errors and crashes to your Cloud Diagnostics Advanced instance.
- Open your project in the Unity Editor.
- Select the Backtrace Configuration asset.
- In the Server Address field, enter the URL for the server address.
- Open your project in the Dashboard to get your server address. b. Open the Diagnostic page of Cloud Diagnostics Advanced and select the three vertical dots on the right of your submission token. c. Select Copy Server Address to copy the address so you can paste it into the server address field.

![backtrace_configure_server_adress](/unity-pipe/backtrace_configure_server_adress.png)

![backtrace_server_adress-2](/unity-pipe/backtrace_server_adress-2.png)

## 5- Configure the exception handling

- In the Backtrace Configuration deselect the **Handle unhandled exception** option.

![backtrace_configure_server_adress](/unity-pipe/backtrace_configure_exception_handling.png)

## 6- Verify the setup

At this point, you installed and set up the Backtrace client to automatically capture crashes and exceptions in your Unity game. To test the integration, use a try/catch block to throw an exception and start sending reports.

```csharp
try
{    
    throw new Exception("Backtrace Exception Test Date: "+ DateTime.Now);
}
catch(Exception exception)
{    
    var report = new BacktraceReport(exception);    
    client.Send(report);    
    Debug.Log("Backtrace report: "+ report.Message);
}
```

## 7- CI/CD Integration

Get  backtrace access token from:
Unity Dashboard / Advanced Diagnostics / Project Settings / Symbols / Access Tokens page and add

```yml
BACKTRACE_ACCESS_TOKEN: "[add backtrace symbol access token here]"
```

variable to the /.gitlab.ci file

![backtrace_symbol_access_token](/unity-pipe/backtrace_symbol_access_token.png)

## 8- Advanced Diagnostic Page

![backtrace_page](/unity-pipe/backtrace_page.png)
