# Changelog

## [2.2.0] - 2024-06-11
### Added
* `UserResponseDuration` tracking
### Changed
* Handyman dependency v5.5.0 -> v5.7.0
### Fixed
* `PackageInitDuration` is now sent as `double` instead of `string`

## [2.1.0] - 2024-06-10
### Added
* Tracking for `Admost Bridge`, `Facebook Bridge`, `Firebase Bridge`, `Data Usage Consent - Usercentrics Service`, `Sherlock - Adjust Service`, `Sherlock - Firebase Service`, `Meteor - Admost Service`, `Meteor - Firebase Service`, `Vegas - Applovin Service`
* If a package's initialization is already finished, it will still send analytics but not `Embrace`

## [2.0.0] - 2024-05-09
### Added
* Methods for tracking custom processes. See documentation.
* (`Embrace`) `FailureReason` parameter
### Changed
* `EndTrackingStartup()` renamed to `StopTrackingStartup()`
* Startup Tracking will start on `SubsystemRegistration` rather than `AfterSceneLoad`
### Fixed
* Compatibility with other platforms than Android and iOS

## [1.0.0] - 2024-05-03
### Added
* Initial release