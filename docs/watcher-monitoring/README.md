# Watcher Monitoring Module

## Introduction

It is for monitoring startup time and overall modules' performance and reporting to Firebase and Embrace if installed.

## Installing 
* Import package from Package Manager UI.
* If you want to use `Embrace`, make sure you have configured it properly. Installation order is not important. [See Embrace docs](https://embrace.io/docs/unity/)
* If you want to use `Firebase`, make sure that you have install `Sherlock-Firebase` package.

## How to use
* Call `Watcher.Instance.Initialize()` to initialize module. Make sure you call this before every module initialization. Otherwise, it won't work properly.
* By default, startup tracking is between app launch and changing to second scene. If you have a different scene setup, you must enable `Custom Startup Tracking` and use `Watcher.EndTrackingStartup()` to end tracking when your startup is over.

## Configuration
Go to `Matchingham > Monitoring > Config`

| Variable                | Description                                                    |
|-------------------------|----------------------------------------------------------------|
| Enabled                 | Enabling/disabling the module                                  |
| AutoInitialize          | If enabled, you don't need to call Initialize method manually. |
| Custom Startup Tracking | If enabled, startup tracking will end manually by developer.   |

## API & Details

#### Methods

* **StopTrackingStartup()**
  : Stops tracking startup and sends data to Firebase and -if installed- to Embrace. Use it if you have different scene setup. See [Configuration](#configuration). You can observe this event as `MGSDK_Startup`

* **StartTrackingPackage(IInitializeHandler package)**
  : Starts tracking package. It will automatically stop tracking when its initialization is completed and sends its initialization data to Firebase and -if installed- to Embrace. You can observe this event as `MGSDK_PackageInitialization`

* **StartTrackingCustomProcess(string name)**
  : Starts tracking custom process named `name`.

* **StopTrackingCustomProcess(string name, string initializationResult, string failureReason = null, double userResponseDuration = -1)**
  : Stops tracking custom process named `name` and sends initialization data with `initializationResult`, `failureReason`, if given, and `userResponseDuration`, if given as greater than 0, to Firebase and -if installed- to Embrace. You can observe this event as `MGSDK_ProcessInitialization`

### Common

#### Methods

* **Initialize()**
  : Starts module initialization. You need to call this at the appropriate place.

* **WhenInitialized(Action callback)**
  : Allows you to register `callback` that will be fired only after the module is successfully initialized. Use this to execute logic that requires this module to be initialized first. If the module has already initialized, immediately invokes the callback.

* **WhenFailedToInitialize(Action callback)**
  : Allows you to register `callback` that will be fired only after the module fails to initialize for any reason. Use this to handle what should happen in case this module fails to initialize. If the module has already failed to initialize, immediately invokes the callback.

* **WhenReady(Action callback)**
  : Combined version of `WhenInitialized` and `WhenFailedToInitialize`. Delays execution of `callback` till module is first initialized or failed to initialize, immediately invoke the callback if it is already initialized or failed to initialize.

#### Fields

* **State**
  : Initialization state of the module

* **Instance**
  : Instance of the module

* **LateInitialized**
  : When the module needs an internet connection but the player became online while playing the game, this becomes `true`

* **Ready**
  : If the module is initialized successfully and ready to operate

* **Config**
  : Configuration of the module. See [configuration](#configuration)

* **InitializationDuration**
  : Initialization duration in seconds