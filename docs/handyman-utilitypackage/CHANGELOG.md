# Changelog

## [7.1.1] - 2025-02-07
### Changed
* NTP server for Apple devices is `ntp.nist.gov` now

## [7.1.0] - 2025-01-24
### Changed
* `Module`: `LateInitialized` setter is `protected` instead of `private` for modules which need their own late initialization logic.

## [7.0.1] - 2025-01-23
### Fixed
* _(Editor only)_ `AddressableHelper`: namespace
* _(Editor only)_ `SpriteEtcEditor`: namespace
* _(Editor only)_ `SpriteEtcEditor`: base class changed to `UnityEditor.Editor` to solve rare conflicts

## [7.0.0] - 2024-09-24
### Removed
* `MEC`
* `Extensions.ScaleIn`
* `Extensions.FadeIn`
* `Extensions.ScaleOut`
* `Extensions.FadeOut`
### Changed
* Min. Unity version is 2022.3 now
### Migration Guide
* If you are using `MEC`, you can download it from Asset Store
* If you are using the removed methods of `Extensions`, you can copy it from older versions and paste it to your project

## [6.0.0] - 2024-09-16
### Removed
* `UnbiasedTime` and any related logic: See `Migration Guide`
### Changed
* `DateTimeUtility`: Now uses NTP servers instead of `UnbiasedTime`
### Migration Guide
* After updating, remove `UNBIASED_TIME` from `Scripting Define Symbols` and remove unbiasedtime.jar from `Assets\Plugins\Android` and `libUnbiasedTime.a` from `Assets\Plugins\iOS`

## [5.8.2] - 2024-09-09
### Changed
* `AdIdUtility`: Gathering GAID logic moved to Unity main thread
### Fixed
* `AdIdUtility`: Exception on android while gathering GAID
### Migration Guide
* If you are using `Handyman` v5.8.0 or v5.8.1, you must update to this version.

## [5.8.1] - 2024-09-06
### Fixed
* `AdIdUtility`: compile error while target is iOS

## [5.8.0] - 2024-09-03
### Changed
* `AdIdUtility.FetchAndDo`: Adjust is no longer needed. Now this method uses native APIs for Android and Unity API for iOS.

## [5.7.0] - 2024-06-11
### Added
* `IUserResponseHandler`: For tracking user response duration on some packages.

## [5.6.0] - 2024-06-04
### Added
* `MGEditorUtility`: `CreateAssemblyDefinition` method: creates an assembly definition file for the specified assembly name.
* `Newtonsoft JSON` dependency

## [5.5.0] - 2024-04-24
### Added
* `IInitializeHandler`: `State`, `FailureReason`, `InitializationDuration` properties and `Initialize` method are inherited from this interface for `Module`, `Service` and `SDKBridge`.
### Changed
* `FailureReason`: Moved to separate script.
* `InitializationResult`: Moved to separate script.

## [5.4.0] - 2024-04-15
### Added
* `OnSystemBackButtonClickedOnAndroid` event on `GlobalEventsManager` to capture Android Back Button clicks.

## [5.3.3] - 2024-04-03
### Changed
* Logger dependency -> 2.1.0
### Fixed
* `Service`, `SDKBridge` now properly records end time before invoking initialization state change.
### Migration Guide
* If you are using `game-utilities` package and using `GameInitializationManager`, you must update to this version.

## [5.3.2] - 2024-04-03
### Fixed
* `Module` now properly records end time before invoking initialization state change.

## [5.3.1] - 2024-02-08
### Fixed
* `Module`, `Service`, `SDKBridge`: `WhenReady` was invoked when `State` is `Initializing` or `NotInitialized` on some cases.

## [5.3.0] - 2024-02-08
### Added
* `FailureReason`: `InEditor`, `NoService`
* _Internal_: Stopping timeout when timeout is happened
### Changed
* `AutoInitialize` is now read-only instead of hidden in inspector. If you really want to use it, change inspector's view as debug mode.

## [5.2.0] - 2024-01-31
### Added
* `FailureReason`: When a `Module`, `Service` or `SDKBridge` fails to initialize, you can get the reason.
* _(Internal)_ `Serivce`: `Timeout` methods 
### Fixed
* `Module`, `Service`, `SDKBridge`: Initialization duration is now calculated correctly.
### Changed
* `ModuleConfig`: `AutoInitialize` is now hidden in inspector. If you really want to use it, change inspector's view as debug mode.
### Migration Guide
* If you are using Handyman v5.1.x, you must update to this version.

## [5.1.0] - 2024-01-22
### Added
* `InitializaitonResult`: `NotInitialized`, `Initializing`
* `SDKBridge`: `InitializationDuration`
### Changed
* `Module`, `Service`, `SDKBridge`: Internal refactor for new `InitializationResult` enums.

## [5.0.0] - 2023-11-28
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Adjust SDK 4.35.x compatibility 
### Removed
* _(Internal)_ `Module`, `Service`, `SDKBridge`: `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.

## [4.5.0] - 2023-11-20
### Added
* MgEditorUtility `CheckScriptingDefineSymbol` feature
* Automatically installing UnbiasedTime at startup now
* `DateTimeUtility` class for DateTime operations

## [4.4.1] - 2023-11-02
### Added
* `Unbiased Time` for time cheat prevention.
* To use Unbiased time, first import package with `MatchinghamGames/Handyman/Import UnbiasedTime`
* `HasTimedOut(string) : bool` API
* `ResetTimeout(string) : void` API
### Fixed
* Exception when registering same callback to same/multiple different timeout contexts.

## [4.4.1-prev.0] - 2023-11-02
### Added
* `Unbiased Time` for time cheat prevention.
* To use Unbiased time, first import package with `MatchinghamGames/Handyman/Import UnbiasedTime`

## [4.4.0-beta.0] - 2023-09-15
### Added
* `HasTimedOut(string) : bool` API
* `ResetTimeout(string) : void` API
### Fixed
* Exception when registering same callback to same/multiple different timeout contexts.

## [4.3.2] - 2023-04-26
### Fixed
* 'DISABLE_SRDEBUGGER' bug case handled.

## [4.3.1] - 2023-04-25
### Added
* Support for disabling SRDebugger related functions when `DISABLE_SRDEBUGGER` scripting define symbol is added

## [4.3.0] - 2023-03-19
### Added
* `NumberAnimator`: `TMP_Text` support added

## [4.2.1] 2023-03-19
### Fixed
* `Module`: When initialization status is set, `WhenInitialized` and `WhenFailedToInitialize` weren't checking initialization status.
* `Service`: When initialization status is set, `WhenInitialized` and `WhenFailedToInitialize` weren't checking initialization status.
* `AsyncSingletonBehaviour`: When initialization status is set, `WhenInitialized` and `WhenFailedToInitialize` weren't checking initialization status.
* `SingletonBehaviour`: When initialization status is set, `WhenInitialized` and `WhenFailedToInitialize` weren't checking initialization status.

## [4.2.0] - 2023-03-16
### Added
* `Module`: `InitializationDuration` added to track initialization duration. This is also printed to result logs, added to DebuggerView.
* `Service`: `InitializationDuration` added to track initialization duration. This is also printed to result logs.

## [4.1.5] - 2022-12-19
### Fixed
* PauseStateManager: In some cases it wasn't working properly.
### Changed
* ModuleDebugger: Initialization becomes manual. 

## [4.1.4] - 2022-10-05
### Changed
* `Module`: code refactor
### Fixed
* `ModuleDebugger` wasn't working when `AutoInitialize` is enabled.

## [4.1.3] - 2022-09-07
### Fixed
* Rare exception when `InternetConnectionManager.NetworkReachability` changed.

## [4.1.2] - 2022-09-06
### Fixed
* `InternetConnectionManager.NetworkReachability` setter wasn't private

## [4.1.1] - 2022-09-06
### Fixed
* `InternetConnectionManager` initialization fix

## [4.1.0] - 2022-09-02
### Added
* UnityMainThreadDispatcher

## [4.0.1] - 2022-09-01
### Fixed
* `Logger` dependency added

## [4.0.0] - 2022-09-01
### Added
* `Module`: Base class for modules
* `Service`: Base class for services
* `ModuleConfig`: Base class for module configs
* `SDKBridge`: Base class for SDK initializers
* `CoroutineManager`: Basic coroutine handling outside MonoBehaviour classes.
* `ModuleDebugger`: Base class for Module Debugging (requires SRDebugger)
### Changed
* Namespace for runtime classes
* `InternetConnectionManager` will be initialized earlier
* `AsyncSingleton` class cleanup
* `[DisallowMultipleComponent]` attribute added to *Behaviour classes


## [3.1.0] - 2022-07-05
### Changed
* InternetConnectionManager refactor

## [3.0.1] - 2022-06-30
### Changed
* `GlobalEventsManager` is `SingletonBehaviour` now.
* `Save` GameEvent will be raised at `OnApplicationQuit` instead of `OnDestroy`

## [3.0.0] - 2022-06-03
### Removed
* MonoSingleton.cs
### Changed
* Lazy Constructor for Singleton classes
* If SingletonBehaviour classes are not in present, they will be instantiated in runtime.

## [2.3.0] - 2022-05-24
### Added
* AddScriptingDefineSymbol: You can add define symbol to given target.
* RemoveScriptingDefineSymbol: You can remove define symbol from given target.
### Changed
* TypeDetectorUtility moved to MGEditorUtility

## [2.2.0] - 2022-05-24
### Added
* TypeDetectorUtility: You can check if certain assemblies exist in the project.

## [2.1.1] - 2022-05-18
### Fixed
* AdIdUtility: ADJUST define symbol was missing in asmdef.

## [2.1.0] - 2022-05-16
### Added
* AdIdUtility: When Adjust SDK is added, you can get GAID or IDFA

## [2.0.2] - 2022-03-09
### Fixed
* Namespace fixes on asmdef files

## [2.0.0] - 2022-03-08
* Initial Release