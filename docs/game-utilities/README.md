# Game Utilities

## Overview

This package aims for standardizing project creation by providing initializer for our modules, basic level management and tools.

## Installing

* Import package from Package Manager UI

## How to use

### GameInitializeManager

This is a script for initializing all modules in a proper order and opening the next scene. There is a sample (Sample Scene) in Package Manager. You can import and modify it by your needs. Make sure that you added you "game scene" into `Build Settings > Scenes In Build`. Also, the scene that has this script must be `Scene 0`. To check modules' situation or your custom modules, you can make scene transition manual. The sample has this functionality.

#### Custom Processes

If you have custom processes that need to be initialized before loading the game, you can add their name to `CustomProcesses` list (See `GameConfig`). When the process ends, you have to call **OnCustomProcessInitializationCompleted(string processName)** with the process name. Otherwise, initialization will be stuck.

#### API & Details

###### Fields

* `InitializationStarted`
  : Invokes when initialization is started.

* `InitializationFinished`
  : Invokes when initialization is finished.

* `InitializationProgressChanged(float progress)`
  : Invokes when initialization progress changes. `progress` is from 0 to 1.

* `StartedWaitingForCustomProcesses`
  : Invokes when if there are custom processes while initializing.

* `IsAdTestUser`
  : Returns `true` if the user is a tester

###### Methods

* `OnCustomProcessInitializationCompleted(string processName)`
  : To inform `GameInitializeManager` when the custom process named `processName` ends.

### LevelUtility

#### Level Management

This package has a basic level management system. There are two types of levels: prefab-based and scene-based. For management, you need to create objects based on `PrefabLevelBase` and `SceneLevelBase`. And pass them to [`LevelUtility`](#levelutility). You can use `ScriptableObject`s too. 

#### Level Event Management

You can send events about level progression by using `LevelUtility`. If you have extra parameters, you can set them too. After sending events, these parameters will be reset. 

#### Level Timer Management

This package has a basic level timer management. In addition, it can detect when the game is paused. When you stop the timer, you can obtain the duration of current level. When the player quits the game, the timer data will be lost. Thus, if your game has an ability to continue current level, you have to handle this situation by yourself.

#### API & Details

##### Fields

* `CurrentLevel`: returns the current level

* `LevelUpExtraParameters`: If you have extra parameters to send with `LevelUp` method. You can add them here.
* `LevelStartExtraParameters`: If you have extra parameters to send with `SendLevelStartEvents` method. You can add them here.
* `LevelCompleteExtraParameters`: If you have extra parameters to send with `SendLevelCompleteEvents` method. You can add them here.
* `LevelFailExtraParameters`: If you have extra parameters to send with `SendLevelFailEvents` method. You can add them here.

* `LevelTimeStat`: Timing details of current level.

##### Methods

* `LoadLevel(levelBase, onLoaded, onLoading)`: invokes `onLoading`, loads `levelBase` then invokes `onLoaded`
* `UnloadCurrentLevel(onLoaded, onLoading)`: invokes `onLoading`, unloads `CurrentLevel` then invokes `onLoaded`

* `LevelUp(sendEvent, autoSave)`: Increments `Level` (see [`PlayerData`](#playerdata)). If `sendEvent` is true, it will send `levelUp` events. If `autoSave` is true, it will call `PlayerData.SaveManually()` after the level is incremented. After all of these, it will reset `LevelUpExtraParameters`.
* `SendLevelStartEvents(level)`: Sends level start events of `level`. Then, it will reset `LevelStartExtraParameters`.
* `SendLevelCompleteEvents(level)`: Sends level complete events of `level`. Then, it will reset `LevelCompleteExtraParameters`.
* `SendLevelFailEvents(level)`: Sends level fail events of `level`. Then, it will reset `LevelFailExtraParameters`.

* `StartLevelTimer()`: Starts a level timer.
* `StopLevelTimer()`: Stops the started level timer. Returns the duration of the level.

### GameConfig

This is a `partial` class for managing configuration of the game. There are some predefined variables:

| Variable                                | Remote Name                          | Description                                                                                                                                                   |
|-----------------------------------------|--------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Data Type                               | N/A                                  | The location of `Stash` of the game                                                                                                                           |
| Auto Save                               | N/A                                  | When true, any changes on `PlayerData` will be saved automatically. Otherwise, it will be saved on `OnApplicationPause`, see also [`PlayerData`](#playerdata) |
| Disable Auto Load Next Scene On Debug   | N/A                                  | When SDKs are ready in `GameInitializeManager`, if `true`, `Scene Id` scene won't be opened automatically. See [GameInitializeManger](#gameinitializemanager) |
| Scene Id                                | gameSceneId                          | When SDKs are ready in `GameInitializeManager`, which scene will be loaded                                                                                    |
| Open Debug Menu On Test Devices         | openDebugMenuOnTestDevices           | If `true`, `SRDebugger` will be initialized, even if in production, on test devices.                                                                          |
| TestDeviceIds                           | gameTestDeviceIds                    | Advertising Ids list of the test devices                                                                                                                      |
| IsVegasInitializationEssential          | IsVegasInitializationEssential			    | Set true if you do not want to wait for ad module to initialize before starting the game													                                                         |
| IsInternetRequiredForCharlie          	 | IsInternetRequiredForCharlie     			 | Set true if notification module requires internet																							                                                                                      |
| CustomProcesses                       	 | N/A          								                | Use it for additional custom processes before loading the game                                                                                                |
| Target Frame Rate                       | N/A                                  | Target frame rate of the game                                                                                                                                 |

You can extend this class. You need to follow these steps:
 * Create an empty folder
 * Create an `Assembly Definition Reference` file and select it.
 * Assign `Assembly Definition` as `MatchinghamGames.GameUtilities.Runtime` and apply.
 * Create a script file named as `GameConfig.YourGameName.cs`
 * Paste this code block below:
```csharp
namespace MatchinghamGames.GameUtilities.Models
{
    public partial class GameConfig
    {
        
    }
}
```
 * Now, you can use your own configuration variables.

### PlayerData

This is a `partial` class for managing data of the player. There are some predefined variables:

| Variable        | Description                                                                                             |
|-----------------|---------------------------------------------------------------------------------------------------------|
| Level           | Level of the player                                                                                     |
| FirstSession    | If the player opens the game for the first time                                                         |
| AppVersion      | Last updated version of the game                                                                        |
| IsGameUpdated   | In First Session of updated version, tells if the game is updated or newly installed. Not persistent.   |
| PreviousVersion | In First Session of updated version, tells the previous version if the game is updated. Not persistent. |

#### Methods

* **SaveManually()**: Save PlayerData manually. 
* **TryGetFirstSession(out bool firstSession)**: Fetch FirstSession variable safely.

You can extend this class. You need to follow these steps:
* Create an empty folder (If you created a folder for [`GameConfig`](#gameconfig))
* Create an `Assembly Definition Reference` file and select it.
* Assign `Assembly Definition` as `MatchinghamGames.GameUtilities.Runtime` and apply.
* Create a script file named as `PlayerData.YourGameName.cs`
* Paste this code block below:
```csharp
namespace MatchinghamGames.GameUtilities.Models
{
    public partial class GameConfig
    {
        
    }
}
```
* Now, you can use your own configuration variables.

### PlayerDataManager

This ia a wrapper class for using predefined `Stash` of this package. [`PlayerData`](#playerdata) uses this script.

#### Methods

* `Get(key, defaultValue)`
  : returns the value of the `key`. If it doesn't exist then returns `defaultValue`.

* `Set(key, value)`
  : set the `value` of the `key`

* `Has(key)`
  : returns if the `key` exists

* `Remove(key)`
  : removes the `key` and corresponding data.