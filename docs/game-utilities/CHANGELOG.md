# Changelog

## [7.1.2] - 2025-01-02
### Changed
* Charlie is only initialized if `Charlie.Config.ManualNotificationPermissionRequest` is set to `false`.
* Unity min version is now 2022.3.*

## [7.1.2-prev.0] - 2024-12-05
### Changed
* Charlie is only initialized if `Charlie.Config.ManualNotificationPermissionRequest` is set to `false`.
* Unity min version is now 2022.3.*

## [7.1.1] - 2024-11-26
### Changed
* `InitializerDebugView` GUI scaling according to screen size for better layout and readability.
* `InitializerDebugView` label coloring according to module/service initialization state
* Readme to include Initialization Information Panel usage tips.

## [7.1.0] -  2024-11-13
### Added
* `InitializerDebugView` component added for displaying module and service initialization states.
* Game Utilities Menu entry to Matchingham menu that contains special toggle to enable/disable initialization information pane.
* Copy button to initializer information panel.
* Auto inject `InitializerDebugView` if MG_DEBUG, DEVELOPMENT_BUILD or INIT_INFO_PANE is defined.

## [7.1.0-prev.3] -  2024-11-13
### Added
* Copy button to initializer information panel.

## [7.1.0-prev.2] -  2024-11-13
### Added
* Adjust GUI Layout for info pane

## [7.1.0-prev.1] -  2024-11-13
### Added
* Game Utilities Menu entry to Matchingham menu that contains special toggle to enable/disable initialization information pane.

## [7.1.0-prev.0] -  2024-11-12
### Added
* `InitializerDebugView` component added for displaying module and service initialization states.

## [7.0.0] - 2024-09-17
### Changed
* Minimum Unity version is 2022.3 now
* Handyman dependency: v5.7.0 -> v5.8.2
* _(Internal)_ `SceneLevelBase`: Using async/await instead of `MEC`

## [6.2.0-prev.1] - 2024-08-13
### Added
* PlayPass Module support.

## [6.1.0] - 2024-08-05
### Added
* `VersionTextManager`: Shows `Application.version` and the commit hash of the current build.
* `SampleScene`: `VersionText` added
* Commit hash of the current build is now also added to `SRDebugger`
### Changed
* Handyman dependency: v5.5.0 -> v5.7.0
* Watcher dependency: v2.0.0 -> v2.2.0
### Migration Guide
* If you are using sample scene, you need to re-import the sample to use `VersionText`

## [6.0.2] - 2024-07-18
### Removed
* Redundant `Meteor.RegisterConfig(Config)` is removed

## [6.0.1] - 2024-05-13
### Removed
* `Facebook MGSDK Bridge` dependency
### Migration Guide
* If you need to initialize Facebook SDK, install `Facebook MGSDK Bridge` package. It's no longer a dependency.

## [6.0.0] - 2024-05-10
### Added
* `Watcher` dependency
* `Facebook MGSDK Bridge` dependency
* `GameConfig.TargetFrameRate`: Target frame rate of the game. In some devices, when `Frame Pacing` is enabled, refresh rate is reported as 30. Thus, `Application.targetFrameRate` is now set manually. (Default is 120) Now `Frame Pacing` can be enabled from now on.
### Removed
* `ModuleInitializationData`, `ModuleType`, `GameConfig.SendGameInitializationAnalytics`: Module initialization duration tracking is now done by `Watcher` module.
### Changed
* Module initialization order: With recent updates of some modules, they don't need `Meteor`. This may improve startup time.
* Handyman dependency: v5.3.3 -> v5.5.0
### Deprecated
* `OnCustomProcessInitializationCompleted(string moduleName, InitializationResult initializationResult, double initializationDuration)`: Use `OnCustomProcessInitializationCompleted(string processName)` for informing `GameInitializeManager` that the custom process has been initialized. For tracking its duration and sending analytics, use `Watcher` module.

## [5.3.0] - 2024-04-15
### Removed
* `GameInitializeManager`: Facebook initialization (See `Migration Guide`).
* `FacebookDetector` (See `Migration Guide`).
### Changed
* Handyman dependency: v5.3.1 -> v5.3.3
### Migration Guide
* If you are using Facebook SDK, please install `Facebook MGSDK Bridge` package. Otherwise, Facebook SDK won't be initialized!

## [5.2.0] - 2024-03-14
### Added
* `Remove(string)` API for `PlayerDataManager`. Data entries can be removed if necessary via this API.

## [5.1.0] - 2024-03-11
### Changed
* `GameInitializeManager`: `PrivacyPolicy` initialization moved before `DataUsageConsentManager` initialization.
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
### Fixed
* Compile error when `DataUsageConsentManager` is not in the project.

## [5.0.4] - 2024-01-31
### Fixed
* GameInitialization, when Apollo SDK is not present, the game does not start bug fix.
* [NonFatal] Error: PrivacyPolicy: Already initialized! error message fix.

## [5.0.3] - 2024-01-24
### Fixed
* GameInitialization, last SDK initialization not being waited bug fix.

## [5.0.2] - 2024-01-03
### Removed
* `LevelBase`: `Config`, `Logger`. These are not used.

## [5.0.1] - 2023-12-25
### Fixed
* Compile error when firebase bridge is not imported.

## [5.0.0] - 2023-11-20
### Fixed
* Compile errors when building not on Android, iOS or tvOS
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.0 -> v5.0.0
* Stash dependency: v1.3.1 -> v1.4.0
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.

## [4.4.0] - 2023-11-16
### Added
* Facebook Bridge support.
* `GameInitializeManager`: Custom Processes can be added main game initialization cycle before loading game.
* `GameInitializeManager`: Sending seperate events after each module initialization.
* `GameInitializeManager`: Facebook initialization duration analytics.
* `GameInitializeManager`: WorldWide (Localization Module) initialized state analytics.
* `GameInitializeManager`: SessionCounter added.
* `GameInitializeManager`: IsAdTestUser added.
* `GameInitializeManager`: StartedWaitingForCustomProcesses event.
* `GameInitializeManager`: WhenFirstSessionDecided method, to detect first session of new update.
* `GameInitializeManager`: Vegas (Ad Module) may be optional for loading the game.
* `GameInitializeManager`: Charlie (Notification Module) can be conditioned to internet connectivity.
* `PlayerData`: TryGetFirstSession function to see if FirstSession data available.
* `PlayerDataManager` : Version check inside PlayerDataManager.
* `GameConfig`: IsVegasInitializationEssential configuration to eliminate waiting for initialization of Ad Module before loading.
* `GameConfig`: IsInternetRequiredForCharlie configuration to require internet for Notification Module.
* `GameConfig`: GameInitializationAnalyticsSendRatio configurations to limit game initialization analytics event count Range[0, 1]
* `GameConfig`: CustomProcesses configuration (not remote) to add additional processes before loading game.
### Changed
* `GameInitializeManager`: Removed GameUpdated info since it was only relevant for single session.
* `GameInitializeManager`: Backpack module does not wait unnecessarily to initialize.
### Fixed
* Waiting Firebase CheckDependencies to complete before sending events
* Fixed an `AccessViolationException` when initializing at first session.
* `PlayerData`: FirstSession is not reliable bug fix.

## [4.4.0-exp.4] - 2023-11-09
### Fixed
* Waiting Firebase CheckDependencies to complete before sending events

## [4.4.0-exp.3] - 2023-11-08
### Added
* Facebook Bridge support.

## [4.4.0-exp.2] - 2023-10-06
### Fixed
* Fixed an `AccessViolationException` when initializing at first session.

## [4.4.0-exp.1] - 2023-10-04
### Added
* `GameInitializeManager`: Custom Processes can be added main game initialization cycle before loading game.
* `GameInitializeManager`: Sending seperate events after each module initialization.
* `GameInitializeManager`: Facebook initialization duration analytics.
* `GameInitializeManager`: WorldWide (Localization Module) initialized state analytics.
* `GameInitializeManager`: SessionCounter added.
* `GameInitializeManager`: IsAdTestUser added.
* `GameInitializeManager`: StartedWaitingForCustomProcesses event.
* `GameInitializeManager`: WhenFirstSessionDecided method, to detect first session of new update.
* `GameInitializeManager`: Vegas (Ad Module) may be optional for loading the game.
* `GameInitializeManager`: Charlie (Notification Module) can be conditioned to internet connectivity.
* `PlayerData`: TryGetFirstSession function to see if FirstSession data available.
* `PlayerDataManager` : Version check inside PlayerDataManager.
* `GameConfig`: IsVegasInitializationEssential configuration to eliminate waiting for initialization of Ad Module before loading.
* `GameConfig`: IsInternetRequiredForCharlie configuration to require internet for Notification Module.
* `GameConfig`: GameInitializationAnalyticsSendRatio configurations to limit game initialization analytics event count Range[0, 1]
* `GameConfig`: CustomProcesses configuration (not remote) to add additional processes before loading game.

### Changed
* `GameInitializeManager`: Removed GameUpdated info since it was only relevant for single session.
* `GameInitializeManager`: Backpack module does not wait unnecessarily to initialize.

### Fixed
* `PlayerData`: FirstSession is not reliable bug fix.

## [4.3.0] - 2023-08-31
### Added
* Apollo initialization

## [4.2.2] - 2023-05-07
### Changed
* Added `DISABLE_SRDEBUGGER` symbol to SRDebugger initialization along with `MG_DEBUG` symbol

## [4.2.1] - 2023-04-26
### Changed
* `GameInitializeManager`: Fix, SRDebugger disable case handled.

## [4.2.0] - 2023-03-23
### Changed
* `GameInitializeManager`: Sending initialization analytics overhaul: Initialization status will also be sent

## [4.1.3] - 2023-03-23
### Changed
* `GameInitializeManager`: Sending initialization analytics order fixed

## [4.1.2] - 2023-03-23
### Changed
* `GameInitializeManager`: Sending initialization analytics order fixed
* `GameInitializeManager`: Setting active scene based on `GameConfig.SceneID` fixed
 
## [4.1.1] - 2023-03-19
### Changed
* Compile fix on `GameInitializeManager`

## [4.1.0] - 2023-03-16
### Changed
* `GameInitializeManager`: Some fields and methods became `protected` for inheritance support
* `GameInitializeManager`: Initialization analytics added
* `GameInitializeManager`: Exception on scene unloading fixed
* `GameInitializeManager`: `WhenReady` is working now

## [4.0.0] - 2023-01-10
### Changed
* `LevelTimeStatManager` removed, level timer logic moved to `LevelUtility` and also reworked.

## [3.5.0] - 2022-12-19
### Changed
* `LevelUtility`: Extra Parameter support added

## [3.4.0] - 2022-11-14
### Changed
* `LevelDataManager`: Rework for runtime platforms, Save method is internal now.
* `PlayerData`: SaveManually added.
* `GameConfig`: AutoSave and OverrideLogFlagsToAllOnDebugMode added.

## [3.3.0] - 2022-11-02
### Changed
* `SceneId` indexed scene will be loaded on GameInitializeManager instead of `1`
* Tester devices ids parsing updated

## [3.2.1] - 2022-10-19
### Changed
* `LevelUtility`: Firebase custom event send removed on level events.

## [3.2.0] - 2022-10-05
### Changed
* Enforcer init moved above
* Vibration init added

## [3.1.1] - 2022-09-06
### Changed
* Missing sprite added in sample code

## [3.1.0] - 2022-09-05
### Changed
* Initialization rework

## [3.0.2] - 2022-09-05
### Changed
* Initialization fix on MG_DEBUG

## [3.0.1] - 2022-09-02
### Changed
* compile error fix

## [3.0.0] - 2022-09-01
### Changed
* Refactor with Handyman package
* LevelTimeStatManager moved here from Sherlock

## [2.3.1] - 2022-07-18
### Changed
* GameInitializeManager: Fix hanging on splash when MG_DEBUG is added to define symbols

## [2.3.0] - 2022-07-07
### Changed
* LevelUtility: Sherlock Unity service implementation
* GameConfig: RemoteSetting attributes added

## [2.2.2] - 2022-07-06
### Changed
* Compile error fix on when DUCM is not in the project

## [2.2.1] - 2022-07-05
### Changed
* Facebook init moved to after DUCM init

## [2.2.0] - 2022-07-05
### Changed
* GameConfig is added to MeteorConfig now.
* New DUCM compatiblity

## [2.1.0] - 2022-07-01
### Changed
* Sample scene added

## [2.0.0] - 2022-07-01
### Changed
* Initial release
