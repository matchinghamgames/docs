# Changelog

## [7.2.0-preview.2] - 2025-01-29
### Fixed
* Verifying IAP with Adjust and sending purchase event to Facebook logic has now the success `result.code` comparison

## [7.2.0-preview.1] - 2025-01-24
### Fixed
* Restoring flag check for analytics events

## [7.2.0-preview.0] - 2025-01-24
### Added
* Transaction tracking via custom backend
* `Restoring` property for detecting when a restore is underway.
### Changed
* Removed `notifyUser` check for analytics events

## [7.1.0-preview.0] - 2025-01-21
### Added
* Verifying IAP with Adjust and sending purchase event to Facebook

## [7.0.8] - 2025-01-02
### Added
* Custom fake store implementation for unity's fake store

## [7.0.7] - 2024-12-19
### Removed
* FakeStore because unity purchasing conflicts and causes compile error.

## [7.0.6] - 2024-12-19
### Update
* Add UnityPurchasing FakeStore into Unity Purchasing Service.

## [7.0.5] - 2024-12-02
### Fixed
* Handle `StoreSubscriptionInfoNotSupported` exceptions in `UnityIAPService` when subscription info is not supported by the (fake) store on target platform.

## [7.0.4] - 2024-12-02
### Fixed
* Fake Store receipt validation issue when using 

## [7.0.3] - 2024-12-02
### Added
* Fake Store purchasing service implementation.
* Persistent fake store toggle and ui mode options to simulation config

## [7.0.2] - 2024-11-28
### Added
* `Sim. Free Trial` debug option for simulation purchasing service in SR Console.
* Added related tip to docs for `Sim. Free Trial` debug option.

## [7.0.1] - 2024-11-07
### Added
* `AddProductEditor.Validations.cs` to contain validation methods for `AddProductEditor.cs`
* `Metadata.Validations.cs` to contain validation methods for `Metadata.cs`
* `DealerConfig.Validations.cs` to contain validation methods for `DealerConfig.cs`
* `ValidationResult` return type to contain general purpose validation result information

## [7.0.1-prev.0] - 2024-10-25
### Added
* `AddProductEditor.Validations.cs` to contain validation methods for `AddProductEditor.cs`
* `Metadata.Validations.cs` to contain validation methods for `Metadata.cs`
* `DealerConfig.Validations.cs` to contain validation methods for `DealerConfig.cs`
* `ValidationResult` return type to contain general purpose validation result information

### Changed
* `DealerEditorUtility` validation methods to use a general purpose return value for validations

## [7.0.0] - 2024-10-22
### Fixed
`Unity IAP Service`: `Purchased` event was invoking with wrong `transactionId`.
### Changed
* Handyman dependency: v5.8.2
* Min. Unity version: 2022.3
### Migration Guide
* If you are using Dealer v6.x.x, update to this version immediately

## [6.0.0] - 2024-09-09
### Added
* `PurchaseInfo`: PurchaseToken
* `PurchaseInfo`: PurchaseDate
### Changed
* Handyman dependency: v5.5.0 -> v5.8.1
* Unity In-App Purchasing package dependency: v4.11.0 -> v4.12.2
* Unity MGSDK Bridge dependency: v2.1.0 -> v2.2.0
* `Unity IAP Service`: `receipt` parsing logic
### Removed
* `Config`: `AdjustIapVerificationEnabled`

## [5.5.1] - 2024-05-30
### Fixed
* `LocalizedPriceStringWithNoCurrencySymbol` property of metadata return value fix.

## [5.5.0] - 2024-05-21
### Changed
* Purchase and Restore calls will throw Failed events when there is no Internet Connectivity
* TryPurchase method will return false only if Dealer Service is not initialized
* Do not forget to update UnityIAPService. Click update on UnityIAPServiceVersionChecker popup window

## [5.4.1] - 2024-05-15
### Changed
* `SendIapAnalyticsToAdjust` is false by default

## [5.4.0] - 2024-05-06
### Changed
* `SendIapAnalyticsToAdjust` is true by default
* Handyman dependency: v5.3.1 -> v5.5.0
### Removed
* `Meteor` dependency. See `Migration Guide`.
### Migration Guide
* Configuration no longer has remote values. If you are using any remote values for this module. Make sure that you moved to local. This also enables this module to be able to be initialized before `Meteor`.

## [5.3.0] - 2024-04-18
### Added
* `isFreeTrial` field to `PurchaseInfo`: It works on IOS platform only for subscription products.
* `AdjustIapVerificationEnabled` field to DealerConfig: Enables Sending Adjust IAP verification events

## [5.3.0-prev.0] - 2024-04-04
### Added
* `isFreeTrial` field to `PurchaseInfo`: It works on IOS platform only for subscription products.
* `AdjustIapVerificationEnabled` field to DealerConfig: Enables Sending Adjust IAP verification events

## [5.2.2] - 2024-03-26
### Changed
* Unity In-App Purchasing package dependency: v4.10.0 -> v4.11.0

## [5.2.1] - 2024-03-05
### Added
* Caution for UnityIAPService sample updates.

## [5.2.0] - 2024-02-15
### Added
* `FailureReason` implementation
### Changed
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
* Unity MGSDK Bridge dependency: v2.0.0 -> v2.1.0
* _Internal_ `MG_SHERLOCK_BEFORE_11` -> `MG_SHERLOCK_10_OR_OLDER`

## [5.1.2] - 2024-02-14
### Fixed
* UnityIAPService: Updated code that fired Failed event when SKUnknownError was captured to also appropriately fire BusyStateChanged event.
### Changed
* UnityIAPServiceVersionChecker message updates

## [5.1.1] - 2024-02-13
### Added
* Added editor script for checking and prompting the user for updating/importing the Unity IAP Service sample.

## [5.1.0] - 2024-02-09
### Changed
* The `Enabled` property in `SimulationConfig` to always return `true` when accessed in Unity Editor
* `receipt` is now set on `PurchaseInfo` regardless of platform in `UnityIapService`
### Added
* `transactionId` field to `PurchaseInfo`

## [5.1.0-prev.2] - 2024-02-05
### Changed
* The `Enabled` property in `SimulationConfig` to always return `true` when accessed in Unity Editor

## [5.1.0-prev.0] - 2024-01-24
### Added
* `transactionId` field to `PurchaseInfo`
### Changed
* `receipt` is now set on `PurchaseInfo` regardless of platform in `UnityIapService`

## [5.0.1] - 2023-12-23
### Added
* `LocalizedPriceStringWithNoCurrencySymbol` property for metadata to use as price string where currency symbol might not be supported.

## [5.0.0] - 2023-11-20
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
* Backpack dependency: v3.0.0-preview -> v4.0.0
* Unity IAP dependency: v4.9.4 -> v4.10.0
* Unity MGSDK Bridge dependency: v1.0.1 -> v2.0.0
* Unity IAP Service: Will be compiled only on Editor, Android, iOS and tvOS
### Removed
* `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* *Don't forget to reimport `Unity IAP Service`!*

## [4.2.4] - 2023-11-10
### Fixed
* Bugfix: No dealer service when in prod mode with simulation enabled

## [4.2.3] - 2023-11-10
### Changed
* Unity IAP Service Menu classes are used with their full name (UnityEditor.Menu)

## [4.2.2] - 2023-11-09
### Changed
* Internet is not required only in Editor or when simulation is enabled in debug mode (MG_DEBUG defined)

## [4.2.1] - 2023-10-06
### Added
* Warning for sending adjust event flag.
### Fixed
* Duplicate config remote value fix

## [4.2.0] - 2023-09-18
### Fixed
* Purchase event might be invoked before adding item to the inventory case fix
* Obsolete RequestPurchase method was removed.

## [4.1.5] - 2023-09-01
### Changed
* Updated product id validation logic to allow uppercase characters in the id as well

## [4.1.4] - 2023-08-31
### Changed
* **IMPORTANT!** Due to a critical update in Apple App Receipt Validation, `com.unity.purchasing` dependency version is updated to `4.9.4`. For ensured validation of In App Purchases on iOS, please upgrade Dealer to this version immediately.

## [4.1.3] - 2023-08-29
### Changed
* `com.unity.purchasing` dependency version is updated to `4.9.3`

## [4.1.2] - 2023-08-08
## Fixed
* Removed conditional compilation for restore fix

## [4.1.1] - 2023-08-07
## Fixed
* BUG: Items can be restored multiple times, resulting in infinite rewards.

## [4.1.0] - 2023-07-28
## Added
* Added send deferred events config entry to Configuration section in readme as well.
## Changed
* Purchase Success event sends an extra parameter for subscription renewals.
  * Event delegate updated: `PurchaseSuccessDelegate(PurchaseInfo info, bool notifyUser) : void` -> `PurchaseSuccessDelegate(PurchaseInfo info, bool notifyUser, bool isRenewal): void`
  * Used for blocking duplicate rewards for renewals.

## [4.0.8] - 2023-07-28
### Added
* iOS and Android Deferred Purchase Debug Analytic Events (See Docs)
  * Can be toggled for compilation via `Matchingham > In App Purchases > Unity IAP Service` menu.
  * `SendDeferredEvents` option in config toggles the feature entirely, but the feature needs to be compiled
    for this to have any effect.
    * Can be controlled from remote config via the remote setting value named `sendDeferredEvents`.

## [4.0.7] - 2023-07-27
### Fixed
* Unity 2020 backwards compatibility fix. (Fixes compile error in SubscriptionsContainer)

## [4.0.6] - 2023-07-27
### Fixed
* iOS expiration for subscriptions on app open

## [4.0.5] - 2023-07-27
### Fixed
* Unity Iap Service implementation for subscription issue
  * Subscriptions add their rewards on activation and remove when they expire
    * On iOS rewards could be given several times due to renewals triggering purchases that are not filtered properly

## [4.0.4] - 2023-07-27
### Fixed
* Subscriptions add their rewards on activation and remove when they expire
  * On iOS rewards could be given several times due to renewals triggering purchases that are not filtered properly

## [4.0.3] - 2023-07-18
### Fixed
* ### `GetStoreSpecificId(IapStore)` just returns empty string now when id is not defined for store
  * Prevents argument exceptions when not using all of the stores.
  * To use product id instead, set `UseProductIdForNonOverriddenStores` to true.

## [4.0.2] - 2023-07-17
### Added
* Support for store specific product ids. (Check README.md)
  * Different ids can be set for different stores. Set them in the `Store Specific Ids` dictionary in `IapProduct`. By default, each store uses the product id, but it can be overwritten. Examples
    * App Store: com.matchingham.braindom.appstore.subs
    * Play Store: com.matchingham.braindom.playstore.subs

## [4.0.1] - 2023-07-17
### Fixed
* Compile error when using analytics modules:
  * `error CS1061: 'IapProduct' does not contain a definition for 'StoreSpecificIdentifier' and no accessible extension method 'StoreSpecificIdentifier' accepting a first argument of type 'IapProduct' could be found (are you missing a using directive or an assembly reference?)`

## [4.0.0] - 2023-07-14
### Added
* Support for store specific product ids.
### Fixed
* When removing purchases, check for iap product in backpack before removing rewards to
  prevent multiple deductions for a single refund/expiration.
### Changed
* Subscription expire checks
  * For google play, we are checking for purchase receipt to determine whether a subscription
    is expired, on account hold or paused.
    * Reference: https://forum.unity.com/threads/google-play-subscription-changes-account-hold-restore.931914/#post-6433931

## [3.1.0] - 2023-06-01
### Added
* Enforcement on adding rewards while adding new products:
  * `IapProduct.ID` must not be same with `Reward.Item.ID`
  * Items in this folder `Assets/MatchinghamGames/Data/Items/IAP` cannot be used

## [3.0.0] - 2023-05-24
### Added
* Options for sending IAP analytics to `Admost`, `Adujst` and `Firebase`
* `TryPurchase`: Will return `false` if the purchase can't be processed
* `TryRestore`: Will return `false` if the restore can't be requested
* `IsPurchased`: More integrity checks
### Changed
* `ProcessPurchase`: deprecated now, use `TryPurchase` instead. Will be removed in future release.
* `RequestRestore`: deprecated now, use `TryRestore` instead. Will be removed in future release.

## [3.0.0-preview.5] - 2023-04-27
### Changed
* Not adding reward items if `Give and forget` is enabled while `isUserAction` is false

## [3.0.0-preview.4] - 2023-02-24
### Fixed
* UGS MGSDK Bridge dependency package name

## [3.0.0-preview.3] - 2023-02-23
### Changed
* Unity IAP dependency moved back.

## [3.0.0-preview.2] - 2023-02-22
### Changed
* `UnityIAPService`: Added support for `UGS MGSDK Bridge` for `Unity Gaming Services` compatibility.
* Unity IAP dependency moved to `UnityIAPService` sample

## [3.0.0-preview.1] - 2023-02-09
### Changed
* New `Backpack` implementation
* `RemoveRewardsOfExpiredSubscription` => `RemoveRewardsFromIAPRefundOrExpiration`: for adding refund support
### Added
* `UnityIAPService`: Added support for refunding non-consumable items.

## [2.0.10] - 2023-02-06
### Fixed
* Auto initialization is moved to `AfterSceneLoad` due to an issue with `SimulationPurchasingService`

## [2.0.9] - 2023-02-01
### Changed
* `AddPurchasedItemsToBackpack` should be registered to `Service.Purchased` before `SendInAppPurchaseAnalytics`, due to rare exceptions may happen in analytics.

## [2.0.8] - 2023-01-26
### Fixed
* `UnityIAPService`: Subscription item validation order is changed: First if the item is a subscription item, then if it has a valid receipt.

## [2.0.7] - 2023-01-09
### Fixed
* Auto Initialization implementation
### Changed
* `UnityIAPService` refactor
### Added
* New debugger implementation

## [2.0.6-preview.2] - 2022-10-11
### Fixed
* `DealerConfig`: `Enabled` remote setting name mismatch
* `DealerConfig`: `Enabled`, `AutoInitialize` and `OverrideLogFlagsToAllOnDebugMode` was missing on window

## [2.0.6-preview.1] - 2022-09-30
### Fixed
* Item removing on subscription expired: `Non-consumable` check removed

## [2.0.5] - 2022-09-26
### Fixed
* Removing IAP items from config throws an exception
* `IapProduct <-> Item` assignment isn't working
### Changed
* `Unity IAP Service`: code refactor

## [2.0.4] - 2022-09-26
### Added
* `null` checking for products.

## [2.0.3] - 2022-09-19
### Fixed
* `AddProductEditor`: Duplicate window name
* `AddProductEditor`: `Create` button wasn't working
* `DealerConfigWindow`: `Products` and `SimulationConfig` was cached instead of direct access

## [2.0.2] - 2022-09-06
### Changed
* UnityIAPService: init code refactor
* `Backpack` initialization enforcement

## [2.0.1] - 2022-09-02
### Fixed
* UnityIAPService: initialization fix

## [2.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`: All public fields and methods are static now.

## [1.3.6] - 2022-06-20
### Fixed
* UnityIAPService: fixed `receipt` data on `Purchased` event. After updating, you need to re-import the UnityIAPService sample.

## [1.3.5] - 2022-03-09
### Fixed
* Sherlock namespaces

## [1.3.4] - 2022-03-09
### Changed
* Namespace fixes

## [1.3.3] - 2022-03-09
* Initial release