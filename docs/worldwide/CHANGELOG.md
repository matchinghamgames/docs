# Changelog

## [5.0.0] - 2024-11-14
### Added
* GetFormatted methods are added. You can use formatted text upto 3 arguments.
* The language of the game can be changed through "Mobile System Settings" now.
  * If you want it to change in session, through "Mobile System Settings" as well, use `LanguageConfig > CheckLanguageChangeAtFocusLoss`.
* "Check Translation" functionality for DataStore objects for debugging.
* `LocalizedViewWithImage` component for UI Image localization.
### Changed
* SpecialFonts data in Language config contains material selection as well.
  * LocalizedView contains override for SpecialFonts, so that material can be customized.
  * Default Font also has material selection.
### Fixed
* Chinese Locale Fixes;
  * "zh-Hans" changed to simply "zh" (Both means Chinese Simplified)
  * "zh-HK" (Hong Kong) changed to "zh-TW" (Taiwan) for accuracy. Translators use Taiwanese.
  * System Language variations are grouped under these two categories: "zh" or "zh-TW".
* Default Font is not assigned by default now. You have to make sure it is assigned though, if you are using SpecialFonts.
### Migration Guide
* Chinese locales are updated. To be able to fix it;
  * Use "Fix Config" button in LanguageConfig.

* In case Traditional Chinese is selected in active locales, update Translation sheets and Data Stores as well.
  * Change "Chinese (Traditional, Hong Kong SAR China)" to "Chinese (Traditional)".
  * Import the changes in associated Data Stores, using "Import" buttons.
  * Create Language Files again.
    * Observe zh-Hans.txt and zh_HK files are updated to zh.txt and zh-TW files respectively.

## [4.1.0] - 2023-12-23
### Added
* Missing Character Analysis Feature for the Editor Menu, now you can detect missing characters in your fonts.
* Tooltip infos are added for Editor Menu.

### Fixed
* After Creating Language Files, LanguageConfig file gets updated late bug.

## [4.0.0] - 2023-11-16
### Added
* Unity 2023.2.x compatibility (see `Migration Guide`)
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v2.0.1 -> v5.0.0
* Unity Localization dependency: v1.3.2 -> v1.4.5
### Removed
* TextMeshPro dependency (see `Migration Guide`)
### Migration Guide
* TextMeshPro dependency:
  * Unity 2023.2 or newer users: TextMeshPro package is deprecated. Unity UI v2+ package includes TextMeshPro. It is necessary to remove TextMeshPro package.
  * Unity 2023.1 and older users: TextMeshPro package is no longer a dependency, but it is necessary to install to use this package properly.
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.   
* Unity 2021.2 and older versions are no longer supported.

## [3.2.2] - 2023-11-02
### Added
* [Editor] Misc characters are added to the character list in seperate row.

## [3.2.1] - 2023-06-22
### Added
* Default Localization Service, unnecessary space at the beginning bug.

## [3.2.0] - 2023-06-08
### Fixed
* Additional check is added if cache is deleted.
* Fix for not recognized system languages.

## [3.1.2-exp.2] - 2023-06-06
### Added
* Character Analysis Functionality Added.
### Fixed
* "The name 'testLanguage' does not exist in the current context" bug fix.

## [3.1.1-exp.1] - 2023-05-24
### Fixed
* Duplicate SetReady call removed.

## [3.1.0-exp.5] - 2023-05-22
### Fixed
* ODIN_INSPECTOR wrap for ReadOnly attribute

## [3.1.0-exp.4] - 2023-05-16
### Changed
* LanguageConfig internal fields changed to be public.

## [3.1.0-exp.3] - 2023-04-06
### Changed
* Fix; IOS file overwrite bug fix.

## [3.1.0-exp.2] - 2023-03-28
### Changed
* Fix; Initialize with SystemLanguage bug fix.

## [3.1.0-exp.1] - 2023-03-28
### Changed
* Feat; Handled LateUpdate case for LocalizationServices.

## [3.0.0-exp.2] - 2023-03-20
### Changed
* Fix; Directory not found bug fix.

## [3.0.0-exp.1] - 2023-03-20
### Changed
* Feat&Fix; Main System is designed to work with persistent files for performance gains.

## [2.2.0-exp.4] - 2023-03-13
### Changed
* Minor fix, removed unnecessary language change event invokes.

## [2.2.0-exp.3] - 2023-03-04
### Changed
* BaseDataStore Data accessibility altered.

## [2.2.0-exp.2] - 2023-02-26
### Changed
* Changelog md is required to be uppercase.

## [2.2.0-exp.1] - 2023-02-26
### Changed
* Performance optimizations; Languages can be seperated into languages to gain garbage collector performance improvement.

## [2.1.4] - 2022-12-01
### Changed
* Several BugFixes, editor improvements.

## [2.1.3] - 2022-11-27
### Changed
* Documentation Update.

## [2.1.2] - 2022-11-20
### Changed
* Log Flags can be assigned in LanguageConfig, some improvements in Logs.

## [2.1.1] - 2022-10-02
### Changed
* OnFontChange logic, and localizedView to be used in UI.

## [2.1.0] - 2022-10-02
### Changed
* Standalone and centralized capabilities are added. Ready to be used.

## [1.0.1] - 2022-08-01
### Changed
* Bugfix. Seperated Basic Resource case and Main Localization case.

## [1.0.0] - 2022-08-01
### Changed
* Basic functionality got achieved with "Resources" based data management.

## [0.0.1] - 2022-07-28
### Changed
* Beta Release
