# Changelog

## [1.0.5] - 2025-03-04
### Fixed
* DISABLE_SRDEBUGGER case compile error fix.

## [1.0.4] - 2025-02-27
### Fixed
* Retouched some extreme cases.

### Added
* Reset overrides button in SRDebugger.

## [1.0.3] - 2025-02-26
### Fixed
* Compile error fix

## [1.0.2] - 2025-02-25
### Added
* Searchable attribute for Config.

### Changed
* Ready call is sent after Meteor is ready.

## [1.0.1] - 2025-02-25
### Fixed
* Added needed Meteor Dependency, doc path fixes.

## [1.0.0] - 2025-02-24
* Initial release