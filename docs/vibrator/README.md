# Vibration Module

## Introduction

Vibrator module is a proxy module for handling iOS and Android Vibration/Haptic Feedback at the same time. Since Unity still doesn't support custom vibration, we had to do by our own.

## Installing

* Import package from Package Manager UI

## How to use
* Make sure to call `Vibrator.Instance.Initialize()` at the appropriate place
* Call the methods wherever you want. See [API&Details](#api--details)

## How does this work?

It uses native libraries of the platforms. iOS Taptic Engine is great but on Android, things are weird. The experience can be different by devices and OS versions. Vibrator module is mimicking iOS Taptic Engine APIs (Apple.CoreHaptics`). On Android we have `VibrationEffect`. Although it supports different amplitudes, some devices don't. But still we can observe different types of vibration. For example, there is another API for it: `HapticFeedback`. It is mostly used by buttons and keyboards. To support as many Android devices as possible, we matched these like this: On Android 10 or higher, `VibrationEffectConstants` will be used. Others will use `HapticFeedbackConstants`. See [configuration](#configuration) Also, we have `VibrationEffect.Compositon`, but it's optional for now.

In this module, there are four types of vibration:
 * **Transient**: Vibrates the device for a very short time period with given intensity and sharpness. (Sharpness works on iOS only)
 * **Continuous**: Vibrates the device for given duration with given intensity and sharpness. (Sharpness works on iOS only)
 * **UIVibration**: Vibrates the device for a very short time period with given type. Each type has its own intensity and sharpness settings. (Sharpness works on iOS only)
 * **UIVibrationPreset**: Vibrates the device with given preset. Each preset has its own `UIVibration` count and delays.

## Configuration
Go to `Matchingham > Vibrator > Config`

| Variable                                              | Description                                                                                                                                           |
|-------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| Enabled                                               | Enabling/disabling the module                                                                                                                         |
| AutoInitialize                                        | If enabled, you don't need to call Initialize method manually.                                                                                        |
| Android Vibration Preset Fallbacks                    | iOS Taptic Engine already has some presets. These are Android equivalent sequences.                                                                   |
| Android Vibration Effect Fallbacks                    | On Android 10 and higher (API Level 29+) `VibrationEffect` will be used for `UIVibrationType`s                                                        |
| Android Use Predefined Vibration Effects On Transient | If true, `Predefined Effect`s will be used on transient vibration, else `VibrationEffect`                                                             |
| Effect Click Min Intensity                            | When `Android Use Predefined Vibration Effects On Transient` is true, this is the intensity threshold of `EFFECT_CLICK` on transient vibration.       |
| Effect Heavy Click Min Intensity                      | When `Android Use Predefined Vibration Effects On Transient` is true, this is the intensity threshold of `EFFECT_HEAVY_CLICK` on transient vibration. |
| Android Haptic Feedback Fallbacks                     | On Android 9 and lower (API Level 28-) `HapticFeedbacks` will be used for `VibrationEffect`s                                                                                             |
| Android Default Transient Vibration Duration          | Legacy Vibration Duration                                                                                                                             |

## API & Details

### Vibrator

#### Enums

* **UIVibrationType**: Based on iOS Taptic Engine. Default Android equivalents based on API Level are written next to it.
``` 
Soft = Android API Level 29+: EFFECT_TICK, Android API Level 28- LONG_PRESS
Selection = Android API Level 29+: EFFECT_TICK, Android API Level 28- LONG_PRESS
Light = Android API Level 29+: EFFECT_TICK, Android API Level 28- LONG_PRESS
Medium = Android API Level 29+: EFFECT_CLICK, Android API Level 28- VIRTUAL_KEY
Rigid = Android API Level 29+: EFFECT_HEAVY_CLICK, Android API Level 28- VIRTUAL_KEY
Heavy = Android API Level 29+: EFFECT_HEAVY_CLICK, Android API Level 28- VIRTUAL_KEY
```

* **UIVibrationPreset**: Based on iOS Taptic Engine. You can change it from Config.
```
Warning // Heavy + 0.175sec + Medium
Error // Medium + 0.125sec + Medium + 0.125sec + Heavy + 0.125sec + Soft
Success // Medium + 0.15sec + Heavy
```    

#### Models

* **UIVibrationSequence**:
  * **UIVibrationType** Type
  * **float** Delay

#### Fields

* **Enabled**: To enable/disable the vibrator. If not set, Config's `Enabled` value will be applied.
* **CanVibrate**: Checks if the device has capability of vibration

#### Methods

* **Vibrate(float intensity, float sharpness) : bool**
  : Vibrate device once with given `intensity` and `sharpness`. `sharpness` is works on iOS only. Returns if the vibration is successful or not.

* **Vibrate(float duration, float intensity, float sharpness)**
  : Vibrate device for given `duration` seconds with given `intensity` and `sharpness`. `sharpness` is works on iOS only. Returns if the vibration is successful or not.

* **Vibrate(UIVibrationType type)**
  : Vibrate device once. `type`: Type of the vibration. Returns if the vibration is successful or not.

* **Vibrate(UIVibrationPreset preset)**
  : Vibrate device according to given preset. `preset`: Predefined sequence of vibration. Returns if the vibration is successful or not.

* **Vibrate(params UIVibrationSequence[] sequence)**
  : Vibrate device according to given sequence array. `sequence`: Sequence array of vibration.

### Platform Specific

#### Android

On Android, there are three classes for vibration. 

##### VibrationEffect.Composition

###### Enums

* **VibrationEffectCompositionConstants**:  On some devices, some of them might not work.
```
// API Level 31+
PRIMITIVE_LOW_TICK // This very short low frequency effect should produce a light crisp sensation intended to be used repetitively for dynamic feedback. 

// API Level 30+
PRIMITIVE_CLICK // This effect should produce a sharp, crisp click sensation. 
PRIMITIVE_QUICK_FALL // A haptic effect that simulates quick downwards movement with gravity. 
PRIMITIVE_QUICK_RISE // A haptic effect that simulates quick upward movement against gravity. 
PRIMITIVE_SLOW_RISE // A haptic effect that simulates slow upward movement against gravity. 
PRIMITIVE_SPIN // A haptic effect that simulates spinning momentum. 
PRIMITIVE_THUD // A haptic effect that simulates downwards movement with gravity. 
PRIMITIVE_TICK // This very short effect should produce a light crisp sensation intended to be used repetitively for dynamic feedback.
```

###### Models

* **CompositeVibration**:
 * **PrimitiveID**: Type of the primitive vibration
 * **Delay**: Delay before the vibration
 * **Scale**: Magnitude of the vibration

###### Methods

##### VibrationEffect

###### Enums

* **VibrationEffectConstants**: API Level 29 and above
```
EFFECT_CLICK // A click effect. Use this effect as a baseline, as it's the most common type of click effect.
EFFECT_DOUBLE_CLICK // A double click effect.              
EFFECT_HEAVY_CLICK // A heavy click effect. This effect is stronger than EFFECT_CLICK.
EFFECT_TICK // A tick effect. This effect is less strong compared to EFFECT_CLICK.
```

###### Fields
* **CanVibrate**: Checks if the android device can vibrate.

###### Methods
* **VibratePredefined(VibrationEffectConstants effectID) : bool**: Vibrates device by given effect. `effectID`: One of the constants defined in VibrationEffectConstants. Returns if the vibration is successful or not. For example, if the device OS is lower than 10, this will return false.
* **VibrateOneShot(long milliseconds, int amplitude) : bool**: Create a one shot vibration. One shot vibrations will vibrate constantly for the specified period of time at the specified amplitude, and then stop. `miliseconds`: Duration `amplitude`: The strength of the vibration. Some devices may not have amplitude control. If that so, amplitude value won't be used. Returns if the vibration is successful or not.
* **VibrateWaveform(long[] timings, int repeat) : bool**: Create a waveform vibration. `timings`: time between vibrations. Returns if the vibration is successful or not.
* **VibrateWaveform(long[] timings, int[] amplitudes, int repeat) : bool**: Create a waveform vibration. `timings`: time between vibrations. `amplitudes`: The strength pair with `timings`. Some devices may not have amplitude control. If that so, amplitude value won't be used. Returns if the vibration is successful or not.
* **Cancel**: Cancels the current vibration.

##### HapticFeedback

This is for a more immersive view on native apps, but also the most similar feedback with iOS Taptic Engine on older Android devices. 

###### Enums

* **HapticFeedbackConstants**: On some devices, some of them might not work.
```
// API Level 30+
GESTURE_END // The user has finished a gesture (e.g. on the soft keyboard).
GESTURE_START // The user has started a gesture (e.g. on the soft keyboard).
CONFIRM // A haptic effect to signal the confirmation or successful completion of a user interaction.
REJECT // A haptic effect to signal the rejection or failure of a user interaction.
       
// API Level 27+
KEYBOARD_PRESS // The user has pressed a virtual or software keyboard key.
KEYBOARD_RELEASE // The user has released a virtual keyboard key.
TEXT_HANDLE_MOVE // The user has performed a selection/insertion handle move on text field
VIRTUAL_KEY_RELEASE // The user has released a virtual key.
        
// API Level 23+
CONTEXT_CLICK // The user has performed a context click on an object.
        
// API Level 21+
CLOCK_TICK // The user has pressed either an hour or minute tick of a Clock.
        
// API Level 8+
KEYBOARD_TAP // The user has pressed a soft keyboard key.
      
// API Level 5+
VIRTUAL_KEY // The user has pressed on a virtual on-screen key.
        
// API Level 3+
LONG_PRESS // The user has performed a long press on an object that is resulting in an action being performed.
```

###### Methods

* **Feedback(HapticFeedbackConstants key, bool overrideGlobalSetting = false)**: Provide haptic feedback to the user. `key`: One of the constants defined in `HapticFeedbackConstants`. `overrideGlobalSetting`: If you want to override the user's global vibration setting (This is deprecated starting with API Level 33)

#### iOSVibratorPlatform

Although Vibrator module is basically same with iOS Taptic Engine API (Apple.CoreHaptics), you can still call iOS API by yourself. 

##### Enums

###### Fields
* **CanVibrate**: Checks if the iOS device can vibrate.

##### Methods

* **VibrateTransient(float intensity, float sharpness) : bool**
  : Vibrate device once with given `intensity` and `sharpness`. Returns if the vibration is successful or not.

* **VibrateContinuous(float duration, float intensity, float sharpness)**
  : Vibrate device for given `duration` seconds with given `intensity` and `sharpness`. Returns if the vibration is successful or not.

* **VibrateImpactFeedback(UIVibrationType type)**
  : Vibrate device once. `type`: Type of the vibration. Returns if the vibration is successful or not.

* **VibrateNotificationFeedback(UIVibrationPreset preset)**
  : Vibrate device according to given preset. `preset`: Predefined sequence of vibration. Returns if the vibration is successful or not.

### Common

#### Methods

* **Initialize()**
  : Starts module initialization. You need to call this at the appropriate place.

* **WhenInitialized(Action callback)**
  : Allows you to register `callback` that will be fired only after the module is successfully initialized. Use this to execute logic that requires this module to be initialized first. If the module has already initialized, immediately invokes the callback.

* **WhenFailedToInitialize(Action callback)**
  : Allows you to register `callback` that will be fired only after the module fails to initialize for any reason. Use this to handle what should happen in case this module fails to initialize. If the module has already failed to initialize, immediately invokes the callback.

* **WhenReady(Action callback)**
  : Combined version of `WhenInitialized` and `WhenFailedToInitialize`. Delays execution of `callback` till module is first initialized or failed to initialize, immediately invoke the callback if it is already initialized or failed to initialize.

#### Fields

* **State**
  : Initialization state of the module

* **Instance**
  : Instance of the module

* **LateInitialized**
  : When the module needs an internet connection but the player became online while playing the game, this becomes `true`

* **Ready**
  : If the module is initialized successfully and ready to operate

* **Config**
  : Configuration of the module. See [configuration](#configuration)

* **InitializationDuration**
  : Initialization duration in seconds