# Changelog

## [7.0.3] - 2024-11-11
### Fixed
* `Apple.CoreHaptics` updated dependency: v1.2.3

## [7.0.2] - 2024-11-11
### Fixed
* Compile error when target is Android

## [7.0.1] - 2024-11-08
### Changed
* Not using GUIDs in referencing asmdefs.

## [7.0.0] - 2024-11-07
### Added
* `IVibratorPlatform`, `AndroidVibratorPlatform`, `iOSVibratorPlatform`
* `Apple.CoreHaptics` dependency: v1.2.2
* `Vibrate(float intensity, float sharpness)`
* `Vibrate(float duration, float intensity, float sharpness)`
* (Android) `LegacyVibration`: Exception handling
* (Android) `VibrationEffect`: Exception handling
* (Android) `VibrationEffect.Composition`: Exception handling
* More controls on SRDebugger including `VibrationEffect.Composition` and new methods
* Config: `AndroidUsePredefinedVibrationEffectsOnTransient`
* Config: `EffectClickMinIntensity`
* Config: `EffectHeavyClickMinIntensity`
### Removed
* Sample code. Since Debugger implementation, it became obsolete.
* (Android) `CommonClasses`: Moved into `AndroidVibratorPlatform`
* (iOS) `ImpactFeedback`, `NotificationFeedback`, `TapticEngine`
### Changed
* Complete rework (see `Migration Guide`)
* `VibrationPreset` -> `UIVibrationPreset`
* `VibrationType` -> `UIVibrationType`
* `VibrationSequence` -> `UIVibrationSequence` 
* `VibrateSequence(params VibrationSequence[] sequence)` -> `Vibrate(params UIVibrationSequence[] sequence)`
* (Android) `VibratonEffect.Composition` is working now
* (iOS) Using official `Apple.CoreHaptics` framework now
### Fixed
* Compile errors when Odin Inspector is not installed
### Migration Guide
* Don't forget to add `com.apple` to scopes of the registry whose URL is "https://upm.matchingham.net" (commonly named as "MG Package Registry" or "Matchingham").
* Configuration from previous versions will be **invalid**.
* It is recommended to read to documentation again.
* If the sample code is imported. Make sure that it is removed.

## [6.0.0] - 2024-09-17
### Changed
* Minimum Unity version is 2022.3 now
* Handyman dependency: v5.8.0 -> v5.8.2
* `VibrateSequence`: Using async/await instead of `MEC`

## [5.0.1] - 2024-09-05
### Fixed
* (Android) `HapticFeedback`: `performHapticFeedback` throws exception.
### Changed
* (Android) `CommonClasses`: `UnityPlayer` caching logic.
* Handyman dependency v5.0.0 -> v5.8.0
* Logger dependency v2.0.0 -> v2.1.0

## [5.0.0] - 2023-11-28
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
* Will be compiled only on Editor, Android and iOS (see `Migration Guide`)
### Removed
* `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* If you are trying to build your project not on Android or iOS, you will get compile errors. You have to use define symbols e.g. `UNITY_IOS || UNITY_ANDROID`

## [4.2.0] - 2023-11-02
### Added
* (Android) `Vibrate`: When `performHapticFeedback` method is unavailable `VibrationEffect` (API Level 26+) or `Vibrator` (API Level 1+) will be used according to the API level. This also fixes a rare exception.
* (Android) `DefaultVibrationDuration` is added to the config for legacy vibration APIs.
* (Android) `Vibrate`: `performHapticFeedback` will override the global vibration settings on the phone for Android 12 and older

## [4.1.0] - 2023-04-26
### Changed
* `VIBRATE` permission moved to a separate android library

## [4.0.2] - 2023-01-09
### Changed
* New Debugger implementation
### Fixed
* `VIBRATE` permission check on PreProcessBuild on Android

## [4.0.1] - 2022-10-07
### Fixed
* `Debugger`: controls sorting fixed
* Sample code compile fix

## [4.0.0] - 2022-10-05
### Added
* `Debugger`: added more controls for Android
### Changed
* Namespace changed

## [3.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`

## [2.0.6] - 2022-06-27
### Fixed
* `Unity Serializable Dictionary` dependency fixed
* asmdef file names fixed

## [2.0.5] - 2022-03-08
### Changed
* Initial release