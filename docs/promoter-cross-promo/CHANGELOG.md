# Changelog

## [7.2.0] - 2024-04-30
### Added
* `SendImpression()` API.
* `Click()` is not sending impressions anymore.
### Changed
* Cross Promotion card is now sending impressions when it becomes visible.
* Cross Promotion card just sends click url request when clicked, no longer any impressions.

## [7.1.2] - 2024-04-19
### Changed
* Updated documentation. Replaced links and names with generic placeholders.

## [7.1.1] - 2024-03-19
### Added
* Native AppLauncher for `LaunchApp()` public API
### Fixed
* `RequestAdvertisingIdentifierAsync()` use `advertisingId` to directly determine if it should be appended or not.
### Changed
* Removed native app launcher call on click for iOS.
* Updated click url to include IDFA on ios for id based deep linking
### Removed
* Unused Unity Advertisement Support references

## [7.1.1-preview.3] - 2024-03-19
### Fixed
* `RequestAdvertisingIdentifierAsync()` use `advertisingId` to directly determine if it should be appended or not.

## [7.1.1-preview.2] - 2024-03-19
### Removed
* Unused Unity Advertisement Support references

## [7.1.1-preview.1] - 2024-03-19
### Added
* Native AppLauncher for `LaunchApp()` public API

## [7.1.1-preview.0] - 2024-03-19
### Changed
* Removed native app launcher for iOS
* Updated click url to include IDFA on ios for id based deep linking

## [7.1.0] - 2024-02-23
### Added
* `FailureReason` implementation
### Changed
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
* Meteor dependency: v5.0.0 -> v5.1.0
* When mg builder 1.5.0 or higher is installed, AppLauncher Post Processing is handled by it.
### Fixed
* Version define for mg builder moved to editor assembly definition

## [7.1.0-preview.0] - 2024-02-16
### Added
* `FailureReason` implementation
### Changed
* Handyman dependency: v5.0.0 -> v5.3.1
* Logger dependency: v2.0.0 -> v2.1.0
* Meteor dependency: v5.0.0 -> v5.1.0

## [7.0.3-prev.1] - 2024-02-13
### Fixed
* Version define for mg builder moved to editor assembly definition

## [7.0.3-prev.0] - 2024-02-13
### Changed
* When mg builder 1.5.0 or higher is installed, AppLauncher Post Processing is handled by it.

## [7.0.2] - 2024-02-07
### Fixed
* Fix for IOS deeplinking do not work for apps with ID larger than 2147483647 

## [7.0.1] - 2024-01-23
### Changed
* The "Editor" namespace was changed to prevent potential conflicts with the "Unity.Editor" namespace  

## [7.0.0] - 2024-01-03
### Added
* Added callback for generating promo-cache when running game from the editor without previous build attempt.
### Changed
* Added an extra asset database refresh to force import of created asset instead of leaving it to the editor.
* Fixed an issue when loading cache on editor in android target mode
* Automatically create empty cache file on import, instead of processing cache when entering play mode.
* Instead of trying to process & cache promotions when entering play mode, add manual process & cache button under Cross Promotion menu
* Moved config generator in `Initializer` to `EditorApplication.update`. This should prevent config import issues
* Return value of `LoadImage` calls to a tuple `(Texture2D texture, bool isLocalImage)`. Fixes an exception when card unloads
with local image
* Updated README

## [7.0.0-prev.3] - 2023-12-20
### Changed
* Added an extra asset database refresh to force import of created asset instead of leaving it to the editor.
* Fixed an issue when loading cache on editor in android target mode

## [7.0.0-prev.2] - 2023-12-20
### Changed
* Automatically create empty cache file on import, instead of processing cache when entering play mode.
* Instead of trying to process & cache promotions when entering play mode, add manual process & cache button under Cross Promotion menu
* Updated README

## [7.0.0-prev.1] - 2023-12-20
### Added
* Added callback for generating promo-cache when running game from the editor without previous build attempt.
### Changed
* Moved config generator in `Initializer` to `EditorApplication.update`. This should prevent config import issues
* Return value of `LoadImage` calls to a tuple `(Texture2D texture, bool isLocalImage)`. Fixes an exception when card unloads
with local image
* Updated README

## [6.1.1] - 2023-12-15
### Added
* Set header call before send web request

## [6.1.0] - 2023-12-15
### Added
* Url referer header to download requests

## [6.0.0] - 2023-11-17
### Added
* Unity 2023.2.x compatibility (see `Migration Guide`)
### Changed
* Logger 2.x implementation (see `Migration Guide`)
* Minimum Unity version is now 2021.3
* Handyman dependency: v4.3.2 -> v5.0.0
* Meteor dependency: v4.3.0 -> v5.0.0
### Removed
* `LogFlags`, `OverrideLogFlagsToAllOnDebug`: With Logger 2.x update, all log levels are moved to Logger.Config
### Migration Guide
* With Logger v2.x update, we are setting a milestone here. If any other packages depend on Logger v1.x, you will get compile errors. Please update all packages.
* TextMeshPro dependency:
  * Unity 2023.2 or newer users: TextMeshPro package is deprecated. Unity UI v2+ package includes TextMeshPro. It is necessary to remove TextMeshPro package.
  * Unity 2023.1 and older users: TextMeshPro package is no longer a dependency, but it is necessary to install to use this package properly.

## [5.0.0] - 2023-11-16
### Added
* Local caching of remote assets.
### Fixed
* `StreamingAssets` access method fix

## [5.0.0-beta.2] - 2023-10-16
### Fixed
* `StreamingAssets` access method fix

## [5.0.0-beta.1] - 2023-10-16
### Added
* Local caching of remote assets.

## [4.2.0] - 2023-10-16
### Added
* `IsVideoAvailable(CrossPromo) : bool` API added.
### Changed
* CrossPromoCard checks video availability before requesting video path to prevent error log.

## [4.1.1] - 2023-08.29
### Fixed
* Bugfix: Card video player overriding show image state

## [4.1.0] - 2023-08.23
### Fixed
* iOS App Id can be larger than an integer now 

## [4.0.3] - 2023-08.09
### Updated
* Meteor dependency updated to 4.3.0
### Fixed 
* Allowed using same photo url in different promos.

## [4.0.2] - 2023-07.11
### Fixed
* Pipeline test.

## [4.0.0] - 2023-07.11
### Fixed
* Added video support to cross promotions.
  * `CrossPromo.videoUrl : string`
  * `CrossPromo.remoteVideoExtension : string`
  * `CrossPromo.localVideoPath : string`
  * `Promoter.GetVideoPath(CrossPromo) : string`
  * `Promoter.GetLocalVideoPath(CrossPromo) : string`
  * `Promoter.DownloadVideo(CrossPromo) : AsyncOperation`
* `CrossPromoCard` now supports video playback.

## [3.0.1] - 2023-06-07
### Fixed
* `CanShowCrossPromo`: persistent images count removed due to new local image feature
* A possible `NullReferenceException` when `imageUrl` is null or empty on `CrossPromoData`

## [3.0.0] - 2023-02-17
### Added
* Local image support added. If URLs aren't provided, this will ve used.
* `CrossPromoCard`: If `CrossPromoData` is null or its texture is null. Card will be closed automatically.
### Changed
* `RoundedPanel` => `9Sliced` for better compression
* `CrossPromoData`: `CrossPromoMap` removed. Instead of using separate arrays and id mapping, switched to Dictionary. Remote setting name is also changed to `crossPromoData` to prevent parsing issues.
### Fixed
* `PromoterDebugger` moved to correct folder
* `CrossPromoCard`: Code and prefab cleanup 

## [2.1.1] - 2023-01-09
### Added
* Auto Initialization
* New Debugger implementation

## [2.1.0] - 2022-10-11
### Changed
* `LoadOnEnable` is `LoadManually` now.

## [2.0.1] - 2022-09-06
### Fixed
* `Meteor` initialization message is `warning` instead of `error` now

## [2.0.0] - 2022-09-01
### Changed
* Code refactor with `Handyman`: All public fields and methods are static now.

## [1.3.0] - 2022-06-17
### Changed
* *Internal* code refactor 

## [1.2.3] - 2022-03-24
### Updated
*  Native Applauncher made specific for only iOS `#if UNITY_IOS` Conditional compilation added. This was causing problems while building in Android.

## [1.2.2] - 2022-03-09
### Updated
* License
* Documentation
* Handyman dependency

## [1.2.1] - 2022-02-22
### Changed
* Initialization of config is updated

## [1.2.0] - 2022-02-16
### Added 
* Native AppLauncher for iOS
  * Promoter config has new value called, `iosDeeplinkUrls` it accepts application id from AppStore as key value and Deeplink url as a parameter.
  Deeplink url must end wiht `://` eg `com.matchinghamgames.braindom3://`
  * Cross Promo object has a new value called `iosAppID` which can be obtained from AppStoreConnect or AppStore url ending with `idXXXXXXX`

## [1.1.0] - 2021-12-31
* Initial release