# Promoter Cross Promotion Module

## Overview

Promoter provides a simple way of displaying basic cross promotion in game. On iOS, if the promoting game is installed, it won't redirect to its store page, it will open the game. 

## Installing

* Install package from Package Manager UI

## How to use

* Open settings from `Matchingham Games > Cross Promotion` and set local promo data. This can also be
  configured from the remote using `crossPromoData` remote setting. 
* After configuring, to properly see full functionality in the editor, use `Cross Promotion > Process & Cache Promotions` menu

### Sample json format

```json
{
  "PinnedPromoId": "someGame",
  "Promos": {
    "someGame": {
      "title": "Some Game",
      "clickUrl": "https://click.link.com/awmdlkamwf",
      "impressionUrl": "https://impression.link.com/awkfmnakwlfmaw",
      "localImagePath": "",
      "hrImages": [
        "https://online.data.com/alkwmflakwmf"
      ],
      "mrImages": [
        "https://online.data.com/alkwmflakwmf"
      ],
      "lrImages": [
        "https://online.data.com/alkwmflakwmf"
      ],
      "videoUrl": "https://url.to.video/my-video",
      "remoteVideoExtension": ".mp4",
      "localVideoPath": "/Path/To/Streaming/Video/Asset",
      "iosAppID": 1513009813,
      "iosDeeplinkUrl": "com.matchingham.braindom://"
    },
    "ourNewGame": {
      "title": "Our New Game",
      "clickUrl": "https://click.link.com/awmdlkamwf",
      "impressionUrl": "https://impression.link.com/awkfmnakwlfmaw",
      "localImagePath": "",
      "hrImages": [
        "https://online.data.com/alkwmflakwmf"
      ],
      "mrImages": [
        "https://online.data.com/alkwmflakwmf"
      ],
      "lrImages": [
        "https://online.data.com/alkwmflakwmf"
      ],
      "videoUrl": "https://url.to.video/my-video",
      "remoteVideoExtension": ".mp4",
      "localVideoPath": "/Path/To/Streaming/Video/Asset",
      "iosAppID": 1526842079,
      "iosDeeplinkUrl": "com.braindom2riddle://"
    },
    "ourBestGame": {
      "title": "Our Best Game",
      "clickUrl": "https://click.link.com/awmdlkamwf",
      "impressionUrl": "https://impression.link.com/awkfmnakwlfmaw",
      "localImagePath": "",
      "hrImages": [
        "https://online.data.com/alkwmflakwmf"
      ],
      "mrImages": [
        "https://online.data.com/alkwmflakwmf"
      ],
      "lrImages": [
        "https://online.data.com/alkwmflakwmf"
      ],
      "videoUrl": "https://url.to.video/my-video",
      "remoteVideoExtension": ".mp4",
      "localVideoPath": "/Path/To/Streaming/Video/Asset",
      "iosAppID": 0,
      "iosDeeplinkUrl": ""
    },
    "justAnotherGame": {
      "title": "Just Another Game",
      "clickUrl": "https://click.link.com/awmdlkamwf",
      "impressionUrl": "https://impression.link.com/awkfmnakwlfmaw",
      "localImagePath": "",
      "hrImages": [
        "https://online.data.com/alkwmflakwmf"
      ],
      "mrImages": [
        "https://online.data.com/alkwmflakwmf"
      ],
      "lrImages": [
        "https://online.data.com/alkwmflakwmf"
      ],
      "videoUrl": "https://url.to.video/my-video",
      "remoteVideoExtension": ".mp4",
      "localVideoPath": "/Path/To/Streaming/Video/Asset",
      "iosAppID": 0,
      "iosDeeplinkUrl": ""
    }
  }
}
```

* **Don't make changes in the json format, otherwise Promoter won't be able to parse it**

* `pinnedPromo`: Which promotion is pinned
* `promos`: Promotion data
  * `title`: Title of the promotion
  * `clickUrl`: When the player clicks on the promotion, the player will be redirected to this URL
  * `impressionUrl`: When the player clicks on the promotion, this URL will be executed in the background.
  * `localImagePath`: Path to the local image in a `Resources` folder. (e.g. if the image's real path is `Assets/Resources/CrossPromo/image.jpg`, you need to set the path as `CrossPromo/image`). This works only if `hrImages`, `mrImages` and `lrImages` are empty.
  * `hrImages`: URLs of high resolution images of the promotion
  * `mrImages`: URLs of medium resolution images of the promotion
  * `lrImages`: URLs of low resolution images of the promotion
  * `videoUrl`: URL for a video promotion. Can be left empty.
  * `remoteVideoExtension`: The file extension of the video of `videoURL`
  * `iosAppID`: iOS App Store Id the promoting app
  * `iosDeeplinkUrl`: iOS Deep Link URL of the promoting app. There must be `://` at the end. Example: `com.matchingham.braindom://`


* Call `Promoter.Instance.Initialize()` at the appropriate place.
* Right click any canvas and use `UI > Matchingham > Cross Promo Card` to create a cross promotion card.
* Click card and configure how it should work
    * If you want to manually load and show promotions, make sure that `Load Manually` is enabled.
    * Check `Show Pinned Promo` to display a certain promotion pinned in the promo data.
    * Uncheck `Show Pinned Promo` and provide a `Promo Map` to fetch a certain promotion from `mappedPromotions`.
    * All of these options can also be set from code, so you can bind `Promo Map` value to game logic, 
      like finished level. Please note if you are setting these values in code, either set them before the
      card becomes enabled, or enable `Load Manually` and manually load the card.
    * To display a video promotion, make sure to check `Play Video` on the promotion card, or
      set it to true from code when using manual configuration. This will configure the card to
      display the video associated with the promotion, if available.

## Configuration
Go to `Matchingham > Cross Promotion`

| Variable                                | Remote Name        | Description                                                                 |
|-----------------------------------------|--------------------|-----------------------------------------------------------------------------|
| Enabled                                 | crossPromoEnabled  | Enabling/disabling the module                                               |
| AutoInitialize                          | N/A                | If enabled, you don't need to call Initialize method manually.              |
| Cross Promo Data                        | crossPromoData     | Cross Promotion data. See also [How to use](#how-to-use)                    |  

## API & Details

### Promoter

#### Methods

* **GetPinnedPromo()**
  : Returns pinned promo info from the provided promotion data.

* **GetMappedPromo(string map)**
  : Try to get a promotion info with a string key (`map`). If no promotion info is mapped for provided key, a random promotion will be returned.
  
* **GetRandomPromo()**
  : Returns a random promotion info from the promotion data.

* **LoadImage(CrossPromo promo, Resolution resolution, int index) : (Texture2D texture, bool isLocalImage)**
  : Given a promotion info and resolution type, loads the image associated with given index, returns it as a tuple (texture, isLocalImage).
  
* **LoadRandomImage(CrossPromo promo, Resolution resolution) : (Texture2D texture, bool isLocalImage)**
  : Given a promotion info and resolution type, loads one of the images associated with it. Returns it as a tuple (texture, isLocalImage).

* **LoadLocalImage(CrossPromo promo) : (Texture2D texture, bool isLocalImage)**
  : Given a promotion info, loads the local image. Returns it as a tuple (texture, isLocalImage).

* **IsVideoAvailable(CrossPromo promo)
  : Returns a boolean value, indicating whether video content of the promotion is downloaded and available

* **GetVideoPath(CrossPromo promo)**
  : Given a promotion info, returns the path to video file downloaded (null if video is not downloaded yet.)

* **GetLocalVideoPath(CrossPromo promo)**
  : Given a promotion info, returns the path to local video file

* **DownloadVideo(CrossPromo promo)**
  : Given a promotion info, starts an synchronous operation to download and cache the associated video. Returns the download operation instance

* **Click(CrossPromo promo)**
  : Sends an impression for provided promotion and redirects user to promoted url

### Common

#### Methods

* **Initialize()**
  : Starts module initialization. You need to call this at the appropriate place.

* **WhenInitialized(Action callback)**
  : Allows you to register `callback` that will be fired only after the module is successfully initialized. Use this to execute logic that requires this module to be initialized first. If the module has already initialized, immediately invokes the callback.

* **WhenFailedToInitialize(Action callback)**
  : Allows you to register `callback` that will be fired only after the module fails to initialize for any reason. Use this to handle what should happen in case this module fails to initialize. If the module has already failed to initialize, immediately invokes the callback.

* **WhenReady(Action callback)**
  : Combined version of `WhenInitialized` and `WhenFailedToInitialize`. Delays execution of `callback` till module is first initialized or failed to initialize, immediately invoke the callback if it is already initialized or failed to initialize.

#### Fields

* **State**
  : Initialization state of the module

* **Instance**
  : Instance of the module

* **LateInitialized**
  : When the module needs an internet connection but the player became online while playing the game, this becomes `true`

* **Ready**
  : If the module is initialized successfully and ready to operate

* **Config**
  : Configuration of the module. See [configuration](#configuration)


### Cross Promo Card

#### Fields

* **Resolution**
  : Image quality of the card. Possible values: `LowResolution`, `MediumResolution` and `HighResolution`

* **ShowPinnedPromo**
  : Should the card load the pinned promo from the config the next time it loads a promotion?

* **PromoMap**
  : Provide a string id for the promotion to be loaded here. These ids are defined inside the mapped
  promotions data in the config.

* **LoadManually**
  : Should the card be loaded manually? If `true`: You must call `Load` method by yourself.

#### Methods

* **Load()**
  : Manually load a promotion. If you want to configure how the card works, you can turn `Load on Enable`
  off from the inspector, and use this method according to your needs.


