# Changelog

## [1.5.0] - 2024-04-03
### Added
* `PlayerPrefsAsync` method for creating a player prefs based stash asynchronously.
* `PersistentPathAsync` method for creating a persistent path based stash asynchronously.
* Soft dependency on `Handyman` for utilizing `UnityMainThreadDispatcher` for logging from async ops.

## [1.4.0] - 2022-10-13
### Changed
* Code cleanup

## [1.3.1] - 2022-06-13
### Fixed
* Root namespace of Editor asmdef
* AutoSave option on constructor wasn't working.

## [1.3.0] - 2022-05-20
### Added
* **Has** method: You can check if a value is stored in the stash.
* asmdef files have root namespace now.

## [1.2.1] - 2022-03-09
### Added
* Initial release
